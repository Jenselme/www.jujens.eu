#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = "Julien Enselme"
SITENAME = "Jujens' blog"
SITEURL = ""
TIMEZONE = "Europe/Paris"
DEFAULT_LANG = "fr"
PATH = "content"
ARTICLE_EXCLUDES = ['static']
PAGE_EXCLUDES = ['static']
# https://www.iconfinder.com/icons/3215592/brand_brands_linux_logo_logos_icon
SITELOGO = "/images/profile.png"
FAVICON = "/images/favicon.png"

LOCALE = (
    "en_US",
    "fr_FR",
)
DEFAULT_DATE_FORMAT = "%Y-%m-%d"

LOAD_CONTENT_CACHE = True
CACHE_CONTENT = True
IGNORE_FILES = [".#*"]

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None


# Pages
PAGES_LINKS = (
    ("Privacy", "/pages/privacy-policy.html"),
    ("CV (fr)", "/pages/CV.html"),
    ("CV (en)", "/pages/cv_en.html"),
)

# Blogroll
LINKS = (('The "Last run" game', "https://www.last-run.com/"),)

# Social widget
SOCIAL = (
    ("mastodon", "https://mamot.fr/@jenselme"),
    ("gitlab", "https://gitlab.com/Jenselme"),
    ("github", "https://github.com/Jenselme"),
)

DEFAULT_PAGINATION = 10

# static paths will be copied without parsing their contents
STATIC_PATHS = [
    "images",
    "static",
]

# Metadata
PATH_METADATA = r"(?P<category>[^/]*)"
FILENAME_METADATA = r"(?P<date>\d{4}-\d{2}-\d{2})_(?P<slug>.*)"

# URL
ARTICLE_URL = "posts/{date:%Y}/{date:%b}/{date:%d}/{slug}/"
ARTICLE_SAVE_AS = "posts/{date:%Y}/{date:%b}/{date:%d}/{slug}/index.html"
PAGE_URL = "pages/{slug}.html"
PAGE_SAVE_AS = "pages/{slug}.html"
ARTICLE_LANG_URL = "posts/{lang}/{date:%Y}/{date:%b}/{date:%d}/{slug}/"
ARTICLE_LANG_SAVE_AS = "posts/{lang}/{date:%Y}/{date:%b}/{date:%d}/{slug}/index.html"

# Tags cloud
TAG_CLOUD_STEPS = 4
TAG_CLOUD_MAX_ITEMS = 20

# Plugins
PLUGIN_PATHS = ["./vendor/plugins/", "./vendor/plugins-in-their-own-repo/"]
PLUGINS = ["creole_reader", "sitemap"]

# Themes
## All options: https://github.com/alexandrevicenzi/Flex/wiki/Custom-Settings
THEME = "vendor/flex-theme"
SITESUBTITLE = ""
SITEDESCRIPTION = "Programming blog, mostly about Python and JS."
DISPLAY_SEARCH_FORM = True
ROBOTS = "index, follow"
CC_LICENSE = {
    "name": "Creative Commons Attribution-ShareAlike",
    "version": "4.0",
    "slug": "by-sa",
}
REL_CANONICAL = True
COPYRIGHT_NAME = "Julien Enselme"

USE_GOOGLE_FONTS = False

MAIN_MENU = True
MENUITEMS = (
    ("Archives", "/archives.html"),
    ("Categories", "/categories.html"),
    ("Tags", "/tags.html"),
)

CUSTOM_CSS = "static/custom.css"

ISSO_URL = "//comments.jujens.eu"
ISSO_EMBED_JS_PATH = "//comments.jujens.eu/js/embed.min.js"
ISSO_OPTIONS = {
    "avatar": "false",
    "gravatar": "false",
    "reply-to-self": "false",
    "reply-notifications": "true",
}
## Extra cool options provided: change pygment style

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

# Sitemap (plugin related)
SITEMAP = {
    "format": "xml",
    "exclude": ["tag/", "category/"],
    "priorities": {"articles": 0.8, "indexes": 0.5, "pages": 0.5},
    "changefreqs": {"articles": "monthly", "indexes": "weekly", "pages": "monthly"},
}
