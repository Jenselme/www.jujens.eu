PY?=python3
PELICAN?=pelican
PELICANOPTS=

BASEDIR=$(CURDIR)
INPUTDIR=$(BASEDIR)/content
OUTPUTDIR=$(BASEDIR)/output
CONFFILE=$(BASEDIR)/pelicanconf.py
PUBLISHCONF=$(BASEDIR)/publishconf.py

SSH_HOST=olivaw
SSH_PORT=222
SSH_USER=jenselme
SSH_TARGET_DIR=/data/nginx/blog

S3_BUCKET=my_s3_bucket

DEBUG ?= 0
ifeq ($(DEBUG), 1)
	PELICANOPTS += -D
endif

help:
	@echo 'Makefile for a pelican Web site                                        '
	@echo '                                                                       '
	@echo 'Usage:                                                                 '
	@echo '   make html                        (re)generate the web site          '
	@echo '   make clean                       remove the generated files         '
	@echo '   make lint                        lint the files                     '
	@echo '   make publish                     generate using production settings '
	@echo '   make devserver [PORT=8000]       start/restart develop_server.sh    '
	@echo '   make upload                      upload the web site                '
	@echo '                                                                       '
	@echo 'Set the DEBUG variable to 1 to enable debugging, e.g. make DEBUG=1 html'
	@echo '                                                                       '

html:
	uv run $(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)

clean:
	[ ! -d $(OUTPUTDIR) ] || rm -rf $(OUTPUTDIR)

lint:
	uv run rst-lint content/**/*.rst

devserver:
	uv run $(PELICAN) --autoreload --listen

publish: lint
	uv run $(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(PUBLISHCONF) $(PELICANOPTS)

upload: publish
	git push
	rclone sync output blog:www.jujens.eu --progress

.PHONY: html help clean devserver publish upload lint
