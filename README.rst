#. Install ``pipenv``
#. Init shell with ``pipenv install && pipenv shell``
#. Run ``git submodule update --init --recursive`` to init plugins.
#. Run ``make devserver`` to run in reload mode.
#. Run ``make rsync_deploy`` to deploy.
