JavaScript tips
###############

:tags: JavaScript
:lang: en

.. contents::

Normalize an UTF-8 string
=========================

Rely on `String.prototype.normalize() <https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/normalize>`__.

.. code:: javascript

    const str = "Crème Brulée"
    str.normalize('NFD').replace(/[\u0300-\u036f]/g, "")
    > 'Creme Brulee'

Source: https://stackoverflow.com/questions/990904/remove-accents-diacritics-in-a-string-in-javascript/37511463#37511463
