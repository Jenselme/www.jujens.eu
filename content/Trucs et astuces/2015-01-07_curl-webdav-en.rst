Use a webdav service with curl
##############################

:tags: Linux, Bash/Shell, Webdav, curl
:slug: curl-webdav
:lang: en

It is really easy to use ``curl`` to communicate with a webdav service. You just
have to be aware that all events are contained in separate files and that the
lines describing the event must be surrounded by ``BEGIN:VCALENDAR`` and
``END:VCALENDAR`` lines.

To get the list of all events as an xml answer :

.. code:: bash

   curl -k --user "${login}:${password}" -X PROPFIND "https://owncloud.jujens.eu/remote.php/caldav/calendars/jujens/defaultcalendar"

To post a new event or to update an existing event, simply do a put request to
your service url concatenated with a file name. If the file name is not already
used, you create a new event. Otherwise, you update the given event.

.. code:: bash

   curl -k --user "${login}:${password}" -X PUT -H "Content-Type: text/calendar; charset=utf-8" -d "$a" "https://owncloud.jujens.eu/remote.php/caldav/calendars/jujens/omis/<mon-fichier.ics>"

It is the same for the deletion (with a delete request):

.. code:: bash

	  curl -k --user "${login}:${password}" -X DELETE -H "Content-Type: text/calendar; charset=utf-8" "https://owncloud.jujens.eu/remote.php/caldav/calendars/jujens/omis/<mon-fichier.ics>"
