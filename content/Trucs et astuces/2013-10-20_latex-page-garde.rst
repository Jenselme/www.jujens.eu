LaTeX : une jolie page de garde
###############################

:tags: LaTeX

LaTeX sait faire des pages de titre avec la commande \maketitle si on a au préalable renseigné les champs ``\author{Moi}``, ``\date{\today}`` et ``\title{Mon titre}``. Néanmoins, elle fait un peu austère. Heureusement, l’environement titlepage vient à la rescousse. Avec le code ci-dessous, on obtient un très beau résultat.

.. code-block:: latex

    \documentclass[12pt, openany]{report}
    \usepackage[utf8]{inputenc}
    \usepackage[T1]{fontenc}
    \usepackage[a4paper,left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
    \usepackage[frenchb]{babel}
    \usepackage{libertine}
    \usepackage[pdftex]{graphicx}

    \setlength{\parindent}{0cm}
    \setlength{\parskip}{1ex plus 0.5ex minus 0.2ex}
    \newcommand{\hsp}{\hspace{20pt}}
    \newcommand{\HRule}{\rule{\linewidth}{0.5mm}}

    \begin{document}

    \begin{titlepage}
      \begin{sffamily}
      \begin{center}

        % Upper part of the page. The '~' is needed because \\
        % only works if a paragraph has started.
        \includegraphics[scale=0.04]{img1.JPG}~\\[1.5cm]

        \textsc{\LARGE École Centrale Marseille}\\[2cm]

        \textsc{\Large Rapport de stage 1A}\\[1.5cm]

        % Title
        \HRule \\[0.4cm]
        { \huge \bfseries Opérateur expédition produits finis\\[0.4cm] }

        \HRule \\[2cm]
        \includegraphics[scale=0.2]{img2.JPG}
        \\[2cm]

        % Author and supervisor
        \begin{minipage}{0.4\textwidth}
          \begin{flushleft} \large
            Moi \textsc{Même}\\
            Promo 2015\\
          \end{flushleft}
        \end{minipage}
        \begin{minipage}{0.4\textwidth}
          \begin{flushright} \large
            \emph{Tuteur :} M. Le \textsc{Tuteur}\\
            \emph{Chef d'équipe : } M. Chef \textsc{D’Équipe}
          \end{flushright}
        \end{minipage}

        \vfill

        % Bottom of the page
        {\large 1\ier{} Juillet 2013 — 30 Août 2013}

      \end{center}
      \end{sffamily}
    \end{titlepage}
    \end{document}

.. image:: /images/LaTeX_pageTitle.png
    :alt: La page de garde

