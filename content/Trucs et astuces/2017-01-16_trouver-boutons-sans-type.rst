Trouver tous les boutons sans attributs type
############################################

:tags: HTML, Python, formulaire

Certains navigateurs (comme Chrome) afficheront une erreur dans la console si vous avez des formulaires créés avec la balise ``form`` et des boutons sans l'attribut ``type``. Il peut donc être intéressant d'avoir un petit script qui trouve tous ces boutons pour ajouter l'attribut ``type``. La bonne nouvelle c'est qu'avec Python et `BeautifulSoup 4 <https://www.crummy.com/software/BeautifulSoup/bs4/doc/>`__ c'est assez facile :

.. code:: python

    import glob

    from bs4 import BeautifulSoup


    def fix_buttons_type():
        for html_file_path in glob.glob('**/*.html', recursive=True):
            with open(html_file_path, 'r') as html_file:
                soup = BeautifulSoup(html_file.read(), 'html.parser')
                number_buttons_without_type = 0
                buttons = soup.find_all('button')
                for button in buttons:
                    if button.get('type') is None:
                        if number_buttons_without_type == 0:
                            print(html_file_path, end=' ')
                        number_buttons_without_type += 1

                if number_buttons_without_type > 0:
                    print(number_buttons_without_type)


    if __name__ == '__main__':
        fix_buttons_type()


*Note :* Si vous utilisez Python 3.4 ou inférieur, remplacez ``import glob`` par ``import glob2 as glob`` et installez `glob2 <https://pypi.python.org/pypi/glob2>`__.

On peut aussi légèrement modifier le script pour ajouter l'attribut avec la valeur par défaut ``type="button"`` qui signale au navigateur que c'est un simple bouton qui ne doit rien faire vis à vis du formulaire (soumission ou remise à zéro). Le problème étant que cela peut casser le formatage du votre fichier.

.. code:: python

    import glob

    from bs4 import BeautifulSoup


    def fix_buttons_type():
        for html_file_path in glob.glob('**/*.html', recursive=True):
            with open(html_file_path, 'r') as html_file:
                soup = BeautifulSoup(html_file.read(), 'html.parser')
                number_buttons_without_type = 0
                buttons = soup.find_all('button')
                for button in buttons:
                    if button.get('type') is None:
                        button['type'] = 'button'

            with open(html_file_path, 'w') as html_file:
                html_file.write(soup.prettify())


    if __name__ == '__main__':
        fix_buttons_type()
