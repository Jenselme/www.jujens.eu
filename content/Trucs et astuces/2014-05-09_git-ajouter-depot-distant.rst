Ajouter un dépôt git distant
############################

:tags: git
:summary: git est tout à fait capable de gérer plusieurs dépôt distant. Voilà
	  comment en ajouter un.

Un cas d'utilisation typique est lorsque vous avez forké un dépôt sur github et
que vous voulez le synchroniser avec le dépôt initial. Commençons par lister les
sources disponibles et ajouter la nouvelle source avec la commande ``git remote
add NOM URL``.

.. code:: sh

	  [jenselme@fastolfe SimpleCV]% git remote -v
	  origin	git@github.com:Jenselme/SimpleCV.git (fetch)
	  origin	git@github.com:Jenselme/SimpleCV.git (push)

	  [jenselme@fastolfe SimpleCV]% git remote add upstream https://github.com/sightmachine/SimpleCV.git

	  [jenselme@fastolfe SimpleCV]% git remote -v          [git][SimpleCV/.][develop]
	  origin	https://github.com/Jenselme/SimpleCV.git (fetch)
	  origin	https://github.com/Jenselme/SimpleCV.git (push)
	  upstream	https://github.com/sightmachine/SimpleCV.git (fetch)
	  upstream	https://github.com/sightmachine/SimpleCV.git (push)

Il ne reste plus qu'à mettre à jour le dépôt.

=======
 fetch
=======

.. code:: sh

	  jenselme@fastolfe SimpleCV]% git fetch upstream
	  remote: Counting objects: 155, done.
	  remote: Compressing objects: 100% (72/72), done.
	  remote: Total 155 (delta 102), reused 130 (delta 83)
	  Receiving objects: 100% (155/155), 208.82 KiB | 237.00 KiB/s, done.
	  Resolving deltas: 100% (102/102), done.
	  From https://github.com/sightmachine/SimpleCV
	  * [new branch]      AVT-timing -> upstream/AVT-timing
	  * [new branch]      SCV-1      -> upstream/SCV-1
	  * [new branch]      develop    -> upstream/develop
	  * [new branch]      gsoc       -> upstream/gsoc
	  * [new branch]      master     -> upstream/master
	  * [new branch]      njotask/JNAPC-502 -> upstream/njotask/JNAPC-502
	  * [new branch]      task/DEMOS-65 -> upstream/task/DEMOS-65
	  * [new branch]      task/JNAPC-520 -> upstream/task/JNAPC-520

=======
 merge
=======

.. code:: sh

	  [jenselme@fastolfe SimpleCV]% git branch         [git][SimpleCV/.][develop]
	  * develop

	  [jenselme@fastolfe SimpleCV]% git checkout develop

	  [jenselme@fastolfe SimpleCV]% git merge upstream/develop
	  Merge made by the 'recursive' strategy.
	  README.markdown        | 32 +++++++++++++++++++++++++-------
	  SimpleCV/ImageClass.py |  2 +-
	  2 files changed, 26 insertions(+), 8 deletions(-)

Source : https://help.github.com/articles/syncing-a-fork
