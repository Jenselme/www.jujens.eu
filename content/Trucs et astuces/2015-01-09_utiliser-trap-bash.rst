Utiliser trap en bash pour pièger des signaux
#############################################
:tags: Linux, Bash/Shell
:modified: 2017-09-05

Bash est capable d'intercepter les signaux envoyés par certains raccourcis
claviers (comme Ctrl-C) et de changer le comportement par défaut de ces
raccourcis. Il suffit pour cela d'utiliser la commande ``trap``. Elle prend en
premier argument la commande à exécuter puis les signaux sur lesquels elle doit
réagir.

Par exemple :

.. code:: bash

	  trap "echo yollo" 2 3

affichera ``yollo`` quand on fait ``Ctrl-C``.

On obtient la liste des signaux avec leurs numéros et leurs noms avec la commande
``kill``.

.. code:: bash

	  kill -l
	  1) SIGHUP       2) SIGINT       3) SIGQUIT      4) SIGILL
	  5) SIGTRAP      6) SIGABRT      7) SIGEMT       8) SIGFPE
	  9)SIGKILL     10) SIGBUS      11) SIGSEGV     12) SIGSYS

Pour utiliser les noms des signaux, utiliser:

.. code:: bash

    trap "echo 'sig INT caught' " INT


Historique
----------

- Mise à jour du 2017-09-05: ajout d'un exemple d'utilisation des noms des signaux. Merci à `Thomas <https://www.jujens.eu/posts/2015/Jan/09/utiliser-trap-bash/#isso-18>`__ et `Lokta <https://www.jujens.eu/posts/2015/Jan/09/utiliser-trap-bash/#isso-127>`__.
