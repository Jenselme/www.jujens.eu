Require valid-user on Apache only for some request types
########################################################

:tags: apache
:lang: en

You can use the ``Limit`` directive like this:

.. code:: apache

    AuthType basic
    AuthName "Restricted area"
    AuthUserFile /var/www/passwd
    Require valid-user
    # Only GET and OPTIONS request are allowed without authentication.
    <Limit GET OPTIONS>
        Require all granted
    </Limit>
