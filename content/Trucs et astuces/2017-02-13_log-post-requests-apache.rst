Logger les données d'une requête POST avec apache
#################################################

:tags: apache

D'après le manuel, une façon simple de logger le contenu d'une requête POST (a priori uniquement en développement pour débugger l'application, sinon la taille des logs de production va exploser et on risque de faire fuiter des informations) est d'utiliser le module `dumpio <https://httpd.apache.org/docs/2.4/fr/mod/mod_dumpio.html>`__, soit pour Apache 2.4 :

.. code:: apache

    # On met les logs dans des fichiers à part pour faciliter la lecture.
    CustomLog /var/log/httpd/website.log combined
    ErrorLog /var/log/httpd/website.error.log

    # On active les logs debug
    LogLevel debug

    # On active le module
    DumpIOInput On
    DumpIOOutput On
    LogLevel dumpio:trace7

Malheureusement, je n'ai jamais réussi à le faire fonctionner. Heureusement, il y a un autre module qui permet de faire ça : ``mod_security``. Il se configure comme suit :

.. code:: apache

    # On active le module.
    SecRuleEngine On
    SecAuditEngine on
    # On lui donne un fichier de log.
    SecAuditLog /var/log/httpd/website-audit.log
    # On l'autorise à accéder au corps des requêtes.
    SecRequestBodyAccess on
    SecAuditLogParts ABIFHZ

    # On configure une action par défaut.
    SecDefaultAction "nolog,noauditlog,allow,phase:2"

    # On définit une règle qui nous permet de logger le contenu des requêtes POST
    SecRule REQUEST_METHOD "^POST$" "chain,allow,phase:2,id:123"
    SecRule REQUEST_URI ".*" "auditlog"

Source : https://www.technovelty.org/web/logging-post-requests-with-apache.html
