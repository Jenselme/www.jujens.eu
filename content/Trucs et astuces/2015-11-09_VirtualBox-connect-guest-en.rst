Connect to a VirtualBox VM
##########################

:tags: VirtualBox, Virtualisation
:slug: VirtualBox-connect-guest
:lang: en

I recently had to create a server to perform deployment tests for work. This
server is a virtual machine created with `VirtualBox <http://virtualbox.org/>`_
and runs Fedora 22.

For the tests to be realistic, I had to connect with SSH to the VM. Here is the
method I followed:

#. In *File > Preferences > Network*, under the *Host-only Networks* tab,
   click on the add button. This adapter will be used by the host and the guest
   to communicate on the same private network.
#. In the machine settings, go in *Network*. Activate a new adapter and
   configure it like this:

   #. *Attached to:* choose *Host-only Adapter*
   #. *Name* should be set automatically on the adapter you previously
      created. If it is not, set its value manually.

You can now boot the machine. In a terminal, type ``ip a`` to get the ip address
fo the VM. You can now connect to the VW with SSH and do HTTP requests to you VM
server. You should put the ip address in your ``/tc/hosts`` to have a easy to
remember name and to ease the configuration of your HTTP server.
