Docker compose tips
###################

:tags: Docker, Docker compose

For my tips about docker, go `here <|filename|2015-05-24_docker.rst>`__.

.. contents::

Use docker-compose.override.yml
===============================

As describe `here <https://docs.docker.com/compose/extends>`__, if you create a ``docker-compose.override.yml`` next to your ``docker-compose.yml``, you can override or add values to the docker file. This file is loaded by default. To ignore it, you must use the ``--ignore-override`` option.

For instance, with this ``docker-compose.yml``:

.. code:: yaml

    version: '2'

    web:
      image: example/my_web_app:latest
      links:
        - db
        - cache

    db:
      image: postgres:latest

    cache:
      image: redis:latest

and this ``docker-compose.override.yml``:

.. code:: yaml

    version: '2'

    web:
      volumes:
        - '.:/code'
      ports:
        - 8883:80

    db:
      ports:
        - 5432:5432

when you run ``docker-compose up``, it will be like you used this  ``docker-compose.yml``:

.. code:: yaml

    version: '2'

    web:
      image: example/my_web_app:latest
      volumes: 
        - '.:/code'
      ports:
        - 883:80
      links:
        - db
        - cache

    db:
      image: postgres:latest
      ports:
        - 5432:5432

    cache:
      image: redis:latest
