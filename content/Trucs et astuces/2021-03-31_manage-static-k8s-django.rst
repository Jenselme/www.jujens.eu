Manage static files for Django when deployed on kubernetes
==========================================================
:tags: devops, k8s, kubernetes, Django, Python
:lang: en

This will be a continuation of my article `Some tips to deploy Django in kubernetes <{filename}./2021-03-29_deploy-django-kubernetes.rst>`__.
In this previous article, I talked about generic tips you should apply to deploy Django into kuberentes.
Here, I'll focus on static files.

If you don't already know that, ``gunicorn`` (or any other app server) is not designed to serve your static files directly.
You should use something else.

I think you have three main options:

- Rely on `WhiteNoise <http://whitenoise.evans.io/en/stable/>`__ and let your application serve the files.
- Put all your static files into a S3 bucket or something equivalent. For that, you will need to:

  - Make this bucket public on the internet.
  - Configure ``STATIC_URL`` in you settings so Django knows where to search for these files.
  - Collect your static and upload them into the bucket during your deployment process.

- Configure an nginx sidecar and let it serve the files. For that, you will need to:

  - Configure the nginx sidecar as described in `my previous article <{filename}./2021-03-29_deploy-django-kubernetes.rst>`__.
  - Collect the static in the Dockerfile with something like this (I updated my ``setup-django-run-as-non-root.sh`` script for that):

    .. code:: bash

        # Collect static
        # Use dummy values just to allow the command to run.
        export "DJANGO_SETTINGS_MODULE=myapp.settings.prod"
        export "DB_NAME=postgres"
        python manage.py collectstatic --on-input

  - Mount a shared folder in both nginx and the Django pod:

    - Configure this volume mount in both pods:

      .. code:: yaml

        - name: staticfiles
            mountPath: /var/www/api/
            # The API must be able to copy the files to the volume.
            readOnly: false

    - Configure this empty volume:

      .. code:: yaml
        
        - name: staticfiles
          emptyDir: {}

  - Use a script to copy the static files and then run the application: an ``emptyDir`` is always empty at pod startup even if its mount point was not in the image. So, if you mount it directly to the ``static`` folder, it will be emptied. So we need to mount it elsewhere and then run the application. I created ``run-django-production.sh`` for that:

    .. code:: bash

        #!/bin/bash

        set -o errexit
        set -o pipefail
        set -o nounset
        
        mkdir -p /var/www/api/
        cp -R static /var/www/api/
        gunicorn --bind :8000 --workers 5 myapp.wsgi

  - Configure nginx correctly, for instance with this configuration:

    .. code:: nginx
        :number-lines:

        upstream app_server {
            server 127.0.0.1:{{ .Values.container.port }} fail_timeout=0;
        }


        server {
            listen 80;
            root /var/www/api/;
            client_max_body_size 1G;

            access_log stdout;
            error_log  stderr;

            location / {
                location /static {
                    add_header Access-Control-Allow-Origin *;
                    add_header Access-Control-Max-Age 3600;
                    add_header Access-Control-Expose-Headers Content-Length;
                    add_header Access-Control-Allow-Headers Range;

                    if ($request_method = OPTIONS) {
                        return 204;
                    }

                    try_files /$uri @django;
                }

                location /nghealth {
                  return 200;
                }

                try_files $uri @django;
            }

            location @django {
                proxy_connect_timeout 30;
                proxy_send_timeout 30;
                proxy_read_timeout 30;
                send_timeout 30;
                # We have another proxy in front of this one. It will capture traffic
                # as HTTPS, so we must not set X-Forwarded-Proto here since it's already
                # set with the proper value.
                # proxy_set_header X-Forwarded-Proto $schema;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header Host $http_host;
                proxy_redirect off;
                proxy_pass http://app_server;
            }
        }



Now that we've seen the options this question remains: which option should I use?
I guess it depends:

- If you need a nginx sidecar (to handle file uploads for instance, see my other article), you can rely on nginx if your files are small: you still want to keep the image as small as possible to speed up the deployment of your app. With this method, you'll avoid adding another component to your application. And since we are talking about static files here, and since it's mostly CSS and JS files, you shouldn't have very big files, so this should work great. If you have big static files in your app, you should probably store them outside your repo anyway.
- If you have big files or can afford it, use a bucket. It's simple and reliable.
- If you are stuck and cannot use anything else, rely on good old WhiteNoise.
