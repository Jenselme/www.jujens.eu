Utiliser un tableau dans un Makefile
####################################

:tags: make

J'ai récemment eu besoin de lancer une tâche ``make`` pour plusieurs
arguments. Je me suis demandé si on pouvait dans le Makefile, en passant une
variable en argument sous la forme ``portals=geojb,n16``, répéter une tâche
plusieurs fois. Pour cela, il faut réussir à boucler sur l'entrée. Cela se fait
comme suit :

.. code:: Makefile

   .PHONY: toto
   toto:
	@echo ${portals}
	IFS=',' && \
	for portal in $${portals}; do \
            echo "$${portal}"; \
	    $(call process_portal, $${portal}); \
	done

   define process_portal
	echo "processing $1"; \
	echo "done"
   endef


Note : on peut tout à fait passer les arguments multiples sous la forme
``portals="geojb n16"``. Il faut alors supprimer la ligne ``IFS=','``.

On notera que c'est beaucoup plus facile avec `manuel
<https://github.com/ShaneKilkelly/manuel>`_ (un petit lanceur de tâche écrit en
moins de 150 lignes de bash que j'ai décidé d'utiliser à la place de make) :

.. code:: bash

          #! /usr/bin/env bash

          function hello {
              for portal in "$@"; do
                  process "${portal}"
              done
          }


          function _process {
              echo "processing $1"
              echo "done"
          }
