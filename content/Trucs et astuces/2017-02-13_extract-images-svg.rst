Extraire toutes les images encodées en base64 d'un SVG
######################################################

:tags: Python, SVG

Récemment, j'ai eu besoin d'intégrer un SVG dans un template Aurelia. Malheureusement, il contenait beaucoup d'images et elles étaient toutes incluses au format base64. Cela rendait le fichier quasiment inutilisable avec de gros pâtés qui empêchent de voir le code utile et d'ajouter les attributs « Aurelia » (comme ``if.bind``). Heureusement, Python (3) est là !

Le script ci-dessous prend comme paramètre le chemin vers un fichier SVG et va extraire toutes les images encodées en base64 dans le dossier courrant. Le SVG utilisant ces images extraites est sauvegardé dans le dossier courrant avec le nom ``svg-without-images.svg``.

.. code:: python
    :number-lines:

    import sys

    from base64 import b64decode
    from lxml import etree


    # On définit les espaces de nom XML (namespace) qui vont nous permettre de faire des requêtes dans le SVG.
    NS = {
        'svg': 'http://www.w3.org/2000/svg',
        'xlink': 'http://www.w3.org/1999/xlink',
    }


    def print_help():
        print('./extract-images-svg.py SVG_FILE')
        print('This will extract the images included in b64 in the SVG.')


    def extract_images_svg(file_name):
        # On ouvre le fichier source et on le parse.
        with open(file_name) as svg_file:
            svg = etree.parse(svg_file)

        # On utilise xpath pour trouver toutes les images contenues dans le fichier.
        images = svg.xpath('.//svg:image', namespaces=NS)
        for index, img in enumerate(images):
            # On récupère le contenu de l'image.
            content = img.get('{http://www.w3.org/1999/xlink}href')
            # On regarde si l'image est au format base64. Si oui, on extrait le contenu.
            if content.startswith('data:image/'):
                # On extrait l'image et ses méta-données (seul le format nous intéresse ici).
                meta, img_b64 = content.split(';base64,')
                _, img_format = meta.split('/')
                # On extrait l'image dans le format utilisé avant l'encodage en base64.
                img_file_name = 'img-{index}.{format}'.format(index=index, format=img_format)
                # On remplace le lien vers l'image extraite.
                img.set('{http://www.w3.org/1999/xlink}href', img_file_name)
                # On sauvegarde le fichier.
                with open(img_file_name, 'wb') as img_file:
                    img_file.write(b64decode(img_b64))

        # On sauvegarde notre nouveau SVG.
        with open('svg-without-images.svg', 'w') as svg_file:
            svg_content = etree.tostring(svg)\
                    .decode('utf-8')\
                    .replace('&gt;', '>')
            svg_file.write(svg_content)


    if __name__ == "__main__":
        if len(sys.argv) != 2:
            print_help()
            sys.exit(0)

        extract_images_svg(sys.argv[1])
