Chiffrer des fichiers avec GPG
##############################

:tags: GPG, securité
:lang: en
:slug: gpg-crypt

Here are two bash functions to crypt/decrypt files with `GPG <https://gnupg.org/>`__.

Folder are compressed in a ZIP file before being encrypted.

Crypting leaves original files untouched. So does decrypting.

These functions have their place in your ``.bashrc`` or equivalent.

To crypt: ``gpgcrypt FICHIER``

To decrypt: ``gpgdecrypt FICHIER``

.. code:: bash

    gpgcrypt() {
        if [[ -z "$1" ]]; then
            echo "You must supply an argument"
            return 1
        elif [[ -d "$1" ]]; then
            zip -r "$1.zip" "$1"
            gpg --output "$1.zip.gpg" --symmetric "$1.zip"
        else
            gpg --output "$1.gpg" --symmetric "$1"
        fi
    }

    gpgdecrypt() {
        if [[ -z "$1" ]]; then
            echo "You must supply an argument"
            return 1
        else
            gpg --output "${1%.*}" --decrypt "$1"
            if [[ "$(file --mime-type --brief "${1%.*}")" == 'application/zip' ]]; then
                unzip "${1%.*}"
            fi
        fi
    }
