Extraire le HTML d'un email au format mbox
##########################################

:tags: mail, Python

Voici un petit script Python pour convertir un mail au format mbox en HTML. Pour que le script fonctionne, il faut soit que le corps du message soit du HTML (recommandé) soit que la première pièce jointe du message contienne le message en HTML.

Par défaut, le script traitera tous les fichiers dont l'extension est mbox dans le dossier courant. Vous pouvez lui passer le chemin vers un dossier pour qu'il traite les fichiers de ce dossier. Le HTML est sauvegardé dans un fichier du même nom que l'original, dans le même dossier avec l'extension ``.html``.

.. code:: python

    #!/usr/bin/env python3

    import sys
    from glob import glob
    from mailbox import mboxMessage
    from os.path import join, splitext


    path = '.'
    if len(sys.argv) > 1:
        path = sys.argv[1]

    for path in glob(join(path, '*.mbox')):
        with open(path, 'r') as mail_file:
            message = mboxMessage(mail_file.read())

        content = message.get_payload()
        if isinstance(content, str):
            html = message.get_payload(decode=True).decode('utf-8')
        else:
            html = content[0].get_payload(decode=True).decode('utf-8')
            html = html.replace('charset=iso-8859-1', 'charset=utf-8')

        file_name, _ = splitext(path)
        with open(f'{file_name}.html', 'w') as html_file:
            html_file.write(html)

Vous pouvez aussi le trouver sur github : https://github.com/Jenselme/dot-files-shell/blob/master/bin/extract-html-email.py
