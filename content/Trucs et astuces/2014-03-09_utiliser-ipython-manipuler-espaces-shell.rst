Utiliser ipython pour manipuler des espaces en shell
####################################################

:tags: Bash/Shell, python, ipython

Bash est assez pénible quand à la gestion des espaces. Par défaut, la
boucle for itère sur ce qui est séparé par un saut de ligne ou un
espace. Je n'ai pas encore trouvé de solution satisfaisante en bash pur.
Mais python (ou plutôt ipthon peut venir à la rescousse). Ipython est un
shell interactif pour python. Vous pouvez y appeler n'importe quelle
commande bash directement via la syntaxe !cmd. Avec a = !cmd, la sortie
de cmd est stocké dans une liste constitué de chaque ligne de la sortie.
Pour voir ce qu'aurait contenu a sans l'affectée, il suffit d'utiliser
la syntaxe : !!cmd.

Les présentations étant faites, voici un petit script pour renommer des
fichiers avec des espaces en ipython. Il s'adapte faciliment à d'autres
situations :

.. code-block:: python


    #!/usr/bin/env ipython3

    a = !ls
    # Les variables contiennent des espaces, on les entoure donc avec des guillements
    for file in a:
        !mv "$file" tmp/

