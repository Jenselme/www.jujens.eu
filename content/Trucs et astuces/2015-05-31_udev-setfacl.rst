Use udev to setfacl when mounting a usb drive
#############################################

:lang: en

Write the line below (and adapt it) in ``/etc/udev/rules.d/``:


::

   SUBSYSTEMS=="block",ACTION=="add",KERNEL=="sd?1",RUN+="/usr/bin/setfacl -m    u:apache:r-x /run/media/jenselme"

**Attention:** if the path of the command is not absolute, udev will search of
it in ``/usr/lib/udev``.
