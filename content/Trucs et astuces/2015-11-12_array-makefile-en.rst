Utiliser un tableau dans un Makefile
####################################

:tags: make
:lang: en
:slug: array-makefile

I recently had to launch a make target for multiple arguments. I tried to do it
in a Makefile :

- Pass the arguments to loop over ``portals=geojb,n16``
- Loop in the Makefile

It turned out to be harder than I thought:

.. code:: Makefile

   .PHONY: toto
   toto:
	@echo ${portals}
	IFS=',' && \
	for portal in $${portals}; do \
            echo "$${portal}"; \
	    $(call process_portal, $${portal}); \
	done

   define process_portal
	echo "processing $1"; \
	echo "done"
   endef


Note: it is possible to pass the arguments like: ``portals="geojb n16"``. In
this case, you must remove the line ``IFS=','``.

I noted that it is much easier with `manuel
<https://github.com/ShaneKilkelly/manuel>`_ (a small task launcher written in
fewer than 150 lines of Bash which I decided to use instead of make):

.. code:: bash

          #! /usr/bin/env bash

          function hello {
              for portal in "$@"; do
                  process "${portal}"
              done
          }


          function _process {
              echo "processing $1"
              echo "done"
          }
