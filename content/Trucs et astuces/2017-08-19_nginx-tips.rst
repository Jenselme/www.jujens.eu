nginx tips
##########

:tags: nginx


CORS for multiple domains
=========================

.. code:: nginx

    location ~* \.(?:ttf|ttc|otf|eot|woff|woff2)$ {
        if ( $http_origin ~* (https?://(.+\.)?(domain1|domain2|domain3)\.(?:me|co|com)$) ) {
            add_header "Access-Control-Allow-Origin" "$http_origin";
        }
    }

Source: https://stackoverflow.com/a/27879729/3900519
