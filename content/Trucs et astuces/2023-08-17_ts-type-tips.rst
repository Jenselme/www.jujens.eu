Some cool type tips in TypeScript
#################################
:tags: TypeScript
:lang: en

Here is an unsorted type tips I find useful in TypeScript.
I may update it when I find new ones.
Don't hesitate to share yours in the comments!

Another good start, is to read `this page <https://www.typescriptlang.org/docs/handbook/utility-types.html>`__ from the handbook which list various builtin utility types.
If you are really motivated and want to check your skills, you can try `these challenges <https://github.com/type-challenges/type-challenges>`__: I stopped after some intermediate ones, but even the easy ones will help you practice and learn new things (I sure did!).


.. contents::


Functions related
=================

You can use the builtin ``ReturnType`` type to get the return type of a function:

.. code:: typescript

    const toMyString = (obj: object): string => obj.toString()

    // ToString will be string.
    type ToString = ReturnType<typeof toMyString> 

Similarly, you can use the builtin ``Parameters`` type to get the types of the parameters of a function:

.. code:: typescript

    const toMyString = (obj: object): string => obj.toString()
    
    // ToStringParams will be [obj: object]
    type ToStringParams = Parameters<typeof toMyString>


Array related
=============

You can define tuples (that is arrays with a fixed number of elements of a given type) like this:

.. code:: typescript

    type Coords = [number, number]
    
    const a: Coords = [0, 0]
    // This is a type error.
    const b: Coords = [0, 0, 0]

You can access array properties like this:

.. code:: typescript

    type Length<T extends unknown[]> = T['length']
    const a: [string, string] = ['Hello', 'World']
    // CustomLength will be 2. If a were an array, CustomLength would be a number.
    type CustomLength = Length<typeof a>

You can use array types as array in types:

.. code:: typescript

    type Concat<T extends any[], U extends any[]> = [...T, ...U];
    
    const a: [string, string] = ['Hello', 'World']
    const b: number[] = [1, 100]
    
    // c will be of type [string, string, ...number[]]. If a were an array, c would be of type (string | number)[].
    const c: Concat<typeof a, typeof b> = [...a, ...b]

.. note:: See the `Advanced`_ section below for more on extends.


Object related
==============

* ``Partial<Type>`` will make all properties of a object optional.
* ``Required<Type>`` will make all properties of an object required.
* ``Readonly<Type>`` will make all properties of an object read only.
* ``Pick<Type, Keys>`` allows you to define a new type with only the selected properties.
* ``Omit<Type, Keys>`` allows you to define a new type without the selected properties.


Misc
====

* ``typeof`` allows you to get the type of a variable.
* ``Exclude<UnionType, ExcludedMembers>`` is like ``Omit`` but for union types.
* ``Extract<Type, Union>``constructs a type by extracting from ``Type`` all union members that are assignable to ``Union``.
* ``keyof`` allows you to build a union type from the keys of an object.
* ``extends`` allows you to put constraints on a type. See examples in the `Advanced`_ section.


Advanced
========

You can use ``infer`` to infer types to build more complex ones:

.. code:: typescript

    type MyAwaitable<T> = T extends Promise<infer Value> ? Value : never;
    
    // MyValue is a string.
    type MyValue = MyAwaitable<Promise<string>>
    
.. code:: typescript

    type First<T extends any[]> = T extends [infer P, ...any[]] ? P : never
    
    const a: [string, number] = ['Hello', 1]
    // Value is a string.
    type Value = First<typeof a>
    
.. code:: typescript

    type SavedData = {
        options: {isEnabled: boolean};
        authentication: {username: string};
    }
    
    const saveData = <Key extends keyof SavedData>(key: Key, data: SavedData[Key]): void => {
        localStorage.setItem(key, JSON.stringify(data));
    }
    
    saveData('options', {isEnabled: false})
    saveData('authentication', {username: 'Jujens'})
    // Those will be reported as errors.
    saveData('options', {username: 'Jujens'})
    saveData('authentication', {isEnabled: false})

You can define types with string interpolation to force part of a string to be of a certain type:

.. code:: typescript

    type Width = `${number}px`
    
    const a: Width = '18px'
    // All these will be reported as errors.
    const b: Width = 18;
    const c: Width = '18';
    const d: Width = 'totopx'
