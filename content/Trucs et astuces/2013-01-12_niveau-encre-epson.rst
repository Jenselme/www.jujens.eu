Connaître le niveau d'encre de son imprimante epson
###################################################

:tags: Linux, imprimante

Il suffit de lancer en root :
::

    sudo escputil -i -r /dev/usb/lp0
