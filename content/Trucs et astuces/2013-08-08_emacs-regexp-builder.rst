Emacs :  regexp builder
#######################

:tags: Emacs

Perdu avec une expression régulière ? Emacs peut vous aider ! Il propose en effet un outil intitulé regexp-builder qui vérifie la syntaxe de la regexp durant la frappe et cherche une correspondance dans le buffer courant.

Pour le lancer, tapez simplement ``M-x regexp-builder``. Entrez alors simplement votre regexp entre les guillemets.

Attention toute fois, Emacs étant codé en Lisp, par défaut regexp-builder utilise la syntaxe de Lisp pour les regexp qui diffèrent de la syntaxe habitulle (pour ignorer un caractère, il faut utiliser ``\\``). Heureusement, on peut changer le mode d’édition ! Tapez ``^-c TAB`` et choisissez string pour avoir la syntaxe habituelle.
