Emacs : se connecter en ssh
###########################

:tags: Emacs, ssh, Trucs et astuces

Emacs n’est pas installé sur le seveur où vous devez modifier des
fichiers ? Vous détestez vi et trouvez nano trop limité ? Eh bien vous
pouvez utiliser Emacs avec votre configuration habituelle !

Pour que Emacs se connecte au serveur, tapez simplement : ``^x ^f /ssh2:USER@SEVER``. Emacs va alors se connecter en ssh au serveur et vous
pourrez naviguer dans le dossier home du serveur comme si vous étiez en
local. Vous pouvez évidememt directement préciser quel dossier/fichier
vous désirez ouvrir.

C’est beau non ?
