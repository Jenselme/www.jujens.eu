VirtualBox
##########

:tags: VirtualBox, Virtualisation

.. contents::

Transform img disk (qemu) to vdi (Virtual Box)
==============================================

You need to convert img in raw with qemu-img and then raw to vdi. It will take time.

.. code:: bash

	  $ qemu-img convert WindowsXP.img -O raw WindowsXP.raw
	  $ VBoxManage convertdd WindowsXP.raw $HOME/.VirtualBox/VDI/WindowsXP.vdi


Transform vdi to img
====================

.. code:: bash

	  $ VBoxManage clonehd sdb.vdi sdb.raw --format RAW
	  $ qemu-img convert sdb.raw -O img sdb.img


Compress an image
=================

Prepare Windows guest
---------------------

#. run defrag in the guest (Windows)
#. nullify free space: ``sdelete –z``

Prepare Linux guest
-------------------

.. code:: bash

	  sudo dd if=/dev/zero of=/bigemptyfile bs=4096k
	  sudo rm -rf /bigemptyfile

Compess
-------

#. shutdown the guest VM
#. Run VBoxManage's compact command: ``VBoxManage.exe modifyhd thedisk.vdi --compact``


Resize a disk
=============

.. code:: bash

	  vboxmanage modifyhd /home/CHEMIN/VIRTUALBOX\VOTRE.vdi –resize 80000 # 80 Go HDD


Mount a VDI to the local FS
===========================

Run these commands as root:

#. Enable the proper kernel module: ``modprobe nbd``
#. Create a virtual device with at ``/dev/nbd0`` with ``qemu-nbd -c /dev/nbd0 /PATH/TO/YOUR/VDI/disk.vdi``
#. List the partitions on the disk with ``parted /dev/nbd0 print``
#. Mount the partition in ``/mnt/vdi`` (eg if you want partition 2):

   #. ``mkdir /mnt/vdi``
   #. ``mount /dev/nbd0p2 /mnt/vdi/``

#. Extract the data you need by navigating under ``/mnt/vdi``.
#. Unmount the volume: ``umount /dev/nbd0p2``
#. Disable the virtual drive with ``qemu-nbd --disconnect /dev/nbd0``

**Note:** If you want to mount a LVM volume, you will get an ``mount: /mnt/vdi: unknown filesystem type 'LVM2_member'.`` error. You need to do before instead of the mount step:

#. Identify the LVM volume with ``lvscan``. You should see a path to the LVM volume to mount. ``/dev/fedora/root`` in may case with this output:

   .. code::

		ACTIVE            '/dev/fedora/swap' [3.00 GiB] inherit
  		ACTIVE            '/dev/fedora/root' [<26.00 GiB] inherit

#. You can then mount it with ``mount /dev/fedora/root /mnt/vdi/``
#. To unmount, use ``umount /dev/fedora/root``
#. Disable the virtual drive normally.

Sources:

- https://stackoverflow.com/questions/16893306/how-can-i-extract-files-from-vdi
- http://ask.xmodulo.com/mount-lvm-partition-linux.html
