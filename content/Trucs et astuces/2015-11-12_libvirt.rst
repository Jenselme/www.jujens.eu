Tips for libvirt/virtmanager
############################

:tags: Virtualization, libvirt, virtmanager
:lang: en

.. contents::


Static IP
=========

You can affect a static IP to a guest of a libvirt instance with the following
command:

.. code:: bash

   virsh net-update default add ip-dhcp-host "<host mac='52:54:00:7d:2a:5e' name='leap' ip='192.168.122.42' />" --live --config



Launch Virtual Machine manager without a root password
======================================================

#. Add the users that need to do this to the ``libvirt`` group.
#. Create a new polkit rule in ``/etc/polkit-1/rules.d/10-libvirt.rules``
   containing:

.. code:: javascript

   polkit.addRule(function(action, subject) {
    if (action.id == "org.libvirt.unix.manage"
            && subject.local
            && subject.active
            && subject.isInGroup("libvirt")) {
        return polkit.Result.YES;
     }
    });


Compress QCOW2 images
=====================

Linux guest
-----------

Run in the guest:

#. ``fstrim -av``
#. ``dd if=/dev/zero of=/mytempfile``
#. ``rm -f /mytempfile``

Run on the host:

#. ``mv image.qcow2 image.qcow2_backup``
#. ``qemu-img convert -O qcow2 image.qcow2_backup image.qcow2`` or if you want compression to further reduce the disk size but reduce performance: ``qemu-img convert -O qcow2 -c image.qcow2_backup image.qcow2``

Then boot your machine and check everything is OK.

Windows guest
-------------

Run in the guest:

#. The defragementer tool
#. Download `sdelete <https://docs.microsoft.com/fr-fr/sysinternals/downloads/sdelete>`__
#. Run ``sedelete -z c:``

Run on the host:

#. ``mv image.qcow2 image.qcow2_backup``
#. ``qemu-img convert -O qcow2 image.qcow2_backup image.qcow2`` or if you want compression to further reduce the disk size but reduce performance: ``qemu-img convert -O qcow2 -c image.qcow2_backup image.qcow2``

Then boot your machine and check everything is OK.
