Supprimer la ligne encoding de fichiers python
##############################################

:tags: python, Bash/Shell

Lors du passage à python 3 sur un projet, j'ai décidé de supprimer les lignes
qui donnent l'encodage du fichier. En effet, ils étaient tous en UTF-8 et c'est
l'encodage par défaut sous python 3.

J'ai écrit ce petit script pour automatiser tout ça :

.. code:: bash

   for file in $(find chsdi/ -name \*.py); do
     # Remove coding line
     sed -i '/^# *-\*- .*coding: utf-8 -\*-/d' $file
     # Remove whitespace line after it if it exists
     if head -n 1 $file | grep '^$'; then
       sed -i '1d' $file
     fi
   done
