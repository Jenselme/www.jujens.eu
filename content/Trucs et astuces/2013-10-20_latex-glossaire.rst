LaTeX : glossaire
#################

:tags: LaTeX

Pour mon rapport de stage, j’ai eu besoin de faire un glossaire en LaTeX. Contrairement à ce que l’on pourrait croire, c’est assez simple grâce au paquet glossaries. L’option nonumberlist permet de ne pas avoir les numéros de pages pour lesquelles le terme est utilisé à côté du texte explicatif.

.. code-block:: latex

    \documentclass[12pt]{report}
    \usepackage[frenchb]{babel}
    \usepackage{libertine}
    \usepackage[nonumberlist]{glossaries}Cela fait un moment que je me demande comment changer l’apparence des titres de chapitre sous LaTeX. Parce que avoir un gros Chapitre 1 ce n’est pas toujours ce que je veux.

Heureusement, le paquet titlesec vient à la rescousse. Il ajoute la commande titleformat Ainsi, pour avoir un titre qui ressemble à ça :
    \makeglossaries

    \begin{document}
    \input{includes/glossaire.tex}
    \end{document}

Et dans le fichier glossaire.tex :

.. code-block:: latex

    \newglossaryentry{waffer}{name={waffer}, description={Un waffer est la plaque sur laquelle se trouve les puces. C’est sous cette forme que Gemalto reçoit les puces de son fournisseur}}

