Fusionner deux dépôts git
#########################

:tags: git, Linux

Récemment, j'ai eu besoin de fusionner deux dépôts en un seul. Ça se fait plutôt
bien. Voilà la procédure :

.. code:: bash

	  # On clone les dépots
	  git clone projet1
	  git clone projet2
	  cd projet1

	  # On ajoute le projet2 comme source dans le projet1
	  git remote add projet2 ../projet2
	  git fetch projet2

	  # On liste les branches du dépôt 2
	  git branch -a

	  # On crée dans le dépôt 1 la branche à fusionner (par exemple master)
	  git branch projet2-master projet2/master

	  # On fusionne
	  git merge projet2-master

**Surtout ne pas tentez de faire directement un git pull projet2**


Si vous avez des sous-modules, réinitialisez les avec :

::

   git submodule update --init

L'article (en anglais) qui m'a bien aidé :
http://blog.caplin.com/2013/09/18/merging-two-git-repositories/
