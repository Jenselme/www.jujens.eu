Fichiers service pour seafile
#############################

:tags: systemd, seafile

J'ai écrit des fichiers services pour utiliser seafile plus facilement avec
systemd (à placer dans ``/etc/systemd/system/``). Vous pouvez les télécharger :

- `Seafile <|static|/static/service-files-seafile/seafile.service>`_
- `Seahub <|static|/static/service-files-seafile/seahub.service>`_


.. include:: ../static/service-files-seafile/seafile.service
   :code: ini

.. include:: ../static/service-files-seafile/seahub.service
   :code: ini
