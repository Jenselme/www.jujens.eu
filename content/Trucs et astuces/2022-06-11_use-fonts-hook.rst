React hook to load fonts
########################
:tags: JavaScript, React
:lang: en
:modified: 2023-08-01

If you need to load a specific font in a component, you can use this hook.
It will return ``true`` when the fonts are loaded.
It relies on the `document.fonts API <https://developer.mozilla.org/en-US/docs/Web/API/Document/fonts>`__ to load the fonts.
If the fonts are already loaded, the promise will be fulfilled immediately and the component should be "immediately" re-rendered.

.. code:: javascript

    import { useEffect, useState } from "react";

    export const useFonts = (...fontNames: string[]) => {
      const [isLoaded, setIsLoaded] = useState(false);

      useEffect(() => {
        Promise.all(fontNames.map(
          (fontName) => document.fonts.load(`16px "${fontName}"`)
        )).then(() => {
          setIsLoaded(true);
        });
      }, [fontNames]);

      return isLoaded;
    };

Example usage:

.. code:: javascript

  const areFontsLoaded = useFonts("Mistral", "Freestyle Script");

  if (!areFontsLoaded) {
    return <Loader />;
  }


.. note:: We don't need to check that the API is supported with ``document && document.fonts`` since all browsers support it nowadays. See `here <https://caniuse.com/mdn-api_document_fonts>`__.

As pointed out by `choyeK <https://www.jujens.eu/posts/en/2022/Jun/11/use-fonts-hook/#isso-320>`__, for testing you may want to mock this with:

.. code:: javascript

  Object.defineProperty(document, 'fonts', {
    value: { load: Promise.resolve({}) },
  })
