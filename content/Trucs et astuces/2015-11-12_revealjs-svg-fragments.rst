Afficher un SVG progressivement avec reveal.js
##############################################

:tags: reveal.js, SVG, présentation


Pour cela, il faut commencer par installer un plugin : `svg fragments
<https://gist.github.com/bollwyvl/fe1d2806449487cdf88a>`_. Dans votre
installation reveal, dans le sous dossier plugin, cloner le dépôt :

::

   git clone https://gist.github.com/bollwyvl/fe1d2806449487cdf88a


Ensuite, chargez le plugin :

#. Ajoutez la bibliothèque D3 ``<script src="reveal/js/d3.min.js"></script>`` (à
   `télécharger séparément
   <https://raw.githubusercontent.com/mbostock/d3/master/d3.min.js>`_)
#. Activez le plugin en ajoutant dans l'objet de configuration de reveal (celui
   passé à ``Reveal.initialize``) :

   .. code:: javascript

             {src: 'reveal/plugin/svg-fragments/reveal-svg-fragment.js', condition: function () {
                            return !!document.querySelector('[data-svg-fragment]');
                        }}

Puis dans votre SVG ajoutez un tag comme celui-ci ``inkscape:label="fragment1"``
sur les éléments à faire apparaître progressivement. Vous pouvez voir un
`exemple complet sur github
<https://gist.githubusercontent.com/bollwyvl/fe1d2806449487cdf88a/raw/9ab4c5e38f1ea68b3afaf0c1dee998a89229a8d4/test.svg>`_.

Enfin, ajoutez le code correspondant dans votre html. Par exemple, pour le
``test.svg`` précédent :

.. code:: html

          <div data-svg-fragment="test.svg#[*|label=base]">
            <a class="fragment" title="[*|label=fragment1]"></a>
            <a class="fragment" title="[*|label=fragment2]"></a>
          </div>

Disponible `complètement sur github
<https://gist.githubusercontent.com/bollwyvl/fe1d2806449487cdf88a/raw/9ab4c5e38f1ea68b3afaf0c1dee998a89229a8d4/index.html>`_.
