How to cache Python module in Gitlab CI
#######################################

:tags: gitlab, ci, Python
:lang: en

By default, pip cache will be in ``~/.pip``. However, this folder cannot be cached by Gitlab. The trick is to force pip to use a folder located in the build directory with the ``--cache-dir`` option. You can then cache this folder.

For instance, you can use ``.pip`` as in the ``.gitlab-ci.yml`` excerpt below:

.. code:: yaml

    image: python:3.6

    cache:
      paths:
        - .pip

    before_script:
      - mkdir -p .pip
      - pip install -U pip
      - pip --cache-dir=.pip install -r requires.txt
      - pip --cache-dir=.pip install -r tests_requires.txt
