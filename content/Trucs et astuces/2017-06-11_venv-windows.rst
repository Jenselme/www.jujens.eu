Create python virtual enviroments on Windows
############################################

:tags: Python

#. Before creating the venv you will need to open a PowerShell terminal as root and run the commands below to allow the script that activates the virtual env to run:

   .. code::

      cd ..
      Set-ExecutionPolicy Unrestricted

#. Create the venv. Run in a terminal as a normal user: ``python3 -m venv .venv`` If the creation fails due to ``python3 not found``, check that:

   - python3 was added to your your PATH during install.
   - Make a copy of your ``python.exe`` executable into ``python3.exe``

#. Enable the venv: ``.venv\Scripts\activate``
#. Install the dependencies: ``pip install -r requires.txt``
#. Install the tests dependencies: ``pip install -r tests_requires.txt``
#. To install libraries that requires compiled files (like `lxml <http://lxml.de>`__) you can use the wheel files. Most libraries have such a file on pypi so you can install them directly with pip. If not, check `this site <http://www.lfd.uci.edu/~gohlke/pythonlibs/#lxml>`__ to find the proper wheel file for your platform and your version of Python. Then to install it, download it and run ``pip install \path\to\the\weeh.whl``
