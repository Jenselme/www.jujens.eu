Manage deployment transitions for static application
====================================================
:tags: Docker, Kubernetes
:lang: en

When you deploy a frontend app, most of the time the name of your assets contains their hash so you can easily cache the files.
So instead of just having ``main.js`` you will have something like ``main.1234.js``.
The problem is that your HTML will reference ``main.1234.js``, so on the next deploy, once this file is rebuilt if it was changed, it will be named something like ``main.5678.js``.
If you were to load all you files immediately when the browser opens your ``index.html`` this wouldn't be a problem: your user either already has the old file or will load the new one.

.. note:: Depending on your build process, this can concern JS, CSS, images or video files.

However, on big JS apps, your application is decomposed in many small chunks that are loaded on demand.
So you could get in a situation where the user starts using the app in the old version that references ``main.1234.js``.
You deploy your application the old ``main.1234.js`` file is removed.
Now your user needs to download ``main.1234.js`` but it doesn't exist any more.
Ouch.

To avoid that, the idea is to keep serving previous files of your applications.
So instead of removing them right away, we keep them around for a given amount of time.
I think a week is a good compromise between assets to keep (and that takes space) and user session.
Of course, user sessions can be way longer that that.
For these fringe cases, we accept the break.

Depending on how you deploy your application, I think there are two main strategies for that:

#. You copy build files on a server: in this case, copy the new files and then run ``find . -mtime +7 -delete`` in the folder in which you deployed the files. This will delete all files that haven't been modified since more than one week. Since each time you build a file that hasn't changed it will get the same hash, it will get modified on the copy if it still exists and thus won't be deleted.
#. You build the files into some archive (a docker image for instance) and you deploy this: you must restore old files from a cache to have them in your next deployment. This is this more complex technique (when coupled with Docker) that I will discuss in more detail further.

Given my constraints, I decided to store the previous files into a Docker image and use multi-stage build to copy them after building the new files.
My Dockerfile looks like this:

.. code:: Dockerfile
    :number-lines:

    FROM mydocker-registry.io/my-image:latest AS previous-assets
    # Create directory if it doesn't exist.
    # This should only happen during the first build when the cache is empty.
    RUN mkdir -p /var/www/frontend-app/


    # Build the application.
    FROM node:14.16.0-slim AS builder
    WORKDIR /app

    COPY . ./
    # Copy the previous assets into a dedicated folder so our new build won't delete
    # them by accident.
    COPY --from=previous-assets /var/www/frontend-app/ ./previous-builds/

    RUN yarn install --frozen-lockfile
    RUN yarn build && \
        # Restore static files from previous builds. Use --no-clobber to avoid overriding files from
        # current build (this option is not available from the cp command in alpine). This option is
        # not available to the COPY command. This way, existing files will have an updated modification
        # time.
        # Use --archive to be sure to preserve their modification time so we can correctly delete
        # them later.
        cp -R --no-clobber --archive ./previous-builds/* ./build/ && \
        # Delete source maps.
        find build -name \*.js\*.map -type f -delete && \
        # Delete files older than 7 days
        find build -mtime +7 -type f -delete


    # Run the app in nginx.
    FROM nginx:latest AS runner
    RUN mkdir -p /var/www/frontend-app
    WORKDIR /var/www/frontend-app

    COPY --from=builder /app/build /var/www/frontend-app/

The build steps are then like this:

#. Pull or create the asset image with a custom scripts: ``create-assets-image.sh mydocker-registry.io/my-image``. The script is:

   .. code:: bash
    :number-lines:

    #!/usr/bin/env bash

    set -e
    set -u
    set -o pipefail

    ASSETS_IMAGE_NAME="${1:-previous-assets}"
    ASSETS_IMAGE_TAG=latest
    readonly ASSETS_IMAGE_NAME
    readonly ASSETS_IMAGE_TAG

    # If we don't already have the image for previous assets, we create it to be sure to
    # have this base image.
    if ! docker pull "${ASSETS_IMAGE_NAME}:${ASSETS_IMAGE_TAG}"; then
        docker pull nginx:latest
        docker tag nginx:latest "${ASSETS_IMAGE_NAME}:${ASSETS_IMAGE_TAG}"
    fi

#. Build the images and tag them: ``docker build --tag mydocker-registry.io/my-image:$COMMIT_SHA --tag mydocker-registry.io/my-image:latest .``. I need to tag two images with the same content. One will be the production image I deploy and contains the commit hash in its version. The other one will always have the same name so I know which image to use to fetch the previous assets.
#. Push the image: ``docker push mydocker-registry.io/my-image:$COMMIT_SHA mydocker-registry.io/my-image:latest``

.. note:: To avoid issue with the ``latest`` tag and always have file from the previous builds, you must wait for a build to end before starting a new one.
