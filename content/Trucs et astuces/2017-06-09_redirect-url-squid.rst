Use squid and squidGuard to redirect all URLs from a domain to another one
##########################################################################

:tags: squid, Linux
:lang: en

.. contents::

You may find yourself in a situation where you will need to redirect all URLs from a domain (lets say www.example.com) to another one (lets say www.example.org). This can be done with squid, a proxy server, and squidGuard, an extension to squid designed to do redirections. In this article I will deal only with redirections of HTTP requests made to that domain.

First install ``squid`` and ``squidGuard`` on your machine (you should be able to find packages for them in the repository of your distribution).

Configure squid
===============

We need to tweak a bit the default configuration for it to fit our needs. The config file should be located at ``/etc/squid/squid.conf``.

First we need to disable the ``forwarded_for`` feature by adding ``forwarded_for off`` in the config file. We also need to the name of the proxy with ``visible_hostname fastolfe.localdomain``.

Then we need to configure the program used to rewrite the URL (you may need to change the path to the ``squidGuard`` executable and config file): ``url_rewrite_program /usr/bin/squidGuard -c /etc/squid/squidGuard.conf``.

At last but not least, we must allow all connections that are not HTTP or that are on different ports to be passed untouched by the proxy. To achieve that, we need to add the following ACLs (add them below the other ones and before ``acl CONNECT method CONNECT``):

.. code:: squid

    acl Safe_ports port 993         # secured imap
    acl Safe_ports port 587         # secured smpt
    acl Safe_ports port 25          # default smtp
    acl Safe_ports port 465         # gmail smtp
    acl Safe_ports port 53          # DNS

We then need to change the ``http_access`` rule to allow not only HTTPS on port 443 but all the other ones. To do that, replace ``http_access deny CONNECT !SSL_ports`` by ``http_access deny CONNECT !Safe_ports``.

Since all your HTTP request will go through the proxy, you may want to disable the log to prevent them to use lot of disk space. To do that, add to the config file:

::

    access_log none
    cache_log none
    cache_log none
    logfile_rotate 0

You can download my full configuration file `here <|static|/static/squid/squid.conf>`__.


Configure squidGuard
====================

We will first need to write the main configuration file that should be at ``/etc/squid/squidGuard.conf``. If the file already exists, I advise you to replace its content by the code below. You will have to adapt it to mach your needs.

.. code:: squid

    #
    # CONFIG FILE FOR SQUIDGUARD
    #

    # Where to store the database of the rules.
    dbhome /etc/squid
    # Log dir. You may need to create the directory manually, create the /var/log/squidGuard/squidGuard.log file and give it to the squid user.
    logdir /var/log/squidGuard

    #
    # REWRITE RULES:
    #

    rew example {
        # Rewrite (replace in the URL) www.example.com by www.example.org
        s@://www.example.com/@://www.example.org/@i
    }

    #
    # SOURCE ADDRESSES:
    #

    src local {
        # Define what source addresses the proxy must proxy. Here we limit ourselves to local host.
        ip 127.0.0.1
    }

    #
    # DESTINATION CLASSES:
    #

    dest local {
    }


    acl {
        default {
            # Rewrite the URL based on the example rewrite rule.
            rewrite example
            # Don't block any addresses, configure the proxy to pass everything.
            pass	local all
        }
    }

To test the configuration run:

.. code:: bash

    echo 'http://www.example.com/some/url - - GET' | squidGuard -c squidGuard.conf -d

You should get something like:

::

    2017-06-10 17:07:41 [22756] New setting: dbhome: /etc/squid
    2017-06-10 17:07:41 [22756] New setting: logdir: /var/log/squidGuard
    2017-06-10 17:07:41 [22756] destblock local missing active content, set inactive
    2017-06-10 17:07:41 [22756] squidGuard 1.4 started (1497107261.140)
    2017-06-10 17:07:41 [22756] squidGuard ready for requests (1497107261.141)
    2017-06-10 17:07:41 [22756] source not found
    2017-06-10 17:07:41 [22756] no ACL matching source, using default
    OK rewrite-url="http://www.example.org/some/url"
    2017-06-10 17:07:41 [22756] squidGuard stopped (1497107261.141)

If you use another URL, you should get this response:

::

    2017-06-10 17:07:49 [22759] New setting: dbhome: /etc/squid
    2017-06-10 17:07:49 [22759] New setting: logdir: /var/log/squidGuard
    2017-06-10 17:07:49 [22759] destblock local missing active content, set inactive
    2017-06-10 17:07:49 [22759] squidGuard 1.4 started (1497107269.252)
    2017-06-10 17:07:49 [22759] squidGuard ready for requests (1497107269.252)
    2017-06-10 17:07:49 [22759] source not found
    2017-06-10 17:07:49 [22759] no ACL matching source, using default
    ERR
    2017-06-10 17:07:49 [22759] squidGuard stopped (1497107269.252)

You can download my full configuration file `here <|static|/static/squid/squidGuard.conf>`__.


Activate squid
==============

You can now enable and start the proxy: ``systemctl enable squid`` and ``systemctl start squid``.

You now need to configure your software to use it. For instance, if you use NetworkManager, to make all requests go through it:

#. Open the network settings.
#. Go under *Network proxy*.
#. Select *Manual* in *Method*.
#. Fill the first field of *HTTP Proxy* with ``localhost`` and put ``3128`` in the second one.

That's it. If you have a problem or a question, please leave a comment.
