Se connecter à une machine virtuelle sous VirtualBox
####################################################

:tags: VirtualBox, Virtualisation

J'ai récemment eu besoin de créer un serveur pour faire des tests de déploiement
en production pour le travail. Ce serveur est une machine virtuelle créée avec
`VirtualBox <http://virtualbox.org/>`_ sous Fedora 22.

Pour que les tests soient concluant, je dois pouvoir me connecter à cette
machine via SSH. Voici la méthode que j'ai suivie :

#. Dans *File > Preferences > Network*, sous l'onglet *Host-only Networks*,
   cliquez sur le bouton ajouter. Cette adaptateur sera utilisé par l'hôte et
   l'invité pour être sur un même réseau privé.
#. Dans les préférences de la machines, allez dans *Network*. Activez un
   adaptateur non encore utiliser et paramétrez le comme suit :

   #. *Attached to:* choisissez *Host-only Adapter*
   #. *Name* devrait être mis automatiquement à l'adaptateur créé
      précédemment. Si ce n'est pas le cas, corriger le paramètre.

On peut maintenant démarrer la machine virtuelle. Dans une console, tapez ``ip
a`` pour récupérer son adresse ip. Vous pouvez l'utiliser pour vous connectez à
la machine virtuelle. Vous pouvez également la renseigner dans ``/etc/hosts``
pour avoir un nom plus facile à retenir et faciliter la configuration de votre
serveur HTTP.

Et c'est tout !
