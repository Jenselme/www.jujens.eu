LaTeX : de jolis titres
#######################

:tags: LaTeX

Cela fait un moment que je me demande comment changer l’apparence des titres de chapitre sous LaTeX. Parce que avoir un gros Chapitre 1 ce n’est pas toujours ce que je veux.

Heureusement, le paquet titlesec vient à la rescousse. Il ajoute la commande titleformat Ainsi, pour avoir un titre qui ressemble à ça :

.. image:: /images/LaTeX_titlesec.png
    :alt: Rendu du titre

Utilisez simplement :

.. code-block:: latex

    \documentclass[12pt, openany]{report}
    \usepackage[frenchb]{babel}
    \usepackage{libertine}
    \usepackage{titlesec}

    \newcommand{\hsp}{\hspace{20pt}}

    \titleformat{\chapter}[hang]{\Huge\bfseries\sffamily}{\thechapter\hsp}{0pt}{\Huge\bfseries\sffamily}
    \begin{document}
    \chapter{Introduction}
    \end{document}

Cela enlève à la fois le chapter et modifie la police pour avoir celle qui me convient.

Citons également le paquet fncychap et le type de document memoire (\documentclass{memoir}) qui changent également l’apparence des titres.

Source
------
Pour voir encore plus de titre : http://texblog.org/2012/07/03/fancy-latex-chapter-styles/
