Utiliser un service webdav avec curl
####################################

:tags: Linux, Bash/Shell, Webdav, curl

Il est tout à fait possible (et c'est même facile) d'utiliser ``curl`` pour
communiquer avec un service webdav. Attention toute fois, tous les événements
sont contenus dans des fichiers séparés qui doivent contenir les lignes
``BEGIN:VCALENDAR`` et ``END:VCALENDAR`` en plus des lignes décrivant
l'évènement en lui même.

Pour recevoir la liste au format xml des événements, il suffit de faire :

.. code:: bash

   curl -k --user "${login}:${password}" -X PROPFIND "https://owncloud.jujens.eu/remote.php/caldav/calendars/jujens/defaultcalendar"

Pour poster un nouvel événement ou mettre à jour un événement existant, il
suffit de faire une requête put à l'url du service complétée par un nom de
fichier. Si le fichier existe, on met à jour l'événement correspondant. Sinon on
en crée un nouveau.

.. code:: bash

   curl -k --user "${login}:${password}" -X PUT -H "Content-Type: text/calendar; charset=utf-8" -d "$a" "https://owncloud.jujens.eu/remote.php/caldav/calendars/jujens/omis/<mon-fichier.ics>"

Idem pour la suppression :

.. code:: bash

	  curl -k --user "${login}:${password}" -X DELETE -H "Content-Type: text/calendar; charset=utf-8" "https://owncloud.jujens.eu/remote.php/caldav/calendars/jujens/omis/<mon-fichier.ics>"
