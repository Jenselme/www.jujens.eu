Some tips to deploy Django in kubernetes
========================================
:tags: devops, k8s, kubernetes, Django, Python
:lang: en
:modified: 2022-03-09

I am not going to go into details in this article about how you can deploy Django in kubernetes.
I am just going to highlight the main points you should pay attention to when deploying a Django app.
I expect you to have prior knowledge about how to deploy an application in kubernetes using `Helm <https://helm.sh>`__.
I hope you will still find useful pieces of information in this article.

.. contents:: Contents


Deploying the application
-------------------------

- Always disable the ``DEBUG`` mode with ``DEBUG=False`` in your settings. That's the case for all Django deployments not matter how you do it.
- Don't use the the Django dev server to launch your application (that's the ``python manage.py server`` command), rely on ``gunicorn`` or something equivalent instead (like you normally would).
- Rely on environment variables to inject configurations into your settings files. You can use `django-environ <https://django-environ.readthedocs.io/en/latest/>`__ to help you read, validate and parse them.

  - Store secrets into kubernetes secrets. That includes: the ``SECRET_KEY`` configuration value, your database connection details, API keys…
  - Store everything else into a ``ConfigMap`` managed by Helm.

- Configure ``livenessProbe`` to detect issues with your applications and allow kubernetes to correctly restart the pod if needed.
- You may want to add a nginx sidecar container to buffer some requests like file uploads. By default, when you deploy Django into kubernetes, the request will hit ``gunicorn`` directly. In the case of long file uploads, it means the ``gunicorn`` worker that handles this request cannot do anything until the upload is done. This can be a problem and may result in container restarts (because kubernetes cannot check the liveness probe) or request timeouts. A good way to avoid that, is to put a nginx server in front of ``gunicorn`` like you would do if you weren't on kubernetes. The sidecar pattern is a common way to do that. Just make sure your service will route traffic to nginx and not to gunicorn. Normally, this can be done by changing the port it must route traffic to to 80.

  - If you use async Django, you should already be good without nginx. Sadly, at this time, the ORM doesn't support async yet so it limits where you can apply this pattern, meaning you probably will need nginx.
  - You could also use gevent workers, but this involves patching the standard library, so I'm not a fan and don't advise it.
  - You may be able to configure a ngnix ingress at cluster level. However, after some tests, I didn't succeed to correctly configure it. So I decided to use a nginx sidecar which is a much easier pattern to deal with.

- Don't run ``gunicorn`` as root in the container to limit the surface of attack.
- Use an ``initContainer`` to run your migrations.
- Give your containers resource quotas to avoid any of them using too much resources.
- Put the static files into a bucket or let nginx serve them. See `this article <{filename}./2021-03-31_manage-static-k8s-django.rst>`__.

Configurations
++++++++++++++

To help you put this into practice, here are some configuration samples.

Nginx sidecar configuration
###########################

It's a very standard reverse proxy configuration.

.. code:: yaml
    :number-lines:

    apiVersion: v1
    kind: ConfigMap
    metadata:
    name: backend-api-nginx
    data:
    api.conf: |
        upstream app_server {
            # All containers in the same pod are reachable with 127.0.0.1
            server 127.0.0.1:{{ .Values.container.port }} fail_timeout=0;
        }


        server {
            listen 80;
            root /var/www/api/;
            client_max_body_size 1G;

            access_log stdout;
            error_log  stderr;

            location / {
                location /static {
                    add_header Access-Control-Allow-Origin *;
                    add_header Access-Control-Max-Age 3600;
                    add_header Access-Control-Expose-Headers Content-Length;
                    add_header Access-Control-Allow-Headers Range;

                    if ($request_method = OPTIONS) {
                        return 204;
                    }

                    try_files /$uri @django;
                }

                # Dedicated route for nginx health to better understand wher problems come from if needed.
                location /nghealth {
                    return 200;
                }

                try_files $uri @django;
            }

            location @django {
                proxy_connect_timeout 30;
                proxy_send_timeout 30;
                proxy_read_timeout 30;
                send_timeout 30;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                # We have another proxy in front of this one. It will capture traffic
                # as HTTPS, so we must not set X-Forwarded-Proto here since it's already
                # set with the proper value.
                # proxy_set_header X-Forwarded-Proto $schema;
                proxy_set_header Host $http_host;
                proxy_redirect off;
                proxy_pass http://app_server;
            }
        }


deployment.yaml
###############

.. code:: yaml
    :number-lines:

    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: {{ include "chart.fullname" . }}
      labels:
    {{ include "chart.labels" . | indent 4 }}
    spec:
      selector:
        matchLabels:
          app.kubernetes.io/name: {{ include "chart.name" . }}
          app.kubernetes.io/instance: {{ .Release.Name }}
      template:
        metadata:
          labels:
            app.kubernetes.io/name: {{ include "chart.name" . }}
            app.kubernetes.io/instance: {{ .Release.Name }}
        spec:
          containers:
            - name: {{ .Chart.Name }}
              image: "{{ .Values.container.image.repository }}:{{ .Values.container.image.tag }}"
              imagePullPolicy: {{ .Values.container.image.pullPolicy }}
              securityContext:
                privileged: false
                runAsUser: 1001
                runAsGroup: 1001
                # Required to prevent escalations to root.
                allowPrivilegeEscalation: false
                runAsNonRoot: true
              envFrom:
              - configMapRef:
                  name: {{ .Chart.Name }}
                  optional: true
              - secretRef:
                  name: {{ .Chart.Name }}
                  optional: true
              ports:
                - name: http
                  containerPort: {{ .Values.container.port }}
                  protocol: TCP
              resources:
                limits:
                  memory: {{ .Values.container.resources.limits.memory }}
                  cpu: {{ .Values.container.resources.limits.cpu }}
                requests:
                  memory: {{ .Values.container.resources.requests.memory }}
                  cpu: {{ .Values.container.resources.requests.cpu }}
              {{ if .Values.container.probe.enabled -}}
              # As soon as this container is alive, it can serve traffic, so no need for a readinessProbe.
              # We still need a bit for it to start before trying to consider it alive: gunicorn must
              # start its workers and open connections to the database.
              livenessProbe:
                httpGet:
                  path: {{ .Values.container.probe.path }}
                  port: {{ .Values.container.port }}
                timeoutSeconds: {{ .Values.container.probe.livenessTimeOut }}
                initialDelaySeconds: {{ .Values.container.probe.initialDelaySeconds }}
              {{- end }}
              volumeMounts:
                - name: backend-credentials
                  mountPath: /secrets/backend
                  readOnly: true
                - name: staticfiles
                  mountPath: /var/www/api/
                  # The API must be able to copy the files to the volume.
                  readOnly: false
            - name: nginx-sidecar
              image: nginx:stable
              imagePullPolicy: Always
              securityContext:
                privileged: false
                # Nginx must start as root to bind the proper port in the container.
                allowPrivilegeEscalation: true
                runAsNonRoot: false
              ports:
                - name: http
                  containerPort: {{ .Values.service.port }}
                  protocol: TCP
              volumeMounts:
                - name: nginx-conf
                  mountPath: /etc/nginx/conf.d
                  readOnly: true
                - name: staticfiles
                  mountPath: /var/www/api/
                  readOnly: true
              {{ if .Values.sidecar.nginx.probe.enabled -}}
              livenessProbe:
                httpGet:
                  # When we can access this route, nginx is alive, but it is not ready (ie cannot serve
                  # traffic yet).
                  path: {{ .Values.sidecar.nginx.probe.path }}
                  port: {{ .Values.service.port }}
                timeoutSeconds: {{ .Values.sidecar.nginx.probe.livenessTimeOut }}
              readinessProbe:
                httpGet:
                  # The container cannot be ready (that is accepting traffic) until it can talk to the
                  # container. So we need to pass through nginx (with the port) to the container (with
                  # the path) to check this.
                  # Since it can take a few seconds, we have an initialDelaySeconds.
                  path: {{ .Values.container.probe.path }}
                  port: {{ .Values.service.port }}
                initialDelaySeconds: {{ .Values.sidecar.nginx.probe.initialDelaySeconds }}
                timeoutSeconds: {{ .Values.sidecar.nginx.probe.livenessTimeOut }}
              {{- end }}
              resources:
                limits:
                  memory: {{ .Values.container.resources.limits.memory }}
                  cpu: {{ .Values.container.resources.limits.cpu }}
                requests:
                  memory: {{ .Values.initContainer.resources.requests.memory }}
                  cpu: {{ .Values.initContainer.resources.requests.cpu }}
          {{ if .Values.initContainer.enabled -}}
          initContainers:
            - name: {{ .Values.initContainer.name }}
              image: "{{  .Values.container.image.repository }}:{{ .Values.container.image.tag }}"
              imagePullPolicy: {{ .Values.container.image.pullPolicy }}
              envFrom:
                - configMapRef:
                    name: {{ .Chart.Name }}
                    optional: true
                - secretRef:
                    name: {{ .Chart.Name }}
                    optional: true
              resources:
                limits:
                  memory: {{ .Values.initContainer.resources.limits.memory }}
                  cpu: {{ .Values.initContainer.resources.limits.cpu }}
                requests:
                  memory: {{ .Values.initContainer.resources.requests.memory }}
                  cpu: {{ .Values.initContainer.resources.requests.cpu }}
          {{- end }}
          volumes:
            - name: nginx-conf
              configMap:
                name: backend-api-nginx
            - name: staticfiles
              emptyDir: {}
            - name: backend-credentials
              secret:
                secretName: {{ .Values.gcp.backend.credentials.secret }}


The Dockerfile and related scripts
##################################

.. code:: Dockerfile
    :number-lines:

    # Don't use alpine based images: Python was designed for glibc and is very slow in them.
    # Always use the -slim images if you can: they are the best compromise between performance and image size.
    FROM python:3.8-slim
    ENV PYTHONPATH /code
    # This is to print directly to stdout instead of buffering output
    ENV PYTHONUNBUFFERED 1
    ARG BUILD_RELEASE=undefined
    ENV RELEASE=$BUILD_RELEASE
    RUN pip install pipenv

    WORKDIR /code

    COPY Pipfile ./
    COPY Pipfile.lock ./
    COPY scripts/django-entrypoint.sh scripts/django-install.sh scripts/setup-django-run-as-non-root.sh scripts/run-django-production.sh /usr/local/bin/
    RUN /usr/local/bin/django-install.sh prod
    RUN pip install dumb-init

    COPY myapp/ ./myapp/
    COPY manage.py .
    COPY pyproject.toml .
    COPY tox.ini .

    # Create non-root user and configure it for the project to run correctly with it.
    RUN /usr/local/bin/setup-django-run-as-non-root.sh

    ENTRYPOINT ["/usr/local/bin/dumb-init", "--"]
    CMD ["/usr/local/bin/django-entrypoint.sh", "/usr/local/bin/run-django-production.sh"]

``django-install.sh``:

.. code:: bash
    :number-lines:

    #!/usr/bin/env bash

    set -e
    set -u
    set -o pipefail

    ENV="${1:-prod}"
    readonly ENV

    echo "Installing deps for env ${ENV}"

    apt-get update
    # We must install some deps from git, hence the need to install git.
    # You may not need this and you may need to install extra libs.
    apt-get install -y git
    if [[ "${ENV}" == 'prod' ]]; then
        pipenv install --system --deploy
    else
        pipenv install --dev --system --deploy
    fi
    apt-get auto-remove -y git
    apt-get clean

``setup-django-run-as-non-root.sh``:

.. code:: bash
    :number-lines:

    #!/usr/bin/env bash

    set -e
    set -u
    set -o pipefail

    # In production, we won't start the container as root so we compile the pyc files
    # and to prepare the collect static while we can write files.
    # We wont be able to do this after once we lost write access to code folders.
    # uuid cannot be 1000 otherwise chown won't work.
    # This must match what we use in the securityContext of the pod.
    groupadd --gid 1001 gunicorn
    useradd gunicorn --uid 1001 --gid 1001
    mkdir static
    chown gunicorn:gunicorn static
    python -m compileall myapp

``django-entrypoint.sh``:

.. code:: bash

    #!/bin/bash

    set -o errexit
    set -o pipefail
    set -o nounset

    postgres_ready() {
        python << END
        import sys
        import psycopg2
        try:
            psycopg2.connect(
                dbname="${DB_NAME}",
                user="${DB_USER}",
                password="${DB_PASSWORD}",
                host="${DB_HOST}",
                port="${DB_PORT}",
            )
        except psycopg2.OperationalError:
            sys.exit(-1)
        sys.exit(0)
        END
    }
    until postgres_ready; do
    >&2 echo 'Waiting for PostgreSQL to become available...'
    sleep 1
    done
    >&2 echo 'PostgreSQL is available'

    exec "$@"

``run-django-production.sh``:

.. code:: bash

  #!/usr/bin/env bash

  set -o errexit
  set -o pipefail
  set -o nounset

  mkdir -p /var/www/api/
  cp -R static /var/www/api/
  gunicorn --bind :8000 --workers 1 myapp.wsgi


Handling commands
-----------------

You can run commands at regular intervals with `CronJob <https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/>`__.
To avoid the need to create one file per CronJob, you can loop over values as described `here <https://www.padok.fr/en/blog/kubernetes-cronjob-helm-templates>`__.
In a nutshell, you can combine this ``cronjobs.yaml`` Helm template:

.. code:: yaml

    {- range $job, $val := .Values.cronjobs }}
    apiVersion: batch/v1beta1
    kind: CronJob
    metadata:
    name: "{{ .name }}"
    spec:
    schedule: "{{ .schedule }}"
    jobTemplate:
        spec:
        template:
            spec:
            containers:
            - name: "{{ .name }}"
                image: "{{ $.Values.container.image.repository }}:{{ $.Values.container.image.tag }}"
                imagePullPolicy: "{{ $.Values.container.image.pullPolicy }}"
                args:
                - python
                - manage.py
                - "{{ .djangoCommand }}"
                envFrom:
                - configMapRef:
                    name: {{ $.Chart.Name }}
                    optional: true
                - secretRef:
                    name: {{ $.Chart.Name }}
                    optional: true
            restartPolicy: "{{ .restartPolicy }}"
    ---
    {{- end}}
    
With this configuration:

.. code:: yaml

    # We currently assume we run the API Python/Django image for all jobs.
    cronjobs:
        "0":
            name: backend-api-clearsessions
            # This must be in the standard Unix crontab format
            schedule: "0 23 * * *"
            djangoCommand: clearsessions
            restartPolicy: Never
        "1":
            name: backend-api-clean-pending-loan-applications
            schedule: "0 23 1 * *"
            djangoCommand: remove_stale_contenttypes
            restartPolicy: Never

To create two CronJob in kubernetes: one for ``python manage.py clearsessions`` launched every day at 23:00 and one for ``python manage.py remove_stale_contenttypes`` launched every fist day of each month at 23:00.


History
-------

* 9th of March 2022: corrected static files volume mounts and copy of static files to volumes. Thanks to `RomoSapiens <https://www.jujens.eu/posts/en/2021/Mar/29/deploy-django-kubernetes/#isso-281>`__ for the catch.
