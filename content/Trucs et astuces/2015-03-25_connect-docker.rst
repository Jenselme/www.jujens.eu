Connect to a running docker container
#####################################

:tags: Docker

If you are running docker 1.3 or above, you should use: ``docker exec -it CONTAINER COMMAND`` to run ``COMMAND`` within the container. You can easily create a function to ease the thing and run bash by default:

.. code:: bash

    dk-enter() {
        docker exec -it "$1" "${2:-/bin/bash}"
    }

Otherwies, you can easily open a shell in a running docker container with the following commands:

- Get the container pid: ``docker inspect -f {{.State.Pid}} <container-id>``
- Connect: ``nsenter --target <pid> --mount --uts --ipc --net --pid``
