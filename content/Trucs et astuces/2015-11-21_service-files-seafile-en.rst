Fichiers service pour seafile
#############################

:slug: service-files-seafile
:tags: systemd, seafile
:lang: en

I wrote service files to use seafile easily with systemd (put them in
``/etc/systemd/system/``). You can download them:

- `Seafile <|static|/static/service-files-seafile/seafile.service>`_
- `Seahub <|static|/static/service-files-seafile/seahub.service>`_


.. include:: ../static/service-files-seafile/seafile.service
   :code: ini

.. include:: ../static/service-files-seafile/seahub.service
   :code: ini
