Remove the encoding line in python files
########################################

:lang: en
:slug: remove-encoding-python
:tags: python, Bash/Shell

When making a python project python 3 only, I decided to delete the lines giving
the encoding of the files. All files were in UTF-8 and it is the default
encoding in python 3.

I wrote this little script to get the job done.

.. code:: bash

   for file in $(find chsdi/ -name \*.py); do
     # Remove coding line
     sed -i '/^# *-\*- .*coding: utf-8 -\*-/d' $file
     # Remove whitespace line after it if it exists
     if head -n 1 $file | grep '^$'; then
       sed -i '1d' $file
     fi
   done
