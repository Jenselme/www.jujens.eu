Use dnsmasq with NetworkManager
###############################

:tags: linux
:lang: en

This will, for instance, allow you to redirect all matching domains to a certain server: dnsmasq will intercept the DNS query and return the ip you specified.

To do this, edit NetworkManager configuration in ``/etc/NetworkManager/NetworkManager.conf`` and add in the ``main`` section ``dns=dnsmasq``:

.. code:: ini

    [main]
    dns=dnsmasq

You can then create a file in ``/etc/NetworkManager/dnsmasq.d/redirect.conf`` to redirect the domain you want. For instance to redirect ``jujens.test`` to localhost host, add this line:

.. code:: ini

    address=/jujens.test/127.0.0.1
