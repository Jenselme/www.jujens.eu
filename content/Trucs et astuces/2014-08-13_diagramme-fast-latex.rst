Réaliser des diagramme FAST en LaTeX
####################################

:tags: LaTeX, diagramme FAST

.. role:: latex(code)
	  :language: latex
	  :class: highlight

J'ai récemment eu besoin d'insérer des diagrammes FAST dans un rapport en
LaTeX. J'ai d'abord pensé les réaliser sur un logiciel de diagramme comme `dia
<http://en.wikipedia.org/wiki/Dia_%28software%29>`_ pour l'insérer comme une
image et puis j'ai découvert le paquet `fast-diagram
<http://www.tug.org/texlive//devsrc/Master/texmf-dist/doc/latex/fast-diagram/help.pdf>`_
qui définit un environnent et des commandes LaTeX pour réaliser un joli
diagramme FAST facilement.

C'est très simple d'utilisation, on importe le paquet avec
:latex:`\usepackage[raccourcis]{fast-digram}` et ensuite :

.. code:: latex

   \begin{fast}{Fonction de service}
	  \FT{FT1}
	  {
  	    \FT{FT2}
	    {
	      \ST{Solution technique}
	    }
	  }
	  \FT{}
	  {
	    \FT{FT3}
	    {
	      \ST{ST2}
	    }
	  }
	  \FT{FT4}
	  {
	    \ST{ST3}
	  }
   \end{fast}
   \fastReset

ce qui donne :

.. image:: /images/diagramme-fast-LaTeX.png
	   :alt: Un diagramme fast réalisé en LaTeX
	   :width: 150%

On peut également modifier la largeur des boîtes avec
:latex:`\renewcommand*{\fastLargeurBoite}{8em}` par exemple pour une largeur de
8em. Idem pour la hauteur avec :latex:`\renewcommand*{\fastHauteurBoite}{3em}`.

C'est facile et très complet. La documentation complète est là :
http://www.tug.org/texlive//devsrc/Master/texmf-dist/doc/latex/fast-diagram/help.pdf
