Emacs : édition multiple
########################

:tags: emacs

J’ai récemment eu besoin d’éditer plusieurs fichiers contenant la même
chaîne de caractères pour la remplacer. Je pensais tout d’abord utiliser
sed avant de regarder ce que pouvais faire Emacs. Eh bien non seulement
il peut le faire, mais il peut le faire facilement en vous laissant un
certain "contrôle" lors du remplacement.

Ouvrons nos fichiers
--------------------

Étape 1 : trouvez tout les fichiers à éditer. Peu importe leur
localisation, on va utiliser la commande find qui s’intègre très bien
avec Emacs. Lancez Emacs. Tapez ensuite :

::

    M-x find-dired

Et ouvrez le répertoire dans lequel vous voulez lancer votre recherche.
Emacs vous demande alors les options à passer à find. Allez voir le man
de find pour savoir tout ce que vous pouvez donner comme argument. Par
exemple :

::

    -type f -name settings.php -maxdepth 3

ce qui nous retournera tous les fichiers nommés settings.php situé à une
profondeur maximale de 3. Libre à vous de faire plus compliqué suivant
vous besoin. (Ne vous prenez pas trop la tête non plus).

Sélectionnons les fichiers à éditer
-----------------------------------

Une fois la recherche terminée, Emacs vous affiche tous les résultats.
Et c’est à vous de les sélectionner. Nul besoin de trop se prendre la
tête avec find à l’étape précédente. Aller sur la ligne/le nom du
fichier à éditer et appuyez sur la touche t. Le nom du fichier change de
couleur. Il est sélectionné ! Pour sélectionner tous les fichiers, tapez m.

Cherchons et remplaçons
-----------------------

Tapez

::

    M-x dired-do-query-replace-regexp

pour procéder à la recherche. Emacs vous demande tout d’abord
l’expression régulière à chercher, puis la chaîne par laquelle il faut
la remplacer. Emacs va alors vous proposer fichier par fichier et
occurrence par occurrence le remplacement. Tapez "y" pour le faire, "n"
pour passer à l’occurrence suivante. "!" remplace toutes les occurrences
du buffer courant et "Y" toutes les occurrences dans tous les fichiers.
"?" affiche la liste complète des commandes disponibles.

 

Sauvegardons
------------

Pour sauvegarder tous vos fichiers en une seule fois, tapez

::

    M-x ibuffer

Sélectionnez les fichers non sauvegardés en tapant ``* U``. Sauvegarder
les avec S. Et voilà, c’est fini ;-).

 

Conclusion
----------

Nous venons facilement de remplacer une chaîne présente dans plusieurs
fichiers. Il est évidement possible de le faire sur `des fichiers
présents sur un serveur <|filename|2013-08-08_emacs-connecter-ssh.rst>`__. Vous avez
également accès à des fonctions de sélection de fichiers plus avancées,
comme sélectionner tous les fichiers contenant une expression régulière
et à d’autres fonctions de manipulation de fichier (comme les renommer).

Maintenant, vous savez quel outil prendre pour éditer vos fichiers !
