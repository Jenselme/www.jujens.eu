Log POST data with apache
#########################

:tags: apache
:slug: log-post-requests-apache
:lang: en

According to the manual, the easiest way to log the content of a POST request (for debugging purpose, in a production environment, it would increase the size of the logs and increase the probability to leak information) is to use the `dumpio module <https://httpd.apache.org/docs/2.4/fr/mod/mod_dumpio.html>`__. For Apache 2.4, all you should do is:

.. code:: apache

    # Put the logs in specific files to ease reading.
    CustomLog /var/log/httpd/website.log combined
    ErrorLog /var/log/httpd/website.error.log

    # Enable debug logging.
    LogLevel debug

    # Enable the module.
    DumpIOInput On
    DumpIOOutput On
    LogLevel dumpio:trace7

Sadly, I never managed to make it work. Hopefully, there is another module which allows us to do that: ``mod_security``. You should configure it as follows:

.. code:: apache

    # Enable the module.
    SecRuleEngine On
    SecAuditEngine on

    # Setup logging in a dedicated file.
    SecAuditLog /var/log/httpd/website-audit.log
    # Allow it to access requests body.
    SecRequestBodyAccess on
    SecAuditLogParts ABIFHZ

    # Setup default action.
    SecDefaultAction "nolog,noauditlog,allow,phase:2"

    # Define the rule that will log the content of POST requests.
    SecRule REQUEST_METHOD "^POST$" "chain,allow,phase:2,id:123"
    SecRule REQUEST_URI ".*" "auditlog"

Source: https://www.technovelty.org/web/logging-post-requests-with-apache.html
