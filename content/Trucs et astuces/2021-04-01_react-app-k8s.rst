Deploy a React app in kuberentes
================================
:tags: devops, k8s, kubernetes, Django, Python
:lang: en

I recently deployed a React app in kubernetes.
It was initially deployed directly in a public bucket: I have an API that is already hosted on kubernetes, but the app itself is made only of static files, so it made sense.
However, requirements for my app changed and I required to add basic authentication to it, so before accessing the app the browser would ask for a username and a password.
The main idea being to prevent users to access our pre-production app and to mistake it for our production one should a URL leak.

Since we are limited in what we can do with HTTP headers with GCP buckets, I was stuck.
I also wanted to add extra HTTP headers like ``X-Frame-Options`` or ``X-Content-Type-Options`` which are useful for security reasons.

To do this, we can:

- Insert a nginx between the user and the bucket. The nginx server would then act as a reverse proxy for the bucket. This would add extra round-trips to get the files and after some tests, it's hard to configure nginx correctly in this case.
- Just put everything into a container and host the result on kubernetes alongside the API. We avoid round trips, and we have a complex but standard nginx configuration for SPAs. This configuration will be more simple and more tested than our weird "bucket proxy" one. Since the built JS, CSS and image files are small, we will get a small Docker image we can easily deploy.

To achieve this, I used the nginx configuration below, stored in a ``ConfigMap`` and deployed with `Helm <https://helm.sh>`__:

.. code:: yaml
    :number-lines:

    apiVersion: v1
    kind: ConfigMap
    metadata:
      name: frontend-app-nginx
    data:
      website.conf: |
        server {
            listen 80;
            root /var/www/frontend-app/;
            client_max_body_size 1G;
    
            gzip on;
            index index.html;
    
            access_log stdout;
            error_log  stderr;
    
            location / {
                {{ if .Values.container.nginx.enableBasicAuth -}}
                auth_basic           "Pre-Production. Access Restricted";
                auth_basic_user_file /etc/nginx/conf.d/.htpasswd;
                {{- end }}
    
                add_header X-Frame-Options DENY;
                add_header X-XSS-Protection "1; mode=block";
                add_header X-Content-Type-Options nosniff;
    
                # Set a very long cache on scripts and CSS since their URL contains
                # their hash making them immutable.
                # We can keep them for as long as we want.
                # All those files are in a /static folder.
                location /static/ {
                    add_header Cache-Control "public, max-age=31536000, immutable";
                    try_files $uri /$uri =404;
                }
    
                # Set some cache on unhashed files (that's everything that ends with an extension).
                location ~ .*\.[a-z]+$ {
                    add_header Cache-Control "public, max-age=3600";
                    try_files $uri /$uri =404;
                }
    
                location /nghealth {
                    {{ if .Values.container.nginx.enableBasicAuth -}}
                    # Always disable basic authentication for health routes.
                    # It makes working with them easier in kubernetes and for load balancers.
                    auth_basic off;
                    {{- end }}
                    return 200;
                }
    
                # If path doesn't exist, the user wants to reach the site.
                # So we return the default index.
                add_header Cache-Control 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0';
                try_files /$uri /index.html =404;
            }
        }
      # Put the password here: it's base64 encoded so no directly readable and it exists first and foremost
      # to signal users they don't have anything to do here, not to protect access from anything sensitive.
      .htpasswd: <copy your .htpasswd here>


.. note:: You should always disable authentication for your health routes. You can configure probes to send HTTP headers to make it work, but I encountered issues in my setup where the load balancer can talk directly to the pod and required a health route too. It didn't seem to work when it was protected with basic authentication.
      
      
The ``deployment.yaml`` file:

.. code:: yaml
    :number-lines:

    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: {{ include "chart.fullname" . }}
      labels:
    {{ include "chart.labels" . | indent 4 }}
    spec:
      selector:
        matchLabels:
          app.kubernetes.io/name: {{ include "chart.name" . }}
          app.kubernetes.io/instance: {{ .Release.Name }}
      template:
        metadata:
          labels:
            app.kubernetes.io/name: {{ include "chart.name" . }}
            app.kubernetes.io/instance: {{ .Release.Name }}
        spec:
          containers:
            - name: {{ .Chart.Name }}
              image: "{{ .Values.container.image.repository }}:{{ .Values.container.image.tag }}"
              imagePullPolicy: {{ .Values.container.image.pullPolicy }}
              ports:
                - name: http
                  containerPort: {{ .Values.container.port }}
                  protocol: TCP
              resources:
                limits:
                  memory: {{ .Values.container.resources.limits.memory }}
                  cpu: {{ .Values.container.resources.limits.cpu }}
                requests:
                  memory: {{ .Values.container.resources.requests.memory }}
                  cpu: {{ .Values.container.resources.requests.cpu }}
              {{ if .Values.container.probe.enabled -}}
              livenessProbe:
                httpGet:
                  path: {{ .Values.container.probe.path }}
                  port: {{ .Values.container.port }}
                timeoutSeconds: {{ .Values.container.probe.livenessTimeOut }}
                initialDelaySeconds: {{ .Values.container.probe.initialDelaySeconds }}
              readinessProbe:
                httpGet:
                  path: {{ .Values.container.probe.path }}
                  port: {{ .Values.container.port }}
                timeoutSeconds: {{ .Values.container.probe.livenessTimeOut }}
                initialDelaySeconds: {{ .Values.container.probe.initialDelaySeconds }}
              {{- end }}
              volumeMounts:
                - name: nginx-conf
                  mountPath: /etc/nginx/conf.d
                  readOnly: true
          volumes:
            - name: nginx-conf
              configMap:
                name: frontend-app-nginx

And my ``Dockerfile`` which relies on multi-stage builds to reduce the size of the target image.
I use a node image to build the files and then only a standard nginx image to serve them:

.. code:: Dockerfile

    FROM node:14.16.0-alpine3.13 AS builder
    WORKDIR /app

    RUN apk add python3 make gcc g++ libc-dev

    ARG REACT_APP_ENV=undefined

    ENV REACT_APP_ENV=$REACT_APP_ENV

    COPY . ./

    RUN yarn install --frozen-lockfile
    RUN yarn build && \
        find -name \*.js\*.map -delete


    # Run the app in nginx.
    FROM nginx:latest AS runner
    RUN mkdir -p /var/www/frontend-app
    WORKDIR /var/www/frontend-app

    COPY --from=builder /app/build /var/www/frontend-app/

I pass the necessary arguments to docker so it can build the image with something like ``--build-arg REACT_APP_ENV=$REACT_APP_ENV``.
I then capture it with the ``ARG`` command in the ``Dockerfile`` and transform it into an environment variable ``react-app`` can use.

With all this, you should be able to have a working nginx for your SPA if you need it.
There are some consequences to doing it this way though:

- Nginx will respond with a 200 status and the ``index.html`` when a 404 would be more appropriate. Sadly, this cannot be solved in a simple manner, and we also have this issue with the bucket. So on that problem, both solutions are even.
- You gain more flexibility over what you can/cannot do with headers and authentication, which is very good.
- You should be able to keep old versions of static files to prevent issues when we switch from one version of the app to another. I hope I'll be able to make it work soon. If I do, I'll post a link here.
- If we need big files (eg videos, many images…), we will need to put them into a bucket to keep the image small. For instance, if you have big images or videos. Since those files shouldn't change often (and most likely not at every build), managing them differently shouldn't be an issue.

.. note:: To keep things simple, I don't recommend you deploy a SPA into kubernetes unless you need the extra flexibility a dedicated nginx will give you. Bucket are bare bones, but simple and reliable. Use them for as long as you can.