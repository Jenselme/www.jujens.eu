LaTeX : annexe
##############

:tags: LaTeX

Comment faire des annexes (numéroté par des lettres plutôt que par des chiffres) en LaTeX ? C’est très facile avec le paquet appendix. Il suffit juste de l’utiliser : ``\usepackage{appendix}``. Ensuite, placé toutes vos annexes dans l’environnement appendix comme ci-dessous.

.. code-block:: latex

    \begin{appendix}
        \chapter{Les micromodules}
        \label{a_micro}

        \begin{center}
            \includegraphics[scale=0.4, angle=90]{./img/micro.jpg}
        \end{center}

        \chapter{L’encartage bancaire}
        \label{a_encartage}

        \begin{center}
            \includegraphics[scale=0.28, angle=90]{./img/encartage.jpg}
        \end{center}

        \chapter{La personnalisation}
        \label{a_perso}

        \begin{center}
            \includegraphics[scale=0.28, angle=90]{./img/perso.jpg}
        \end{center}

        \chapter{Les différentes étapes de la production}
        \label{a_prod}
    
        \begin{center}
            \includegraphics[scale=0.5, angle=0]{./img/stage_prod.png}
        \end{center}
    \end{appendix}

