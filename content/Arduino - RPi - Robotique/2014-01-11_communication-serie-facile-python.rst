Communication série facile avec python
######################################

:tags: Python, Communication série, Arduino, Programmation
:modified: 2014-05-09 16:00

Python permet de réaliser facilement une liaison via USB entre un PC et
une carte arduino. Nous allons l'illustrer par un exemple qui lit ce
qu'une Arduino Uno envoie et un autre qui envoie un nombre à la Arduino.
La diode de celle-ci doit alors clignoter du nombre envoyé.

Il faut uploader le code suivant sur votre carte Arduino :

.. code:: cpp

    #define LED_PIN 13
    void setup()
    {
      pinMode(ledPin, OUTPUT);
      Serial.begin(9600);
    }

    void loop()
    {
        Serial.println("Bonjour");
        Serial.println("C'est la carte Arduino qui parle");

        while(Serial.available()) {
             int lu = Serial.read();
            flash(lu);
        }
        delay(1000);
    }

    void flash(int n)
    {
      for (int i = 0; i < n; i++)
      {
        digitalWrite(ledPin, HIGH);
        delay(1000);
        digitalWrite(ledPin, LOW);
        delay(1000);
      }
    }

 

Pour la lecture de ce qu'envoie la Arduino :

.. code:: python

    # Module de lecture/ecriture du port série
    from serial import *
    # Port série ttyACM0
    # Vitesse de baud : 9600
    # Timeout en lecture : 1 sec
    # Timeout en écriture : 1 sec
    with Serial(port="/dev/ttyACM0", baudrate=9600, timeout=1, writeTimeout=1) as port_serie:
        if port_serie.isOpen():
            while True:
                ligne = port_serie.read_line()
                print ligne

Pour l'envoie de données vers la Arduino, *en python2* :

.. code:: python

    from serial import *
    nombre = raw_input("Entrez un nombre : ")
    port_serie.write([nombre])

Notez qu'il est possible d'envoyer un plusieurs nombres à la suite au
port série, puisque la fonction "serial.write()" en Python prend en
argument un tableau d'entiers.

Pareil *en python3* :

.. code:: python

	  from serial import *
	  nombre = input("Entrez un nombre : ")
	  port_serie.write(nombre.encode('ascii'))

La différence entre le code python2 et python3 vient du fait qu'en python3, les
communications réseaux et série doivent se faire avec des objets de type
``bytes`` (chaîne d'octet). De même, les données reçues précédemment sont de
type ``bytes`` en python3 mais de type ``str`` (chaîne de caractère) en python2.
Voir par `là
<http://python.developpez.com/cours/apprendre-python3/?page=page_12#L12-A-9>`_
pour plus d'infos.
