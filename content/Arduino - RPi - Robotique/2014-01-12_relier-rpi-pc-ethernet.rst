Relier sa Raspberry Pi à son PC directement en Ethernet
#######################################################

:tags: Raspberry Pi

Dans cet article, nous allons étudier comment connecter son Raspberry Pi
directement à son PC pour

-  pouvoir s'y connecter en SSH
-  qu'il ait une connection internet (via le wifi de notre PC)

.. contents:: Sommaire

Configurer le réseau
--------------------

Le Raspberry Pi et le PC doivent être sur la même plage d'adresse IP
pour pouvoir communiquer mais pas sur la même plage d'adresse que
l'interface wifi (sinon il y a conflit). Dans la suite, je vais
configurer les interfaces éthernet sur 192.168.1.\* le wifi étant géré
automatiquement par votre gestionnaire habituel (NetworkManager chez
moi). Je ne détaillerai que la connexion en IP fixe qui fonctionne très
bien mais qui peux être lourde si vous avez beaucoup de machine à
configurer. Il vous faudra alors regarder comment installer un `serveur
DHCP <http://fr.wikipedia.org/wiki/DHCP>`__ par exemple en suivant les
instructions données
`ici <http://doc.fedora-fr.org/wiki/Mise_en_place_d%27un_serveur_DHCP_Simple>`__.

Méthode graphique avec NetworkManager
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

`NetworkManager <http://fr.wikipedia.org/wiki/NetworkManager>`__ permet
de gérer les réseaux plutôt facilement. Ouvrez la fenêtre de
configuration et cliquez sur *réseaux filaires*. Choisissez *Ajoutez un
profil*. Dans la section *Identité*, donnez un nom au profil et cochez
*Connecter automatiquement*. Dans la section *IPV4*, changez *Automatic
(DHCP)* en *Manuel*. Entrez l'adresse IP que vous désirez associer à
cette interface. Dans mon cas : 192.168.1.1, le masque de sous réseau :
255.255.255 et la route par défaut : 192.168.1.1.

Cochez *Use this connection only for resources on its network.*

Enregistrez et activez cette interface.

En ligne de commande
~~~~~~~~~~~~~~~~~~~~

.. code:: bash

    sudo ip addr add 192.168.1.1/24 dev eth0

Configurer le pare feu
----------------------

Il faut maintenant autoriser la connexion et rediriger le traffic de la
raspberry pi vers l'interface wifi. Pour cela, on entre les commandes
suivantes :

.. code::

    sudo iptables -A FORWARD -o wlan0 -i eth0 -s 192.168.1.0/24 -m conntrack --ctstate NEW -j ACCEPT
    sudo iptables -A FORWARD -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
    sudo iptables -A POSTROUTING -t nat -j MASQUERADE

Ou sous Fedora (ou tout autre système utilisant firewalld)

::

    firewall-cmd --zone=public --add-masquerade

À titre d'information : sous Fedora, les trois commandes précédentes
permettent de se connecter en SSH à la Raspberry Pi, de pinger une IP
depuis la Raspberry Pi mais pas un nom de domaine. Après un certain
temps à chercher du côté du resolv.conf, c'est grâce à la commande *dig
centrale-marseille.fr @8.8.8.8* que j'ai pu voir que le problème était
d'un autre ordre. Étrangement cette commande ne vient pas par défaut
sous `Raspbian <http://raspbian.org/>`__. Elle est en revanche présente
sous `Pidora <http://pidora.ca/>`__.

Si vous rencontrez des problèmes, sachez que la commande *iptables -F*
enlève toutes les règles du parefeu.

Pensez également à vérifier que votre système autorise le transfert d'IP

::

    sysctl -a | grep ip_forward

Si la réponse est :

::

    net.ipv4.ip_forward = 0

faîtes, pour autoriser ce paramétrage :

::

    sysctl net.ipv4.ip_forward = 1

Configurons la Raspberry Pi pour le réseau
------------------------------------------

Il y a deux méthodes pour avoir du réseau : l'IP fixe qui implique de
configurer la Raspberry Pi et l'IP attribuée par un serveur DHCP qui
tourne sur votre machine (ce qui implique de le configurer mais permet
d'installer sa rapsberry pi sans écran).

Avec un serveur DHCP
~~~~~~~~~~~~~~~~~~~~

Installez le paquet dhcp qui devrait être présent dans les dépôts de
votre distribution. Éditez ensuite le fichier /etc/dhcp/dhcpd.conf. Il
doit ressembler à :

.. code:: nginx

    #
    # DHCP Server Configuration file.
    #   see /usr/share/doc/dhcp/dhcpd.conf.example
    #   see dhcpd.conf(5) man page
    #
    subnet 192.168.1.0 netmask 255.255.255.0 {
             option routers 192.168.1.1;            # passerelle par défaut
            option domain-name-servers 8.8.8.8;# serveurs DNS
            range 192.168.1.2 192.168.1.100;        # plage d’adresse
    }

Pour donner une adresse fixe à un hôte (l'adresse mac est requise) :

.. code:: nginx

    subnet 192.168.1.0 netmask 255.255.255.0 {
             option routers 192.168.1.1;            # passerelle par défaut
            option domain-name-servers 8.8.8.8;# serveurs DNS
            host daneel.egab {
                hardware ethernet b8:27:eb:44:ca:ee;
                fixed-address 192.168.1.3;
            }
            range 192.168.1.2 192.168.1.100;        # plage d’adresse
    }

Le bloc host qui donne l'ip fixe doit être avant la directive range

Il n'y a plus qu'à lancer le serveur dhcp avec la commande
``systemctl start dhcpd``. Pour savoir quelle est l'adresse atribuée à
la Raspberry, consultez les log : ``journalctl -f /usr/sbin/dhcpd`` ou
``tail -f /var/log/messages`` si vous n'utilisez pas journald.

Pour une IP fixe
~~~~~~~~~~~~~~~~

Mainteant, au tour de la Raspberry Pi de recevoir sa conf.

Pidora
~~~~~~

Ouvrez /etc/sysconfig/network-scripts/ifcfg-eth0 et entrez (remplacez
les instructions déjà présentes au besoin) :

::

    DEVICE=eth0
    BOOTPROTO=none
    BROADCAST=192.168.1.255
    ONBOOT=yes
    USERCTL=no
    DNS=8.8.8.8
    PEERDNS=yes
    IPV6INIT=no
    NM_CONTROLLED=no
    NETMASK=255.255.255.0
    IPADDR=192.168.1.2
    GATEWAY=192.168.1.1
    TYPE=Ethernet

Raspbian
~~~~~~~~

Ouvrez /etc/network/interfaces et entrez (en ne modifiant que les lignes
concernant l'interface eth0) :

::

    auto eth0
    iface eth0 inet static
            address 192.0.2.7
            netmask 255.255.255.0
            gateway 192.0.2.254
            dns-nameservers 8.8.8.8

Et voilà
--------

Normalement tout fonctionne. J'espère que cet article vous a été utile.
Si vous avez des questions ou des remarques, n'hésitez pas ;-).

Si vous avez des problèmes avec la résolutions des noms de domaine,
entrez dans /etc/resolv.conf

::

    nameserver 8.8.8.8


En plus
-------

- `Se connecter en wifi à une raspberry pi <{filename}2015-11-08_rpi-wifi.rst>`_


Sources
-------

-  https://fedoraproject.org/wiki/FirewallD
-  http://wiki.archlinux.fr/Partage\_de\_connexion
-  https://gist.github.com/ChickenProp/3037292
