Émuler une Raspberry Pi sous Linux avec Qemu
############################################

:tags: Raspberry Pi, Qemu

Qemu est un logiciel de virtualisation qui présente l'avantage de
pouvoir émuler de façon logicielle une architecture donnée. Il vous
permet donc de simuler une architecture que vous n'avez pas, par exemple
l'architecture arm sur x86. Qemu existe aussi sous windows, mais seule
la méthode sous linux sera évoquée ici. Vous pouvez vous référer à `ce
document <http://www.linux-mitterteich.de/fileadmin/datafile/papers/2013/qemu_raspiemu_lug_18_sep_2013.pdf>`__
pour la version windows. Je signale au passage que virt-manager ne
supporte pas a priori l'architecture arm1176. Voir par
`là <https://www.redhat.com/archives/virt-tools-list/2013-February/msg00115.html>`__
pour plus d'infos.

Installer Qemu
--------------

Tout d'abord, il faut installer qemu. Normalement, c'est présent dans
les dépôts de votre distribution. Sous Fedora, j'ai installé les paquets
suivants : qemu-system-arm et qemu-user. Vous devez avoir les commades
qemu-arm et qemu-system-arm une fois installé. Vérifiez que
l'achitecture arm1176 est bien supportée avec :

::

    qemu-arm -cpu ? | grep arm

Vous devriez voir l'architecture correctement listée.

Récupérer une distribution et un noyau
--------------------------------------

Récupérez une image de la distribution de votre choix sur le site qui va
bien : http://www.raspberrypi.org/downloads

Pour booter, vous allez aussi avoir besoin d'un noyau. Récuprérez le
avec :

::

    wget http://xecdesign.com/downloads/linux-qemu/kernel-qemu

Augmenter la taille de l'image
------------------------------

Par défaut, les images des distributions sont très petites. Vous pourrez
faire quelques tests mais pas l'utiliser comme un vrai système émulé.
Pour cela, il faut augmenter la taille de l'image avec :

::

    qemu-img resize 2014-01-07-wheezy-raspbian.img +6G

Démarrer avec un shell root (pour régler des détails)
-----------------------------------------------------

Lancez la commande ci-dessous pour démarrer. Vous aurez un clavier
qwerty. Ne changez pas la valeur après -m, qui représente la mémoire du
système. Il semble qu'elle soit hardcodée quelque part et une autre
valeur vous empêchera de démarrer.

::

    qemu-system-arm -kernel kernel-qemu -cpu arm1176 -m 256 -M versatilepb -append "root=/dev/sda2 panic=1 init=/bin/sh rw" -hda 2014-01-07-wheezy-raspbian.img

Éditez le fichiers /etc/ld.so.preload et commentez la première ligne
(maj + 3 pour faire un #) :

::

    nano /etc/ld.so.preload

Puis Ctlr-O pour sauvegarder et Ctrl-X pour quitter. Ce changement évite
le chargement de libcofi\_rpi.so ce qui évite une boucle inifinie au
boot.

Ensuite, on prépare les disques pour une utilisation future en créant
/etc/udev/rules.d/90-qemu.rules. Entrez les lignes suivantes dans ce
fichier :

::

    KERNEL=="sda", SYMLINK+="mmcblk0"
    KERNEL=="sda?", SYMLINK+="mmcblk0p%n",

Terminez avec la commande sync pour être sûr que tout est écrit sur
l'image. Éditez également votre /etc/fstab : changez *mmcblk0p1 et
mmcblk0p2*\ en respectivement *sda1* et *sda2*.

Démarrage normal
----------------

Entrez :

::

    qemu-system-arm -kernel kernel-qemu -cpu arm1176 -m 256 -M versatilepb -append "root=/dev/sda2 panic=1" -hda 2014-01-07-wheezy-raspbian.img

Si qemu vous demande de lancer fsck, entrez :

::

    fsck /dev/sda2 && shutdown -r now

Qemu devrait démarrer sur raspiconfig. Sélectionnez directement
*Finish*. Nous allons maintenant agrandir les partitions. Avec

::

    sudo cfdisk /dev/sdb

Supprimez la deuxième partition et recréez là avec tout l'espace
disponible. Ensuite, lancez (si les commandes ratent, redémarrez et
relancez les) :

::

    sudo resize2fs /dev/sdb2
    sudo fsck -f /dev/sdb2
    sudo halt

C'est fini.
