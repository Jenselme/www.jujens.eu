use std::path::Path;
use std::process::{ exit };

use clap::{Arg, App, AppSettings, ArgMatches};

use rusqlite::{params, Connection, Result};
use rusqlite::NO_PARAMS;

#[derive(Debug)]
struct Todo {
    id: i32,
    title: String,
    is_done: bool,
}

fn main() {
    const DEFAULT_DB_PATH: &str = "./todos.db";
    let matches = App::new("Todo application")
        .author("Jujens <jujens@jujens.eu>")
        .about("Small app to manage todos in a local SQLite database.")
        .setting(AppSettings::SubcommandRequiredElseHelp)
        .arg(
            Arg::with_name("file")
            .short("f")
            .long("file")
            .takes_value(true)
            .default_value(DEFAULT_DB_PATH)
            .help("Path to the database.")
        )
        .subcommand(
            App::new("insert-test-data")
            .about("Insert test TODOs in the database")
        )
        .subcommand(
            App::new("list")
            .about("List all TODOs from the database")
        )
        .subcommand(
            App::new("create")
            .about("Create a new TODOs")
            .setting(AppSettings::ArgRequiredElseHelp)
            .arg(
                Arg::with_name("title")
                .short("t")
                .long("title")
                .takes_value(true)
                .required(true)
                .multiple(false)
                .help("The title of the TODO to use.")
            )
        )
        .subcommand(
            App::new("complete")
            .about("Mark a TODO as done")
            .setting(AppSettings::ArgRequiredElseHelp)
            .arg(
                Arg::with_name("id")
                .required(true)
                .help("The ID of the TODO to complete.")
            )
        )
        .get_matches();

    let mut conn = match init(matches.value_of("file").unwrap_or(DEFAULT_DB_PATH)) {
        Ok(conn) => conn,
        Err(e) => {
            println!("An error occurred while initializing the database: \"{}\".", e);
            exit(1);
        },
    };

    match matches.subcommand() {
        ("insert-test-data", _) => handle_insert_test_data(&mut conn),
        ("list", _) => handle_list_all(conn),
        ("create", creation_matches) => handle_create(conn, creation_matches),
        ("complete", Some(complete_matches)) => handle_complete(conn, complete_matches),
        _ => {
            println!("No valid subcommand was used");
            exit(1);
        },
    }
}

fn init(db_path: &str) -> Result<Connection> {
    if Path::new(db_path).exists() {
        return Ok(create_connection(db_path)?);
    }

    let conn = create_connection(db_path)?;
    conn.execute("CREATE TABLE IF NOT EXISTS todo (id integer primary key, title text not null, is_done boolean not null)", NO_PARAMS)?;

    return Ok(conn);
}

fn create_connection(db_path: &str) -> Result<Connection> {
    return Connection::open(db_path);
}

fn handle_insert_test_data(conn: &mut Connection) {
    match insert_test_data(conn) {
        Ok(()) => {},
        Err(e) => {
            println!("An error occurred while inserting test data: \"{}\"", e);
            exit(1);
        }
    }
}

fn insert_test_data(conn: &mut Connection) -> Result<()> {
    let tx = conn.transaction()?;

    tx.execute("DELETE FROM todo;", NO_PARAMS).unwrap();
    tx.execute("INSERT INTO todo (title, is_done) VALUES (?1, ?2)", params!["Start project", true])?;
    tx.execute("INSERT INTO todo (title, is_done) VALUES (?1, ?2)", params!["Complete project", false])?;
    
    return tx.commit();
}

fn handle_list_all(conn: Connection) {
    match list_all(conn) {
        Ok(results) => display_all(results),
        Err(e) => {
            println!("An error occurred while listing all todos: \"{}\".", e);
            exit(1);
        }
    }
}

fn display_all(todos: Vec<Todo>) {
    for todo in todos.iter() {
        println!("- id: {}, title: \"{}\", done: {}", todo.id, todo.title, todo.is_done);
    }
}

fn list_all(conn: Connection) -> Result<Vec<Todo>> {
    let mut stmt = conn.prepare("SELECT id, title, is_done FROM todo")?;
    let todos_iter = stmt.query_map(NO_PARAMS, |row| {
        Ok(Todo {
            id: row.get(0)?,
            title: row.get(1)?,
            is_done: row.get(2)?,
        })
    })?;

    let mut todos = vec![];
    for todo_result in todos_iter {
        match todo_result {
            Ok(todo) => todos.push(todo),
            Err(e) => println!("Error while fetching some todos: \"{}\".", e),
        }
    }

    return Ok(todos);
}

fn handle_create(conn: Connection, create_matches: Option<&ArgMatches>) {
    let matches = match create_matches {
        Some(matches) => matches,
        None => {
            println!("You must supply a title for the match.");
            exit(1);
        }
    };
    let title = match matches.value_of("title") {
        Some(title) => title,
        None => {
            println!("You must supply a title for the match");
            exit(1);
        },
    };

    match create(conn, title) {
        Ok(()) => {},
        Err(e) => {
            println!("An error occurred while creating a new TODO: \"{}\"", e);
            exit(1);
        }
    };
}

fn create(conn: Connection, title: &str) -> Result<()> {
    conn.execute("INSERT INTO todo (title, is_done) VALUES (?1, ?2)", params![title, false])?;

    return Ok(());
}

fn handle_complete(conn: Connection, complete_matches: &ArgMatches) {
    let raw_id = match complete_matches.value_of("id") {
        Some(raw_id) => raw_id,
        None => {
            println!("You must supply an id");
            exit(1);
        }
    };

    let id = match raw_id.parse::<u32>() {
        Ok(id) => id,
        Err(e) => {
            println!("You must supply a valid ID (an interger): \"{}\"", e);
            exit(1);
        }
    };

    match complete(conn, id) {
        Ok(()) => {},
        Err(e) => {
            println!("An error occurred while completing the TODO {}: \"{}\"", id, e);
        }
    }
}

fn complete(conn: Connection, id: u32) -> Result<(), String> {
    let nb_rows_updated = conn.execute("UPDATE todo SET is_done = ?1 WHERE id = ?2", params![true, id]);

    return match nb_rows_updated {
        Ok(0) => Err(format!("No row found for supplied id: {}.", id)),
        Err(e) => Err(format!("{}", e)),
        _ => Ok(()),
    };
}
