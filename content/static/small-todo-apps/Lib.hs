{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Lib
    ( listAll,
      initDb,
      insertTestData,
      createTodo,
      completeTodo,
    ) where

import GHC.Generics
import Control.Monad (forM, forM_)
import Prelude hiding (id)
import Control.Applicative
import Control.Exception
import Database.SQLite.Simple ( open, execute, executeNamed, execute_, query_, changes, withTransaction, Connection, NamedParam( (:=) ) )
import Database.SQLite.Simple.FromRow

data Todo = Todo Int String Bool deriving (Generic, Show)

-- The number of field here must match the number of fields we get from the SELECT.
instance FromRow Todo where
  fromRow = Todo <$> field <*> field <*> field

initDb :: IO Connection
initDb = do
  conn <- open "./todos.db" -- The database file will be created if it doesn't exist.
  execute_ conn "CREATE TABLE IF NOT EXISTS todo (id INTEGER PRIMARY KEY, title STR NOT NULL, is_done BOOLEAN NOT NULL)"
  return conn

insertTestData :: Connection -> IO ()
insertTestData conn = do
    withTransaction conn $ replaceByTestData conn

replaceByTestData :: Connection -> IO ()
replaceByTestData conn = do
  execute_ conn "DELETE FROM todo;"
  execute conn "INSERT INTO todo (title, is_done) values (?, ?)" ("Start project" :: String, True :: Bool)
  execute conn "INSERT INTO todo (title, is_done) values (?, ?)" ("Complete project" :: String, False :: Bool)

listAll :: Connection -> IO ()
listAll conn = do
  todos <- try (query_ conn "SELECT id, title, is_done FROM todo;" :: IO [Todo]) :: IO (Either SomeException [Todo])
  case todos of
    (Left error) -> putStrLn $ "Failed to list all todos: " ++ show error
    (Right allTodos) -> mapM_ (putStrLn . formatTodo) allTodos

formatTodo :: Todo -> String
formatTodo (Todo id title isDone) = "- id: " ++ show id ++ ", title: " ++ show title ++ ", done: " ++ show isDone

createTodo :: String -> Connection -> IO()
createTodo title conn = do
  insertResult <- try (execute conn "INSERT INTO todo (title, is_done) values (?, ?)" (title, False :: Bool)) :: IO (Either SomeException ())
  case insertResult of
      (Left error) -> putStrLn $ "Failed to insert the todo: " ++ show error
      (Right result) -> pure ()


completeTodo :: Int -> Connection -> IO()
completeTodo todoId conn = do
  completeResult <- try (executeNamed conn "UPDATE todo SET is_done = true WHERE id = :id" [":id" := todoId]) :: IO (Either SomeException ())
  case completeResult of
    (Left error) -> putStrLn $ "Failed to update the todo: " ++ show error
    (Right _) -> do
      nbUpdatedRows <- changes conn
      displayUpdateMessage nbUpdatedRows

displayUpdateMessage :: Int -> IO()
displayUpdateMessage 0 = putStrLn "Failed to find a todo with the supplied id."
displayUpdateMessage _ = pure ()
