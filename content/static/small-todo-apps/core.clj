(ns todo.core
  (:require [clojure.tools.cli :refer [parse-opts]]
            [clojure.string :as string]
            [clojure.java.jdbc :as jdbc])
  (:gen-class)
  (:import (clojure.lang ExceptionInfo)))

(def allowed-commands #{"insert-test-data", "list", "create", "complete"})

(def command->options {
                       "insert-test-data" [["-h" "--help"]]
                       "list" [["-h" "--help"]]
                       "create" [["-h" "--help"]
                                 ["-t" "--title TITLE" "The title of the todo"]]
                       "complete" [["-h" "--help"]
                                   [nil "--id ID" "The ID of the TODO to mark as done"
                                    :parse-fn #(Integer/parseInt %)]]
                       })

(def command->desc {
                       "insert-test-data" "Insert test TODOs into the database"
                       "list" "List all TODOs from the database"
                       "create" "Create a new TODO"
                       "complete" "Mark the TODO as done"
                       })

(def cli-options
  [["-f" "--file PORT" "Path to the database file to use."
    :default "./todos.db"]
   ["-h" "--help"]])

(defn- usage [options-summary]
  (->> ["This is a sample TODO program to create and update TODOs"
        "Usage: todo [options] COMMAND [command-options]"
        ""
        "Options:"
        options-summary
        ""
        "Commands:"
        (string/join \newline allowed-commands)]
       (string/join \newline)))

(defn- error-msg [errors]
  (str "The following errors occurred while parsing your command:\n"
       (string/join \newline errors)))

(defn- exit [status msg]
  (println msg)
  (System/exit status))

(defn- command-usage [command summary]
  (string/join \newline [(command->desc command) summary]))

(defn- validate-command-args
  "Validate arguments for each subcommands"
  [args global-options]
  (let [command (first args)
        command-args (rest args)
        {:keys [options arguments errors summary]} (parse-opts args (command->options command))]
    (cond
      errors {:exit-message (error-msg errors) :ok? false}
      (:help options) {:exit-message (command-usage command summary) :ok? true}
      :else {:command command :options options :global-options global-options})))

(defn- validate-args
  "Validate command line arguments. Either return a map indicating the program
  should exit (with a error message, and optional ok status), or a map
  indicating the action the program should take and the options provided."
  [args]
  (let [{:keys [options arguments errors summary]} (parse-opts args cli-options :in-order true)]
    (cond
      (:help options) {:exit-message (usage summary) :ok? true}
      errors {:exit-message (error-msg errors) :ok? false}
      (and (<= 1 (count arguments))
           (allowed-commands (first arguments))) (validate-command-args arguments options)
      :else {:exit-message (usage summary)})))

(defn- insert-test-data
  [db]
  (jdbc/insert-multi! db :todo
                      [:title :done]
                      [["Start the project" true]
                       ["Complete the project" false]]))

(defn- display-todo
  [todo]
  (->> [(str "- id: " (:id todo))
        (str "title: " (:title todo))
        (str "done: " (if (= (:done todo) 1) "true" "false"))]
       (string/join ", ")
       (println)))

(defn- list-all
  [db]
  (let [rows (jdbc/query db ["SELECT id, title, done FROM todo"])]
    (doseq [todo rows]
      (display-todo todo))))

(defn- create
  [conn options]
  (if-let [title (:title options)]
    (jdbc/insert! conn :todo {:title title :done false})
    (exit 1 (error-msg ["You must supply a title with --title"]))))

(defn- complete
  [conn options]
  (if-let [id (:id options)]
    (let [nb-rows-updated (first (jdbc/update! conn :todo {:done true} ["id = ?" id]))]
      (if (not (= nb-rows-updated 1))
        (exit 1 (str "No rows could be found for id " id))))
    (exit 1 (error-msg ["You must supply the id of the TODO with --id"]))))

(defn- init
  [db]
  (jdbc/db-do-commands db
                       (jdbc/create-table-ddl :todo [[:id :integer :primary :key]
                                                     [:title :text "not null"]
                                                     [:done :boolean :default false "not null"]]
                                              {:conditional? true})))

(defn- run
  [global-options command options]
  (let [db {:classname   "org.sqlite.JDBC"
            :subprotocol "sqlite"
            :subname     (:file global-options)}]
    (jdbc/with-db-transaction [conn db]
                              (try
                                (init conn)
                                (case command
                                  "insert-test-data" (insert-test-data conn)
                                  "list" (list-all conn)
                                  "create" (create conn options)
                                  "complete" (complete conn options))
                                (catch Exception ex
                                  (println
                                    "An exception occurred while performing " command
                                    " with options " options
                                    " and global options " global-options
                                    ": "
                                    (.getMessage ex)))))))

(defn -main
  [& args]
  (let [{:keys [command options global-options exit-message ok?]} (validate-args args)]
    (if exit-message
      (exit (if ok? 0 1) exit-message)
      (run global-options command options))))
