{-# LANGUAGE BlockArguments #-}

module Main where

import Lib
import Options.Applicative
import Data.Semigroup ((<>))
import Database.SQLite.Simple ( Connection )

data Options = Options GlobalOptions Command

data GlobalOptions = GlobalOptions { file :: String } deriving Show

data CreateOptions = CreateOptions { title :: String } deriving Show

data CompleteOptions = CompleteOptions { todoId :: Int } deriving Show

data Command =
  List
  | InsertTestData
  | Create CreateOptions
  | Complete CompleteOptions
  deriving Show

listParser :: Parser Command
listParser = pure List

insertTestDataParser :: Parser Command
insertTestDataParser = pure InsertTestData

createParser :: Parser Command
createParser = Create <$> createOptionsParser

createOptionsParser :: Parser CreateOptions
createOptionsParser = CreateOptions
  <$> strOption (long "title" <> short 't' <> help "The title of the TODO." <> metavar "TITLE")

completeParser :: Parser Command
completeParser = Complete <$> completeOptionsParser

completeOptionsParser :: Parser CompleteOptions
completeOptionsParser = CompleteOptions
  <$> argument auto (metavar "ID" <> help "The ID of the todo to complete")

globalOptionsParser :: Parser GlobalOptions
globalOptionsParser = GlobalOptions
  <$> strOption (long "file" <> short 'f' <> help "Path to the database file." <> metavar "FILE" <> value "./todos.db")

commandParser :: Parser Command
commandParser = hsubparser
  (
    command "list" (info listParser (progDesc "List all TODOs"))
    <> command "insert-test-data" (info insertTestDataParser (progDesc "Add test TODOs in the database"))
    <> command "create" (info createParser (progDesc "Create a new TODO."))
    <> command "complete" (info completeParser (progDesc "Mark a TODO as done."))
  )

optionsParser :: Parser Options
optionsParser = Options
  <$> globalOptionsParser
  <*> commandParser

main :: IO ()
main = run =<< execParser opts
  where
    opts = info (optionsParser <**> helper)
      (fullDesc <> progDesc "Manage TODOs")

run :: Options -> IO ()
run (Options globalOptions command) = do
  conn <- initDb
  runCommand command conn

runCommand :: Command -> Connection -> IO ()
runCommand List = listAll
runCommand InsertTestData = insertTestData
runCommand (Create CreateOptions { title = title }) = createTodo title
runCommand (Complete CompleteOptions { todoId = todoId }) = completeTodo todoId
