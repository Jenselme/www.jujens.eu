const CACHE_NAME = 'my-cache-v1';
const urlsToCache = [
  '/',
  '/page1.html',
];

self.addEventListener('install', event => {
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(cache => {
        return cache.addAll(urlsToCache);
      })
  );
});

self.addEventListener('fetch', event => {
  console.log('Captured fetching', event.request)
  event.respondWith(
    caches.match(event.request)
      .then(response => {
        if (response) {
          console.log('Serving from cache')
          return response;
        }
        console.log('Fetching from server')
        return fetch(event.request)
          .then(fetchResponse => {
            return caches.open(CACHE_NAME)
              .then(cache => {
                cache.put(event.request, fetchResponse.clone()); // Add the fetched response to the cache
                return fetchResponse;
              });
          });
      })
  );
});
