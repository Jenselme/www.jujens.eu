#!/usr/bin/env bash

# Usage: ./add-user.sh USERNAME REPO_TO_CLONE
# USERNAME: username used for ssh and in the domain
# REPO_TO_CLONE: the repository to clone.
# example: ./add-user.sh jujens https://gitlab.com/Jenselme/hello-flask.git
# will create a jujens user to use the jujens.bl.test subdomain with the specified app.

# Configure bash to exit on errors, consider undefined variables as errors
# and fail if any step in a pipe does.
set -e
set -u
set -o pipefail

GIT_REPO_BASE_DIR="/var/www/bl.test"
BASE_DOMAIN="bl.test"

USERNAME="$1"
REPO_URL="$2"
REPO_DIR="${GIT_REPO_BASE_DIR}/${USERNAME}.${BASE_DOMAIN}"
SUB_DOMAIN="${USERNAME}.${BASE_DOMAIN}"

echo "Creating user and add to group."
useradd "${USERNAME}"
usermod -aG deploy "${USERNAME}"

echo "Preparing repo."
mkdir -p "${REPO_DIR}"
chown jujens:nginx "${REPO_DIR}"
chmod u=rwX,g=rXs,o=- "${REPO_DIR}"
git clone "${REPO_URL}" "${REPO_DIR}"

# Adapt to your needs and language.
echo "Preparing venv"
pushd "${REPO_DIR}"
    echo "Opening .env file"
    ${EDITOR:-vim} .env

    python3 -m venv .venv
    source ".venv/bin/activate"
    pip install -r requirements.txt
pushd
echo "Launching service"
systemctl start "gunicorn@${SUB_DOMAIN}"
systemctl enable "gunicorn@${SUB_DOMAIN}"


# That's assuming you use the default SSH port, if not, adapt.
echo "To add the git remote: git remote add test ${USERNAME}@${BASE_DOMAIN}:${REPO_DIR}"
echo "Done"
