From ZSH to Fish
################
:lang: en

After several years of ZSH with `oh-my-zsh <https://ohmyz.sh/>`__, I decided to give `fish (Friendly Interactive Shell) <https://fishshell.com/>`__.
I heard and read several good things about it.
I also wanted to test `starship <https://starship.rs/>`__ (a cross shell prompt) easily without messing with my ZSH configuration.

The least I can say is: I'm impressed.
Most of the niceties that ohmyzsh brings simply work out of the box: better history, better completion, syntax coloring…
Since Fish don't use the standard shell syntax, I had to port some of my functions to get a usable shell.
I must admit the changes made by Fish, although a bit hard to get used to after so many years, are really nice and make syntax clearer.
And you can document them!

Regarding starship, it provides a lot of configuration and options but the default are very good.
Again, the migration was swift and painless.
I merely disabled the AWS and GCP plugins to avoid cluttering my shell with data I don't often need to get the same behavior as my old ZSH configuration.

To conclude, I'm impressed and very satisfied with my migration.
It went smoothly and most tools (like starship or git) are compatible with it.
I still have to read the doc from time to time and unlearn years of bashism.
Since Fish is not widespread (yet?), I still use Bash more the scripts that must be shared with other people.

`Here <https://github.com/Jenselme/dot-files-shell/tree/master/.config/fish>`__ is my Fish configuration and `my starfish one <https://github.com/Jenselme/dot-files-shell/blob/master/.config/starship.toml>`__ (I hardly changed anything, the defaults are that good!)
