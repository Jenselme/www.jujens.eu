Drupal : un an d'utilisation, mes impressions
#############################################

:tags: Drupal
:summary: Je suis utilisateur de `Drupal <http://drupal.org>`_ depuis environ 1 an maintenant et j’ai donc décidé de partager ce que j’ai appris avec cet outil que j’ai découvert à Centrale Marseille (ECM) grâce au  `projet multi-assos <http://assos.centrale-marseille.fr>`_. Aujourd’hui, j’admets avoir pris goût à l’outil et je suis le webmaster du forum foceen un des plus gros sites de l’installation (et un des plus compliqué aussi).

Je suis utilisateur de `Drupal <http://drupal.org>`__ depuis environ 1
an maintenant et j’ai donc décidé de partager ce que j’ai appris avec
cet outil que j’ai découvert à Centrale Marseille (ECM) grâce au `projet
multi-assos <http://assos.centrale-marseille.fr>`__ (qui héberge cet
humble blog). Aujourd’hui, j’admets avoir pris goût à l’outil et je suis
le webmaster du forum foceen un des plus gros sites de l’installation
(et un des plus compliqué aussi).

Avant
-----

Avant d’entrer à l’ECM, je n’avais jamais fait de vrai site web. J’avais
appris quelques bases de php et de mysql sur le site du zéro
(`openclassrooms <http://fr.openclassrooms.com/>`__ maintenant). J’avais
un peu entendu parler de framework et de CMS mais je n’en pensais pas
grand chose de bien. Sans trop caricatirer, je pensais : *« Non mais un
site ça se code à la main. Les CMS et les framework c’est des machins
d’assistés »*. Et puis j'ai découvert Drupal.

Drupal
------

Si vous ne connaissez pas encore `Drupal <http://drupal.org>`__,
permettez que je vous présente l'outil. C'est un des CMS les plus
utilisés et les plus populaires. Drupal accueille de gros sites (comme
celui de la maison blanche) et des plus modestes (comme celui-ci) et
globalement de nombreux sites d'entreprises ou d'institutions.

Un des gros avantages de Drupal est sa modularité. Sa communauté très
active maintient de très nombreux modules de qualité qui vous permettent
de transformer votre Drupal en site de e-commerce, en forum ou plus
simplement de lui ajouter des fonctionnalités intéressantes comme un
éditeur WYSIWYG. Le tout sans coder une seule ligne. De plus, ces
modules viennent avec des interfaces puissantes mais bien faîtes pour
les configurer suivant vos besoins. Le tout juste avec votre souris. Ce
qui signifie que vous n'avez pas besoin d'être un geek pour être
webmaster d'un site. Tout ce fait très bien via l'interface (sauf pour
des besoins très spécifiques qui demandent du dévelloppement).

La communauté crée aussi pleins de thèmes et certains sont mêmes
adaptatifs. Pas besoin d'être un très bon designer pour avoir un joli
site. Plutôt cool non ?

Ce sont toutes ces raisons qui font que Drupal est bien adapté pour les
associations de l'école. On est pas des dévelopeurs et cela permet de
péréniser les sites web. Car c'est dur de faire un site web (du moins
correctement).

Administrer l'installation
--------------------------

En tant que membre du club Drupal, je fais des sites web (et aide ceux
qui en font) et administre l'installation. C'est à mon avis la partie la
plus intéressante.

Faire des scripts Shell pour automatiser des tâches, faire du git,
monter un serveur en local, bosser sur les migartions de drupal,… c'est
intéressant et formateur (surtout quand il ne faut rien casser).

C'est dur de faire un site web
------------------------------

Contrairement à ce que peut laisser penser un cours de PHP ce n'est pas
simple de faire un site web correctement. Ma (certes assez petite)
expérience de webmaster me l'a bien montrée. Sur un site fait par
quelqu'un qui est reconnu comme plutôt bon et qui sait coder, il peut y
avoir de nombreux problèmes.

Ceux que j'ai vu et qui m'ont posés le plus de problèmes sont :

-  du code non indenté
-  les mots de passe stockés en clair dans la base de données (si si, il
   y en a qui le font)
-  des accès base de données sans passer par PDO (donc pas de requêtes
   préparées et des injections SQL faciles à faire)
-  des données encodées n'importe comment dans une base de données
-  fonction\_1, fonction\_2 et fonction\_3 qui font la même chose (en
   tout cas je n'ai pas vu la différence). Et la gestion de version ça
   sert à quoi ?

Conclusion
----------

Drupal c'est bien ;-). Vous vous prendrez la tête sur l'interface au
lieu de vous prendre la tête sur du code mais bon, vous surviverez. Vous
tomberez aussi sur des gens qui pense que comme tout se fait à la souris
c'est facile et ça ne s'apprend pas. Ça c'est faux. Il vont faire des
pages entièrement codé en html sans utiliser les champs et les vues. Ils
vous dirons que Drupal c'est pourri et qu'en PHP ils auraient pu le
faire facilement. On ne peut pas tout résoudre avec un outil, aussi bien
soit-il.
