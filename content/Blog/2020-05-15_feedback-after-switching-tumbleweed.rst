Feedback after switching to openSUSE Tumbleweed
###############################################

:tags: Linux
:lang: en

This article is a follow up to `Installing openSUSE next to Fedora with BTRFS <{filename}./2020-02-23_opensuse-install-btrfs-subvolumes.rst>`__ where I detailed how I switched to openSUSE Tumbleweed.
In this article, now that I have been using Tumbleweed for about 2 months and a half, I'll give some feedback on my experience.

The issues I had:

- For some reason, I lost an EFI file used by Fedora to boot which prevented me to run my Fedora installation. I was able to restore it by copying another EFI file (``mmx.efi``) into the proper location.
- Unlocking the LUCKS drive in Grub is slow. Although I find it's a bit faster lately (and it's not an issue with Tumbleweed per say).
- Docker sometimes fails to pull images. It only happens in script, running ``docker pull SOME_IMAGE`` always runs correctly.
- Unlike in Fedora, ``/tmp`` is not cleaned automatically after a reboot. You have to enable this manually by putting the content below in ``/etc/tmpfiles.d/tmp.conf`` (see `this thread <https://forums.opensuse.org/showthread.php/535050-tmp-amp-var-tmp-safe-to-remove?p=2895079#post2895079>`__ for the solution and the reasoning behind it):

   .. code:: aconf

       #  This file is part of systemd.
       #
       #  systemd is free software; you can redistribute it and/or modify it
       #  under the terms of the GNU Lesser General Public License as published by
       #  the Free Software Foundation; either version 2.1 of the License, or
       #  (at your option) any later version.

       # See tmpfiles.d(5) for details

       # Clear tmp directories separately, to make them easier to override
       # SUSE policy: we don't clean those directories
       D! /tmp 1777 root root -
       q /var/tmp 1777 root root -

- Updating the kernel to a new minor version (eg from 4.5 to 4.6) can break the Nvidia drivers and thus your display. The patched driver comes relatively fast but it was a bad surprise when it happened to me! I hope the process will be smother in the future. It was also the occasion to experiment booting from a read-only BTRFS snapshot. And it worked well!

Despite these issues (for which I have workarounds anyway), I am very pleased with my experience.
The update process works well (with the exception of Nvidia drivers as pointed above) and the system is stable.
So I decided to switch all my computers to it (I switched to a rolling release to avoid Fedora updates after all, didn't I?).

I must also say that YAST (which acts as a configuration panel for you computer from firewall to installing packages) is well done and a pleasure to use.
No need to search/install different tools, just use YAST!

I also find the use of openSUSE of BTRFS snapshots to be really cool.
I haven't used them much since the system is stable and I haven't encountered issues with updates (except once with Nvidia).
But it's still a nice thing.
I'll just note that while installing openSUSE to another computer, I made a big configuration mistake that locked me out of the system.
Thanks to the snapshots and the rollback mechanism, it wasn't such a big deal (because yes, ``/etc`` will be in the snapshot too).
Without it, I might have needed to reinstall everything.
So I don't need them often, but I'm really glad they are here when I do.

This post wouldn't be complete without some observation on KDE.
As a long time Gnome user, I also decided to switch to KDE to see how it goes (and while your changing, why not change more?).
I am pleased with the change and was able to adapt to KDE very quickly (despite many years of using Gnome).
I do appreciate the fact that many things are integrated by default (like the clipboard manager or media player integration) whereas you need extension in Gnome to do this.
I also really like the fact that the command that ask for the root password is mentioned on the popup that ask for it.
I think it is a big plus for security.

I had some issues though, here is the list and how I solved them:

- I cannot configure my gmail account to use it easily with KMail. From what I read, I have to enable IMAP support in gmail. In Gnome this is supported by default (calendar works fine though). Since I only have my professional mailbox in it, I can just keep gmail opened in a pined tab. So not a big deal.
- Since I use the same user on both Fedora and openSUSE, when KDE launched it changed some configuration and now the close, maximise and minimise icons looks a bit funny on Gnome.
- I use the `bépo <http://bepo.fr/wiki/Accueil>`__ keyboard disposition (which was made specifically for French). It was really easy to configure this keyboard disposition in the session but harder to enable it on the login screen (although it may come from the fact that openSUSE doesn't support this disposition during install unlike Fedora). Luckily, `this post on the ArchLinux forum <https://forums.archlinux.fr/viewtopic.php?t=19684>`__ had the solution: update ``/etc/X11/xorg.conf.d/00-keyboard.conf`` with the content below.

   .. code:: aconf

       Section "InputClass"
           Identifier         "Keyboard Layout"
           MatchIsKeyboard    "yes"
           MatchDevicePath    "/dev/input/event*"
           Option             "XkbLayout"  "fr"
           Option             "XkbVariant" "bepo_latin9"
           Option             "XkbOptions" "terminate:ctrl_alt_bksp"
       EndSection

- I had to create a script in ``.config/autostart-scripts/ssh-add.sh`` with the content below to correctly pre-load my SSH keys, something that worked by default on Gnome. The script is based on `this article from the ArchLinux wiki <https://wiki.archlinux.org/index.php/KDE_Wallet#Using_the_KDE_Wallet_to_store_ssh_key_passphrases>`__.

    .. code:: bash

        #!/usr/bin/env bash

        for key in $(ls -d ~/.ssh/* | grep -v pub | grep -v authorized_keys | grep -v config | grep -v known_hosts | grep -v seahorse); do
            ssh-add -q "${key}"
        done

I also find the combo KDE + openSUSE to need less RAM that my Fedora + Gnome.

So, to conclude, I am very pleased with the change and don't regret doing it.
