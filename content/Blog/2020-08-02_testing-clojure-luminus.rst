Testing a clojure web framework
###############################
:tags: Clojure, Luminus, Reagent
:lang: en
:summary: An opinion about Clojure, Luminus and Reagent after doing a test project with these technologies.

After learning `Clojure <https://clojure.org/>`__ thanks to the awesome book `Clojure for the Brave and True <https://www.braveclojure.com/>`__ I decided to test some web programing with the language to practice a bit.

Before dwelling on this experience, I'd like to say a few words about the language itself.
I heard about the language when I was still completing my masters degree in 2014 or 2015.
I didn't gave it much attention back then.
Later, I noticed that `Robert C. Martin <https://blog.cleancoder.com/>`__ (author of the book `Clean Code <https://www.goodreads.com/book/show/3735293-clean-code>`__ among other books) talked about it and seemed to enjoy the language.
Since I respect his technical opinion, it raised my interest again and I look a bit into it and bought the book *Clojure for the Brave and True* which then sat on my bookshelf for a couple of years.
Until the middle of last year when I decided to seriously look at functional programming and finally went through the book.

This was mind blowing!
In addition to the great qualities of the book who made the learning journey really fun, I found the language itself to be really well designed and interesting to learn.
I like the notion than almost everything is a function (the example with the simple ``+`` function at the beginning of the book really hooked me up), the way it handles "polymorphism", atoms and references…
I also had to change perspective and get used to everything being immutable which requires lot of thinking at first but forced me to reconsider many things about my daily programing practice.
The fact that the book comes with many exercises after each chapter is a great plus to sharpen the reader's skills.
It's a great learning experience and I do encourage you to learn the language.
I must confess I still have issues with the syntax (it's a LISP after all with lots of parentheses everywhere) and how to organize my code.
But I don't practice much, so I guess it's normal.

Moving on to web development and Luminus.
In order to practice and to do something that resembles what I do daily (web development), I decided to write a small website using the language.
I looked around for a library/framework to help me in this endeavour and found `Luminus <https://luminusweb.com/>`__ which looked very good: the documentation is very detailed and it has a great getting started.
It also has a lot of template to create projects with different setup easily (like choose which database you want or to bootstrap the frontend for you).
I also dwelled a bit on frontend development with `ClojureScript <https://clojurescript.org/>`__ (the version of Clojure designed to run in the browser) and `Reagent <https://github.com/reagent-project/reagent>`__, a library written in ClojureScript that leverages React to create dynamic interfaces.
After some quick tests, I knew I would be able to do what I wanted.

My goal was to create a site to help manage various competitions.
In the end, I mostly played with Reagent and coded the home page of the site as well as the detail pages for each competitions.
I initially wanted to add a form to add competitions and a search feature but gave up due to lack of time.
The frontend was entirely done in Reagent and communicated with the backend thanks to REST.
The code is available `here <https://gitlab.com/Jenselme/cljgolf>`__ if you want to take a look at it.

I liked how the frontend and backend can work together and never require a full server restart.
The code, from both the frontend and the backend, is updated via hot updates which makes the developer experience very fluid.
That's also very important since the server is very slow to start, without this auto-reload feature, I think the development experience would be very poor.
It also means I almost never had to fully reload the page.
That was really pleasing.

On the unpleasing side of things: I decided to use a SQL database (as I saw while making the getting started).
From what I tested, that's where the framework doesn't shine: I had to write SQL for each query I had to make which doesn't seem very flexible to me.
Maybe because I'm too used to the Django ORM by now to revert to raw SQL for every requests.
I also had to restart the server each time I edited a query for my changes to be taken into account.
That was adding to the pain I felt when writing SQL queries and one of the reason I decided not to take more time on the project: it would require even more queries.
I looked for ways to avoid this, but didn't find anything (I may not have looked hard enough) and still thinks the framework should handle this for me.
What's strange is other than that, the experience is very fluid and hot reload work perfectly.
This made me think that it's also partially why MongoDB is so popular: all you need is to send JSON to the server and all languages have good tools to do this, mostly Clojure with its emphasis on data and using maps [#what_are_maps]_ everywhere.
No need for someone to build and maintain the complex piece of technology that is an ORM.

All in all, I'm satisfied with what I did and intend to continue trying stuff with Clojure.
If I do something else, I'll probably use MongoDB to avoid SQL altogether.

.. [#what_are_maps] Also known as dict in Python.
