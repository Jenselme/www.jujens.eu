Mon passage à LineageOS
#######################

:tags: Android, LineageOS

Tout d'abord, je présente un projet intéressant : `oandbackup <https://f-droid.org/repository/browse/?fdfilter=Oandbackup&fdid=dk.jens.backup>`__ que j'ai découvert via `cet article <https://tuxicoman.jesuislibre.net/2017/02/passage-de-cyanogen-a-lineageos.html?pk_campaign=feed&amp;pk_kwd=passage-de-cyanogen-a-lineageos>`__. oandbanck vous permet de sauvegarder les APK installés sur votre téléphone pour pouvoir les réinstaller une fois la mise à jour effectuée. Malheureusement, suite à un problème lors de la mise à jour, j'ai dû formater ma carte SD, du coup, je n'ai pu tester que la sauvegarde et pas la restauration. Mais d'après ce que j'ai lu, ça fonctionne très bien.

Concernant l'installation à proprement parler : j'avais déjà tenté une installation début février avec une nightly pour mon galaxy s2. Ça c'était mal passé : l'installation n'arrivait pas à se terminer. Après plusieurs tentatives infructueuses, les builds de cyanogenmod n'étant plus disponibles, j'ai fini par installer une vieille build de replicant pour pouvoir passer des appels et recevoir des SMS. Comme le projet lineageos a pas mal bougé (et que la build de replicant était vraiment vieille), j'ai retenté le week-end dernier une installation. J'ai eu quelques soucis mais j'ai réussi à les contourner. Voici ce que j'ai fait :

#. Télécharger une image récente : https://download.lineageos.org/
#. Télécharger l'utilitaire SU : https://download.lineageos.org/extras
#. Copier les fichiers sur la carte SD
#. Installer TWRP (le mode recovery) : c'est à partir de là que les choses ont commencés à ce gâter. J'ai tenté la commande issue de la documentation:

   .. code:: bash

        heimdall flash --KERNEL twrp-3.1.0-0-i9100.img --no-reboot
        
   Malheureusement, impossible de démarrer sous TWRP : le téléphone démarrait sous l''ancien recovery. J'ai bien tenté ``heimdall flash --KERNEL twrp-3.1.0-0-i9100.img --no-reboot`` (ce que je faisais avec mes anciennes recovery) mais là, impossible de démarrer. D'après ce que j'ai compris, cela vient du fait que TWRP est plus qu'un simple mode recovery, il a besoin d'un noyau pour démarrer. La solution, c'est d'utiliser le noyau contenu dans l'image LineageOS pour pouvoir démarrer en mode recovery. Pour cela :
   
   #. Dézipper ``boot.img`` de l'image.
   #. Lancer avec le téléphone en mode *téléchargement* :
   
   .. code:: bash

       heimdall flash --KERNEL boot.img --RECOVERY twrp-3.1.0-0-i9100.img
       
   Normalement, maintenant, le téléphone peut démarrer en mode recovery.
   
#. Effacer les "caches" dalvik, data, system, et cache.
#. Lancer l'installation du zip. Dans mon cas, l'installation se bloquait aux trois quart environ. En regardant les messages, j'ai vu : ``Failed to mount '/system'``. J'ai testé pas mal de choses (revider les caches, changer le système de fichier avec parted, …) mais rien n'y a fait. Heureusement, j'ai fini par trouver `ce thread reddit <https://www.reddit.com/r/LineageOS/comments/5zf0yx/help_installing_lineageos_on_i9100_need_new_pit/>`__ qui donne la solution : réparer le fichier PIT du téléphone. Pour cela, `télécharger le PIT "qui va bien" </static/lineageos/I9100_1.5GB-System_6GB-Data_512MB-Preload_by-the.gangster.pit>`__ (ou via `le lien original <https://www.androidfilehost.com/?fid=24591000424954843>`__). Ensuite, lancer en mode *téléchargement* (où ``boot.img`` est le noyau de LineageOS dézipper précédemment):

   .. code:: bash
   
      heimdall flash --repartition --pit I9100_1.5GB-System_6GB-Data_512MB-Preload_by-the.gangster.pit --KERNEL boot.img --RECOVERY twrp-3.1.0-0-i9100.img
      
   À ce stade, l'installation du zip devrait fonctionner (j'ai revidé les caches avant on ne sait jamais).

#. Installer su
#. Redémarrer
#. Normalement tout va bien

Malheureusement, je ne sais plus à quel moment j'ai dû effacer la carte SD (je n'ai pas pris de notes pensant pouvoir écrire ce billet plus tôt). Mais de mémoire, c'était évident donc j'espère que pour vous aussi. Et si vous rencontrez un problème, vous pouvez laisser un commentaire !

