Pourquoi j'abandonne LaTeX et je passe à org-mode
#################################################

:tags: Emacs, Org-mode, LaTeX
:summary: Oui, LaTeX est surpuissant et permet de faire de très belles choses étrange assez facilement. Même si je lui préfère LibreOffice pour certaines tâches (tout ce qui contient beaucoup d'images notamment), j'aime LaTeX (et j'ai même fait mon CV en LaTeX ;-)). LaTeX nous force à organiser le contenu, tout est un dans fichier texte (ce qui est à mon avis pérenne) et pouvoir tout faire au clavier est un gros plus (on ne se perd pas dans l'interface). Pour contre, soyons franc parfois ce n'est pas évident d'avoir le résultat souhaité et il faut chercher sur pleins de forum et mettre les mains dans le camboui.

Oui, LaTeX est surpuissant et permet de faire de très belles choses
étrange assez facilement. Même si je lui préfère LibreOffice pour
certaines tâches (tout ce qui contient beaucoup d'images notamment),
j'aime LaTeX (et j'ai même fait mon CV en LaTeX ;-)). LaTeX nous force à
organiser le contenu, tout est un dans fichier texte (ce qui est à mon
avis pérenne) et pouvoir tout faire au clavier est un gros plus (on ne
se perd pas dans l'interface).

Pour contre, soyons franc parfois ce n'est pas évident d'avoir le
résultat souhaité et il faut chercher sur pleins de forum et mettre les
mains dans le camboui.

Il y a quelques temps déjà, je cherchais un logiciel pour prendre des
notes. Je suis tombé sur pas mal de logiciels graphiques avant de
découvrir org-mode. C'est un mode d'Emacs intégralement dédié à la prise
de note et il est présent de façon standard depuis les dernières
versions d'Emacs. Et en plus de pouvoir remplacer LaTeX (on peut faire
de l'export vers LaTeX, html, pdf) ce logiciel fait bien plus. Et c'est
pourquoi j'abonne LaTeX pour passer à org-mode. Voyons tout de suite
pourquoi.

La syntaxe
----------

LaTeX a une syntaxe très verbeuse, tellement verbeuse qu'elle en devient
lourde (un peu comme le HTML à mon avis) et qui utilise lourdement le
backslash pas toujours accessible facilement. À mon avis, pour l'édition
de fichier texte, la syntaxe des divers wiki est vraiment plus
performante.

Et voici Org-mode. La syntaxe d'org-mode est simple et élégante. En
LaTeX pour créer un titre, il faut ``\title{Mon titre}`` soit 8
caractères qui n'ont pas d'utilité pour le lecteur. Et pour les
sous-titres le nombre de caractères explose. En Org-mode :
``* Mon titre``. Et c'est tout ! Pour faire un sous-titre
``** Sous titre``. Et on peut en mettre autant que l'on veut ! Bon
seulement 6 si on veut faire de l’export html.

Autre point très intéressant, Emacs va plier le texte de ces section
tout seul. Et on peut déplier facilement, en appuyant sur TAB.

Pour mettre en gras, il suffit de mettre le texte entre \* et pour
l'italique, entre /. Pour faire des listes, il suffit de commencer la
ligne par un - ou un nombre.

Pas mal hein ? Mais on peut faire bien plus.

Les TODO listes
---------------

Comme je l'ai dis en introduction Org-mode est fait pour prendre des
notes. Mais il fait aussi des TODO list. Placez-vous sur une ligne et
tapez TODO devant celle -ci (ou utilisez le racourcis clavier
``^-c ^-e``). Lors que l'item est fait, réutiliser le raccourcis
clavier. Et hop, TODO devient DONE. Mais ce n'est pas tout. Considérons
la liste ci-dessous, que l'on écrit telle quelle dans Emacs :

::

    * TODO remplir mon blog [0%]
    ** TODO Faire des articles
    - [ ] Sur Org-mode
    - [ ] Sur autre chose
    ** TODO Finir les tutos

Et bien Lorsqu'on aura coché toutes les cases de Faire des articles
(avec ``^-c ^-e``), cette item passera en DONE automatiquement et le
pourcentage de complétion de Remplir mon blog passera à 50% (si ce
pourcentage est faux, forcer sa réévaluation avec ``^-c ^-c``).

::

    * TODO remplir mon blog [50%]
    ** DONE Faire des articles
    - [X] Sur Org-mode
    - [X] Sur autre chose
    ** TODO Finir les tutos

Plutôt pas mal, non ?

Ils sont beaux mes tableaux, ils sont beaux
-------------------------------------------

Autre point très lourd en LaTeX : les tableaux. La syntaxe est lourde et
on ne sait pas forcément quel sera le résultat. En Org-mode c'est simple
et efficace (vous vous en seriez doutez). Entrez :

::

    | Produits | Prix |
    |-

Appuyez sur TAB et hop :

::

    | Produits | Prix |
    |----------+------|
    |          |      |

Et on continue à modifier en appuyant sur TAB pour passer d'une case à
l'autre (Emacs va les ajouter tout seul) et les redimensionner comme un
grand.

Et on peut même le transformer en tableur, mais là je vous revoie vers
la
`doc <http://orgmode.org/worg/org-tutorials/org-spreadsheet-intro.html>`__.

L'export
--------

Mais un joli fichier texte n'est pas adapté pour tout les usages. Un
petit pdf (très joli car généré par LaTeX) ou un fichier html peut être
plus approprié. Org-mode peut évidemment exporter votre TODO liste. Il
existe plein de raccourcis clavier complexe (c'est un peu le problème
d'Emacs) mais vous pouvez aussi passer par les menus (c'est un peu l'un
des avantages d'Emacs sur vim). Ou alors :

::

    M-x org-mode-export-TAB

Et choisissez l'export qui vous convient.

On peut faire du LaTeX avec
---------------------------

Les syntaxes légères c'est bien joli mais parfois ce n'est pas
suffisant. Si je veux faire des équations très compliquées avec pleins
de symboles étranges ça ne suffit pas. Heureusement, avec Org-mode on
peut insérer du code LaTeX directement, sans modification. Il sera
conservé lors de l'export en LaTeX et interprété lors de la compilation
en pdf (même si vous exportez directement en pdf). Je peux donc faire :

::

    * Equation de Jenselme
    La voilà, avec $h = \frac{h}{2}$ :
    $$\frac{p}{2m}|\Psi> + \sum_{i = 1}^{N} x^i = 0$$

Et on obtient après compilation :

|image0|

Conclusion
----------

Voilà pourquoi je passe à Org-mode. On peut évidemment aussi faire des
présentations en beamer avec (avec une jolie syntaxe toute simple et
très claire), mettre du code source, le transformer en calendrier à
partir des TODO list en mettant une date de fin,… Je vous laisse
regarder la doc pour ça.

.. |image0| image:: /images/equation_jenselme.png
