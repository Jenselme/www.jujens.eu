De drupal 6 à drupal 7 : drush sup
##################################

:tags: Drupal, drush

Pour le projet `multi-assos <http://assos.centrale-marseille.fr>`__ j'ai
dû migrer trois sites webs en drupal 6 vers drupal 7. Voilà mon petit
retour d'expérience (un autre suivra bientôt sur le module
`migrate\_d2d <http://drupal.org/project/migrate_d2d>`__ qui m’a permi
de migrer deux des trois sites). Ici c'est la méthode utilisée pour le
`forum <http://forum.centrale-marseille.fr>`__. Ce site a beaucoup de
nodes (plus de 16 000) mais reste simple dans sa construction.

Pourquoi drush sup
------------------

Il existe plusieurs outils pour transférer du contenu d'un drupal vers
un autre (node export p. ex.) mais pour migrer de drupal 6 et 7 ce qui
me semble le mieux c’est drush sup. Il migre le contenu, télécharge les
modules (dont les thèmes) et les active, copie les fichiers,… sur le
site drupal 7 ce qui est plutôt sympa. Bref, en théorie une fois que
vous avez finis la migration, votre site d7 est une copie conforme du d6
(enfin en théorie).

Préparation
-----------

Étape 1 : télécharger le module drush sup avec drush sup

Étape 2 : faire une copie de test des sites (il serait dommage de casser
le vrai site).

Étape 3 : mettre en place les alias drush (pour le drupal 6 et le drupal
7) en éditant le fichier ~/.drush/aliases.drushrc.php comme suit (à
adapter à votre configuration) :

.. code-block:: php

    <?php
    $aliases['toto'] = array(
        'uri' = 'toto.local',
        'root' => '/var/www/local',
    );

Étape 4 : vérifier la disponibilité des modules et thèmes en drupal 7.
Drush sup est très fort mais il ne fait pas de miracle non plus ;-)

Vous êtes prêt. Il vous suffit de lancer : drush @d6 sup @d7 et de
suivre les instructions.

La migration
------------

Normalement, le script de migration détaille bien les étapes et les
questions posées sont claires. Il n’y a pas de raison majeure que ça se
passe mal.

Il est bon de savoir que drush fait des sauvegardes et peut donc
rétablir le site d7 a un point antérieur de la migration (tout refaire,
juste après la migration des modules,…).

Le point qui m’a posé problème est en général la migration du contenu
qui bloque. Parfois en retentant ça passe. Pas compris pourquoi.

Un point bizarre que j’ai eu sur un site : drush sup refusait de migrer
mes modules via l’option qui permet de tout migrer en un coup. Mais en
les prenant un à un, c’est passé.

Conclusion
----------

Pour les sites relativement simples tels que ceux que l’on a sur assos
la migration ne doit pas faire peur. Drupal 7 est suffisament mature,
avec suffisament de modules pour que tout se passe bien. La migration
demande quand même pas mal de tests au préalable pour régler les petits
problèmes tels que les blocs qui disparaissent ou le thème qui casse. Le
nombre de node ne doit pas faire peur. Par contre, il faut se méfier des
sites avec des modules étranges.

Concernant les deux sites pour lesquels je n’ai pas pu faire du drush
sup : le premier avait une base de données pourrie (un de mes
prédécesseurs avait tenté une migration directement sur le serveur ce
qui a pourri la base de données, même en supprimant les tables de drupal
7, il restait des problèmes). Pour le second, les très nombreuses images
indispensables au site ne sont pas migrées et il se trouve que c’est un
peu plus simple de rattraper ça à partir du résultat de migrate\_d2d.

Voilà. Si vous avez des questions ou des remarques, les commentaires
sont là.
