Small TODO apps
###############
:lang: en
:tags: Rust, Clojure, Haskell

I recently decided to lean a bit of Haskell for fun and see what the language was like.
In this context, I decided to create a small TODO app.
Since I also wanted to compare my solution with solutions in other languages, I decided to write the TODO app three times:

- In Haskell to practice the language as well as monads.
- In Rust because I like the language.
- In Clojure because I already knew it and I wanted to practice it.
  It's a functional language like Haskell, so I though it would be interesting to include it in the comparison.

Each app will do the exact same thing:

- Initialize the database on startup, this means create the SQLite file and the table ``todo`` if required.
  This way, we can easily use it later.
- Allow the database file to be specified to use a specific file if needed.
- Store new TODO items in the database. Each TODO will be made of an id, a title and a completion status (ie done or not done).
- List all TODOs.
- Insert test data in the database.
- Mark a TODO as done.

I'll talk a bit about each app and then try to conclude my experience.
I'll start with the Rust one, then the Clojure one and finally the Haskell one (that's the order in which I wrote them).
You will find the full source code in each section.


Rust
====

Rust is the language that most alike to the languages I use everyday: Python and JavaScript.
The goal of the language is to write fast and memory safe code without the need for a garbage collector.
It's done thanks to its compiler and its type system (which I won't explain here).
The errors of the compiler are generally explicit and clear.
While it borrows some concepts from functional programming like immutability by default or ``if`` expressions, we can still write procedural or object like code.
We also have to make some variables mutable from time to time (which is not possible in functional programming languages).
So it's a mix, but an efficient one if you ask me.

I also think the community is striving and I found without any difficulty many libraries to parse command line arguments or communicate with the database.
I settled with `clap <https://github.com/clap-rs/clap>`__ for argument parsing and `rusqlite <https://github.com/rusqlite/rusqlite/>`__ for SQLite communication.
They both seems like popular and robust choices for what I was aiming for while staying simple to use.

Now, let's talk about the code itself.
The definition of the parser resembles what I could do in Python or JS, as you can see with the code sample below.
I also find it very readable and expressive.

.. code:: rust

    let matches = App::new("Todo application")
        .author("Jujens <jujens@jujens.eu>")
        .about("Small app to manage todos in a local SQLite database.")
        .setting(AppSettings::SubcommandRequiredElseHelp)
        .arg(
            Arg::with_name("file")
            .long("file")
            .takes_value(true)
            .default_value(DEFAULT_DB_PATH)
            .help("Path to the database.")
        )
        .subcommand(
            App::new("insert-test-data")
            .about("Insert test TODOs in the database")
        );

Since I have 4 different actions with different arguments, I decided to divide the code in subcommands.
A pattern I reused for all apps.

If creating the parser is very similar to what I could have done in Python, when I had to use it, I saw some particularities of Rust: Rust doesn't have exceptions and forces you to handle all errors.
For instance, when I create a TODO in ``handle_create``, I pass a title with the ``--title`` option.
Since the argument may not be provided, I get an ``Option`` from the parser: it's a data structure used in Rust to handle optional values, like ``Maybe`` in Haskell.
When you have this structure, you need to extract the value from it before you can use it which forces you to handle the case where you have a value and the case where you haven't one.
So the compiler, forces you to handle potential missing data.
There is also a similar structure named ``Result`` which holds a value or an error.
It's useful when you need to provide a useful error message and not just a value or ``None``.

This is how it looks like:

.. code:: rust

    let title = match matches.value_of("title") {
        Some(title) => title,
        None => {
            println!("You must supply a title for the match");
            exit(1);
        },
    };

This is very interesting and allowed me to provide custom and precise error messages at each step.
But, this makes the code quite verbose.
To help a bit, you can use things like ``unwrap`` (which can result in a crash if the structure doesn't hold a value) or ``unwrap_or`` to extract the value or get a default value if needed.
You can also use the ``?`` operator like that:

.. code:: rust

    conn.execute("INSERT INTO todo (title, is_done) VALUES (?1, ?2)", params![title, false])?;

The compiler will propagate the error, meaning the compiler will replace the code above by:

.. code:: rust

    let value = match conn.execute("INSERT INTO todo (title, is_done) VALUES (?1, ?2)", params![title, false]) {
        Ok(value) => value,
        Err(e) => return Error(e),
    };

which is much more compact and achieve the same result if you don't want/need to handle the error at call site.
That's a very nice syntactic sugar I didn't used much to handle errors in order to provide the user with relevant messages.

The thing to remember (from my perspective at least) is that you can write code that crash at runtime if you don't pay attention or don't know what you are doing.
The language will just give you tools to avoid this, it's then up to you to use those tools correctly.

Since all my other commands are written in the same way, I won't detail more: I think I have given you the gist of the code.
To dig deeper, here is my ``Cargo.toml`` file with the project dependencies:

.. include:: ../static/small-todo-apps/Cargo.toml
    :code: toml
    :number-lines:
    :name: Cargo.toml

and the full ``src/main.rs`` file:

.. include:: ../static/small-todo-apps/main.rs
    :code: rust
    :number-lines:
    :name: src/main.rs


Clojure
=======

Clojure is very different.
It's a LISP where all the code is written as lists (like in all LISPs) that runs on the JVM.
All data structures are immutable and variables are dynamically typed.
The language aims to focus on the data instead of the program.

It was easy to find libraries but I found that the parser was less complete than the Rust or Haskell one (or Python ones for that matter).
I guess it comes from the fact that Clojure is a niche language that is less known and has a smaller community.

I had more manual work to do to parse my arguments, including the validation.
I think that when you are not used to the language or another LISP, the program is hard to decode with everything having the same structure.
Since it's also very different, it's also way harder to understand if you don't already know the language.

I defined the options of the parser in a vector (similar to a list in Python):

.. code:: Clojure

    (def cli-options
        [["-f" "--file PORT" "Path to the database file to use."
        :default "./todos.db"]
        ["-h" "--help"]])

I can then use this with the ``parse-opts`` function to extract my arguments (and potential errors):

.. code:: Clojure

    (let [{:keys [options arguments errors summary]} (parse-opts args cli-options :in-order true)]
        "The code")

The ``parse-opts`` function will return a map (a dict in Python), the ``:keys`` allows me to extract values from this map (``options``, ``argument``…) and the ``let`` expression allows me to bind these values to local variables (with the same name they had in the map).
I can then use these values to build a map either to exit the program later with a message or to execute actual code based on the supplied arguments.
I used a similar pattern to define and parse the options of my subcommands.

I won't detail much here since I think it would take too much time to teach Clojure here.
I just say that all the fonctions themselves are way shorter than in the Rust version since I could easily handle all exceptions globally with a catch handler (Clojure relies on exception for errors).
The price is: I'm less precise in the messages I give.
But I could avoid this by adding more catch handlers at the price of verbosity.
I'll also point out that all functions that ends with an exclamation mark like ``insert!`` mutate the sate by convention and must not be confused by by the exclamation mark used to identify macros in Rust.
With that, you should be able to get a high level understanding of what the program does.

Like above, here is my ``project.clj`` with project dependencies:

.. include:: ../static/small-todo-apps/project.clj
    :code: Clojure
    :number-lines:
    :name: project.clj

and my ``src/todo/core.clj`` source file:

.. include:: ../static/small-todo-apps/core.clj
    :code: Clojure
    :number-lines:
    :name: src/todo/core.clj


Haskell
=======

Haskell is also very different from other languages I normally use and it's often cited as the reference of functional languages.
In Haskell, functions are more like mathematical functions, all variables are immutable and all side effects are encapsulated to maintain function purity.
It also relies on indentation to express the code and don't need semi-colon or curly braces or to wrap everything in parentheses.
The type system is very expressive and is a bit hard to read and understand when you start (mostly when many Monads are involved in functions signatures).
Like for Clojure, I'll analyse part of the code but won't attempt to teach you Haskell.

The parsing of the arguments relies heavily on the type system and on the combination of function.
For instance, to parse the title used to create a TODO, first I create a union type to hold all the commands (``deriving Show`` is an easy way to be able to print the data):

.. code:: Haskell

    data Command =
        List
        | InsertTestData
        | Create CreateOptions
        | Complete CompleteOptions
        deriving Show

Then, I create a type named ``CreateOptions`` to hold the options:

.. code:: Haskell

    data CreateOptions = CreateOptions { title :: String }

then a function to parse the option themselves:

.. code:: Haskell

    createOptionsParser :: Parser CreateOptions
    createOptionsParser = CreateOptions
        <$> strOption (long "title" <> short 't' <> help "The title of the TODO." <> metavar "TITLE")

That's readable but to really understand what is going on you need to understand what ``<$>`` and ``<>`` do which isn't simple and which I won't explain there.

I can then create the parser for the command itself:

.. code:: Haskell

    createParser :: Parser Command
    createParser = Create <$> createOptionsParser

which can then be used in the subcommand parser.

The arguments will be parsed in a ``do`` block to handle side effects (another important point I won't explain).

The most interesting part is the use of pattern matching to run the proper command based on the actual type of the parsed command:

.. code:: Haskell

    runCommand :: Command -> Connection -> IO ()
    runCommand List = listAll
    runCommand InsertTestData = insertTestData
    runCommand (Create CreateOptions { title = title }) = createTodo title
    runCommand (Complete CompleteOptions { todoId = todoId }) = completeTodo todoId

The parser will create a ``Command`` type of "type" ``List``, ``Create``… and we will use this syntax to execute specific code depending on the actual type of the command and extract their parameters with destructuring.
I find that very expressive and concise.
And it avoids to have complex ``if`` or ``case``.
I also note that with its usage of parenthesis, Haskell code sometimes makes me thing of LISP.

Now that I managed to parse the options, I can do things, for instance create a TODO:

.. code:: Haskell

    createTodo :: String -> Connection -> IO()
    createTodo title conn = do
        insertResult <- try (execute conn "INSERT INTO todo (title, is_done) values (?, ?)" (title, False :: Bool)) :: IO (Either SomeException ())
        case insertResult of
            (Left error) -> putStrLn $ "Failed to insert the todo: " ++ show error
            (Right result) -> pure ()

The way I handle the errors highly resemble how it's done in Rust.
The ``try`` function is a way to catch the potential errors from the executed query.
Without it, the code would crash if ``execute`` encountered an error.
So yes, Haskell code can compile and crash at runtime if you don't pay attention (or don't know what you are doing) in some cases.
I find that a bit weird given the preconception I had about the language which was all errors must be handled and if it compiles it works.
To be fair, this can also happen in Rust, when you try to access an index that doesn't exist in a array or with invalid use of ``unwrap`` for instance.
I guess no language is the silver bullet in that matter.

As usual, here's the code of my ``app/Main.hs`` file which serves as an entry point with parsing logic:

.. include:: ../static/small-todo-apps/Main.hs
    :code: Haskell
    :number-lines:
    :name: app/Main.hs

and the ``src/Lib.hs`` with the rest of the logic:

.. include:: ../static/small-todo-apps/Lib.hs
    :code: Haskell
    :number-lines:
    :name: src/Lib.hs

Here are also my `stack.yaml <{static}../static/small-todo-apps/stack.yaml>`__ file and may `package.yaml <{static}../static/small-todo-apps/package.yaml>`__.
I didn't include them here since they are way longer that the other dependencies files and give extra details about how the project must be compiled.


Wrapping up
===========

Some stats to compare each solution:

- The full code of the Rust version is 220 lines, but only 164 lines of code when I remove the lines with the closing curly which we don't have in other languages.
  It's also the app where I have the most detailed and precise error messages.
  It felt right to write it this way.
- The Clojure version is 147 lines long.
- The Haskell version is 149 lines long.

So I'd say in terms of code length, they all are pretty much the same, with Rust being a bit more verbose.

I'd like to say I enjoyed writing all these apps.
The Haskell one was the most challenging to write: it's the language I know the least and is the most different from what I am use to writing.
Their usage of Monad and of ``IO`` in type signature was (and still is a bit) obscure to me.
I think these signatures can be obvious to someone at ease with Haskell, but as a newcomer, without examples to illustrate how the function actually works, it was hard to understand.
I often had to search the Internet for examples to help me write the code.
I also struggled a bit with the operators ``<$>`` and ``<*>``.

I'm also a bit disappointed by the Clojure version: the argument parser is less complete than the other ones and I was expecting more of it.
I also still have issues reading the code at time, mostly when I want to scan the code to find something.
Maybe it's just a habit to work on.

I think the Rust one shines: it's expressive, errors must always be handled correctly and I found many examples as well as solid library to implement what I was trying to do.
This small experiment made the language more interesting for me.
It's definitely the one I'm most exited about although I still don't think I'll use the language much aside from toy project for now: I find it a bit verbose and complex for the web app I am writing.

I'll try to write a small web API in the following weeks to complete my experience (and for fun) and write an complementary blog post.

In July, I finally wrote the follow up article: `Small API to manage packages <{filename}./2021-07-10_small-packages-api.rst>`__.
