Mettre sous git sous Drupal
###########################

:tags: Drupal, git

Récemment j'ai mis l'install Drupal du projet `multi
assos <http://assos.centrale-marseille.fr>`__ sous git (vous pouvez
aussi utiliser un autre DVCS si vous voulez). Voilà mon petit compte
rendu.

Intérêt
-------

La première question que vous vous poserez sûrement est pourquoi placer
Drupal sous git ? Parce que c'est utile. Cela :

-  vous permet de suivre les évolutions de l'installation : mise à jour,
   installation/suppression de module.
-  vous fournit un moyen simple de revenir en arrière en cas de
   problèmes (mauvaise mise à jour)
-  vous permet de comparer les différentes versions de vos settings.php
   et .htaccess
-  savoir qu'un fichier utile (comme un .htaccess) a disparu
-  faire des tests en local facilement et les pusher sur le seveur s'ils
   sont concluants.

Préparations
------------

La question qui doit nous guider lors de cette étape est : que devons
placer sous git ? En effet, il ne faut pas faire un brutal *git add
drupal*.

Que contient une installation de Drupal ?

-  Drupal en lui même
-  Les modules et thèmes
-  Les dossiers des sites qui contiennent

   -  un settings.php
   -  les fichiers du sites

Nous voulons tous suivre sauf les fichiers du site (parce qu'ils
changent tous de temps et n'ont rien à faire dans le dépôt) et les
données de connexion à la base de données pour chacun des sites.

Nous devons modifier la façon dont Drupal gère les settings.php pour que
les mots de passes de la base de données ne se retrouvent pas dans le
dépôt. Pour ce faire :

#. créez dans le dossier sites un fichier settings.global.php. Ce
   fichier sera commun à tous les sites. Déplacez-y toutes les
   informations génériques (commentaires, les fonctions qui permettent
   de configurer PHP) et tout ce qui touche à la sécurité.
#. créez dans chaque dossier de site un fichier settings.local.php et
   placez-y les informations spécifiques au site (accès base de données,
   base url,…)
#. remplacez le contenu du settings.php par ceci :

.. code-block:: php

    <?php
    // Load the global settings file.
    require dirname(__FILE__) . '/../settings.global.php';

    // Load the local settings file with databases connexion.
    require dirname(__FILE__) . '/settings.local.php';

Ceci étant fait, préparons le .gitignore. Nous devons y ajouter les
dossiers des sites. (default et assos.centrale-marseille.fr.\* pour
nous). Apdaptez à votre installation.

Reste également vos scripts éventuels. De notre côté plusieurs ont dû
être modifiés. La liste est là (par ordre chronologique) :

#. https://forge.centrale-marseille.fr/projects/clubdrupal/repository/revisions/1c1c5a429b70f697d7504e5a30f92594fa388ff0
#. https://forge.centrale-marseille.fr/projects/clubdrupal/repository/revisions/c01204bf932788df810f08d9d3b2ffe828bd94de
#. https://forge.centrale-marseille.fr/projects/clubdrupal/repository/revisions/6d91f38e4a472a3e517b86e4a66c31e18e018bd3

Le passage
----------

Maintenant que nous sommes prêts, il n'y a plus qu'à faire un petit *git
add* et de faire votre premier commit. Pensez à commiter dès que vous
faîtes un changement dans votre installation (avec des scripts au
besoin).

Conclusion
----------

On sait enfin ce qui se passe et on est certain de pouvoir revenir en
arrière facilement au besoin. Vous pouvez évidement mettre vos scripts,
template, … dans le dépôt. De notre côté, on a aussi un script pour
synchroniser un site et faire des tests dessus en local. Il est
`ici <https://forge.centrale-marseille.fr/projects/clubdrupal/repository/changes/other-scripts/d7-sync.sh?rev=master>`__
(et sa dépendance
`ici <https://forge.centrale-marseille.fr/projects/clubdrupal/repository/changes/other-scripts/modify-settings.py?rev=master>`__).
