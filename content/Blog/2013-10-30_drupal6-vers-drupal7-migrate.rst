De drupal 6 à drupal 7 : migrate_d2d
####################################

:tags: Drupal

Après `la première partie sur drush
sup </posts/2013/Oct/30/drupal6-vers-drupal7/>`_, voilà la
deuxième et dernière partie de mon retour d'expérience sur les
migrations Drupal. Aujourd’hui c'est le module
`migrate\_d2d <http://drupal.org/project/migrate_d2d>`__ (pour migrate
drupal to drupal) qui est à l’honneur. Je ne parlerai ici que de la
version 2.1 (encore en développement à l'heure où j'écris ces lignes)
car c'est avec elle que le projet introduit *migrate\_d2d\_ui* une joli
interface pour vous simplifier la tâche.

Ce module très sympa vous permet de repartir sur de bonnes bases, dans
un site Drupal 7 tout neuf. Il m'a été très utile pour migrer le site
des `annales <http://assos.centrale-marseille.fr/annales>`__ (accessible
uniquement pour les élèves de l'école) (dont la base de données était
corrompue ce qui empêchait drush sup de fonctionner) et pour le `blog
des expats <http://assos.centrale-marseille.fr/expat>`__ dont la version
drupal 7 est en cours de validation de notre côté pour lequel drush sup
ne migre pas correctement les images.

Préparation et migration
------------------------

Ce coup ci, votre site drupal 7 ne va pas se créer tout seul. Il faut
donc tout d'abord créer un site drupal 7 neuf, comme d'habitude. Activer
tout les modules nécessaires, créer vos types de contenu avec les bons
champs, les rôles et les vocabulaires de taxonomie.

Une fois que le site drupal 7 est prêt, il faut télécharger et activer
les modules *migrate* et *migrate\_d2d* (pour le moment, prendre
les\ **versions de développement** des deux modules, sinon vous n’aurez
pas droit à l'interface).

Direction *Contenu > Migrate > Import from drupal*. Après l'interface
détaille bien les étapes. On commence par remplir les infos sur la base
de données, puis on choisit pour chaque type de contenu du drupal 6 le
type de contenu d'arrivée dans le drupal 7. Idem pour les rôles. Pour
les fichiers, il suffit de lui donner l'emplacement sur le serveur. On
sauvegarde.

Ensuite, retour sur le dashboard, on sélectionne ce qu’on veut importer
et c'est parti !

Les problèmes
-------------

J'ai eu quelques problèmes avec les préfix de bases de données qui
n'étaient apparament pas pris en compte. J’ai retesté hier et c'est
passé. Le problème a dû être corrigé en amont.

Les formats de texte n’étaient également pas toujours bien pris en
compte. Mais ça, c'est facile à régler avec du SQL.

Le gros problème est que si la table *file\_managed* était bien remplie,
les tables file\_usage et celles qui spécifient dans quel champ le
fichier est utilisé sont restées désespérément vides. De même pour la
taxonomie. Les termes du vocabulaire sont bien créé mais ils ne sont pas
associé à chaque contenu. Du coup j’ai dû faire un script pour récupérer
les données dans le Drupal 6 pour remplir les tables du Drupal 7.

Conclusion
----------

Cela conclue mes migrations de Drupal 6 à 7. En résumé, quand drush sup
fonctionne, c’est génial. On a alors vraiment l'impression que tout est
magique et automatique. Par contre, sans ça devient galère, surtout s'il
faut aller dans la base de données pour finir la migration. Ça allonge
considérablement le temps nécessaire pour terminer. D'autant plus que je
n'avais aucune expérience sur les bases de données de Drupal avant. Mais
bon, c'est l'occasion d'apprendre ;-)

Les migrations vers drupal 7 c'est bientôt fini pour nous. Les derniers
Drupal 6 qui restent sont en train d'être refait depuis zéro par leur
webmaster respectif. En tout cas, je comprends pourquoi sur
`drupal.org <http://drupal.org>`__ il aura fallu plus d’un an pour faire
la migration.
