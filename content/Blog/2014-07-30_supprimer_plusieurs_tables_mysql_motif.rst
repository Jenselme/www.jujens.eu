Comment supprimer toutes les tables mysql qui contiennent un motif
##################################################################

Récemment, j'ai eu besoin de faire du ménage dans les tables d'une base de
données. Toutes les tables à supprimer commençaient par le même
motif. Malheureusement, mysql ne permet pas d'utiliser le jocker ``%`` dans une
requête ``DROP``.

Heureusement la solution est simple. Il suffit de lister toutes les tables ayant
ce motif grâce à la requête ``SHOW TABLES LIKE 'motif%'``. Reste à boucler sur
ces tables pour les supprimer. D'où le code suivant :

.. code:: bash

	  motif=del_
	  for table in $(mysql database -NBe "SHOW TABLES LIKE '$motif%'"); do
	    mysql database "DROP TABLE $table"
	  done

L'option ``-N`` de mysql masque les entêtes de colonnes et l'option ``-B``
permet d'avoir un résultat brut, exploitable dans la boucle.

On peut également faire `le script plus générique suivant <|static|/static/supprimer_plusieurs_tables_mysql_motif/mysql_drop_table_like.sh>`_ :

.. include:: ../static/supprimer_plusieurs_tables_mysql_motif/mysql_drop_table_like.sh
   :code: bash
