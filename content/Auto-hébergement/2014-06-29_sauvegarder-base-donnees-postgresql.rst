Comment sauvegarder une base de données PostgreSQL
##################################################

:tags: PostgreSQL, backup, Bash/Shell

Je vous propose une solution pour vous aider à sauvegarder votre base de données
PostgreSQL. La commande est ``pg_dump``. Elle prend comme argument (au minimum)
:

- Le nom d'utilisateur
- Le mot de passe
- Le nom de la base de données
- L'adresse IP du serveur

Donc la commande de base pour sauvegarder une base de données est :

::

   pg_dump -h 127.0.0.1 -U owncloud -WMDP owncloud

Le problème est que l'on doit fournir le mot de passe à chaque
fois. Heureusement, il y a une autre solution : le fichier .pgpass. Dans votre
home, créer un fichier ``.pgpass``.

Chaque ligne de ce fichier contient les informations sur une base de données
sous la forme :

::

   IP:Port:Database:Username:Password

Soit par exemple :

::

   127.0.0.1:5432:owncloud:owncloud:MPD

La sauvegarde se lance ensuite avec :

::

   pg_dump -h 127.0.0.1 -U owncloud owncloud

Reste à mettre la commande dans le `crontab
<http://fr.wikipedia.org/wiki/Crontab#Syntaxe_de_la_table>`_.
