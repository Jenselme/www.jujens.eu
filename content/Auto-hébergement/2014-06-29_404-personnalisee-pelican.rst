Créer des pages d'erreurs personnalisées pour son blog pelican
##############################################################

:tags: nginx, pelican

Rien de très compliqué. Par exemple, pour les erreurs 404, il suffit :

- de créer une page 404.rst avec le status *hidden* :

.. code:: rst

   :title: 404
   :status: hidden

   La page demandée n'existe pas. Vous pouvez revenir à `l'accueil <http://www.jujens.eu>`_.

- d'ajouter une ligne dans sa configuration nginx :

.. code:: nginx

   error_page 404 /pages/404.html;

Et voilà ! Vous pouvez évidement faire de même pour les autres erreurs, mais je
doute qu'elle soit aussi courante.
