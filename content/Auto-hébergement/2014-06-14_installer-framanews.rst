Comment installer framanews sur son serveur avec PostgreSQL et nginx
####################################################################

:tags: framanews, tt-rss, nginx, PostgreSQL

`Framanews <https://framanews.org/>`_ est un fork de `ttrss
<http://tt-rss.org/redmine/projects/tt-rss/wiki>`_, un lecteur de flux RSS en
ligne. Il vous permet donc de consulter vos flux, de vous y abonner, …
directement depuis un navigateur, où que vous soyez. Il dispose aussi d'une
`application Android <http://tt-rss.org/redmine/projects/tt-rss-android/wiki>`_.
Il est évidement possible d'importer un fichier opml existant pour garder tous
ses flux.

Vous pouvez utiliser Framanews sur des serveurs de `Framasoft
<http://www.framasoft.net>`_ ou sur votre serveur. C'est ce que nous allons
faire ici.

Installer les bons paquets
==========================

Pour faire fonctionner framanews, nous avons besoin de :

- ``git`` pour récupérer framanews et ses plugins ou ses thèmes.
- ``PostgreSQL`` comme système de base de données.
- ``php`` et de ses extensions pour fonctionner avec ``PostgreSQL``.
- ``nginx`` (serveur web)

Sur un système basé sur debian, nous pouvons installer tous ça avec :

::

   sudo apt-get install php5 php5-pgsql php5-fpm php-apc php5-curl php5-cli
   sudo apt-get install git postgresql nginx

Sur un système basé sur CentOS/RHEL :

::

   yum install postgresql postgresql-libs postgresql-server
   yum install php-fpm php-gd php-mbstring php-pgsql php-pdo
   yum install git nginx

Reste à activer les bons services (nous les lancerons un peu plus tard) :

::

   service nginx enable
   service postgresql enable
   service php-fpm enable


Récupérer framanews
===================

Framanews est disponible sur github. Pour le récupérer, placez vous dans le
dossier où vous voulez le placer (``/var/www/`` pour moi) et lancez :

::

   git clone https://github.com/framasoft/framanews_ttrss.git

Framanews sera alors placé dans ``/var/www/framanews_ttrss`` ce que je ne trouve
pas parlant. Je propose de renommer ce dossier en framanews avec :

::

   mv framanews_ttrss framanews

Ensuite, nous devons donner les droits au serveur pour qu'il puisse faire son
travail correctement. La commande peut varier en fonction de votre système. Sous
debian et affiliée, l'utilisateur s'appelle ``www-data`` et mais ``apache`` sous
CentOS/RHEL et affiliée. On fera donc au choix :

::

   chown -R www-data:root framanews/

ou :

::

   chown -R apache:root framanews/


Configurer nginx
================

Rien de très compliqué. Copiez/collez simplement les lignes suivantes dans :

- ``/etc/nginx/conf.d/ttrss.conf`` sur CentOS/RHEL
- ``/etc/nginx/sites-available/ttrss.conf`` sur Debian

.. code:: nginx

   server {
      listen 80;
      server_name VOTRE_SERVEUR; # ttrss.jujens.eu pour moi
      root /var/www/framanews; # Changer ce chemin au besoin
      index index.php;

      location ~ \.php$ {
          try_files $uri =404;
          fastcgi_pass unix:/var/run/php5-fpm.sock;
          fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
          include fastcgi_params;
      }

Il ne reste plus qu'à lancer nginx et php-fpm :

::

   service nginx start
   service php-fpm start

Vous pourrez trouver des informations complémentaires utiles dans `mes notes sur
nginx <http://wiki.jujens.eu/doku.php?id=server:nginx>`_.

Avec cette configuration, **le trafic (dont votre mot de passe) est en
clair**. Pour faire du https avec nginx et des certificats auto-signés, je vous
propose `ce post </posts/2014/Jun/29/nginx-https/>`_.


Configurer la base de données
=============================

Autoriser la connexion
----------------------

PostgreSQL possède de `nombreux mécanismes d'authentification
<http://www.postgresql.org/docs/current/static/auth-methods.html>`_. Nous allons
vérifier que le mécanisme configuré est le bon. Pour cela :

- Sous debian, éditez :
  ::

     /etc/postgresql/9.1/main/pg_hba.conf

- Sous CentOS, éditez :
  ::

     /var/lib/pgsql/data/pg_hba.conf

Je vous propose la configuration suivante :

.. code:: kconfig

   # TYPE  DATABASE    USER        CIDR-ADDRESS          METHOD
   # "local" is for Unix domain socket connections only
   local        all         all                               ident
   # Allow auth with password
   #local	all	    all				      password
   host	        all	    all		127.0.0.1/32	      password
   host	        all	    all		::1/128		      password

Elle autorise :

- La connexion par socket local
- La connexion à toutes les bases de données pour tous les utilisateurs
  (utilisateurs de la base de données, pas les utilisateurs du système) sur les
  adresses 127.0.0.1/32 avec mot de passe.
- Idem pour les adresses ipv6


Créer la base de données
------------------------

Nous allons maintenant créer la base de données et son utilisateur à l'aide des
commandes ci-dessous (gardez le nom de la base de données, le nom de
l'utilisateur et son mot de passe, nous allons en avoir besoin dans pas longtemps).

::

   root@jujens:/home/jenselme# su - -c "psql" postgres
   psql (9.1.13)
   Saisissez « help » pour l'aide.

   postgres=# DROP USER ttrss;
   DROP ROLE
   postgres=# CREATE USER ttrss WITH PASSWORD 'SUPER_MDP';
   CREATE ROLE
   postgres=# CREATE DATABASE ttrss OWNER ttrss ENCODING 'UTF8';
   CREATE DATABASE
   postgres=# GRANT ALL PRIVILEGES ON DATABASE ttrss TO ttrss;
   GRANT

Reste à lancer PostgreSQL :

::

   service postgresql start

Pour sauvegarder, je vous invite à lire `cet article </posts/2014/Jun/29/sauvegarder-base-donnees-postgresql/>`_.


Finaliser l'installation
========================

Allez sur l'adresse que vous avez choisie pour Framanews (ttrss.jujens.eu pour
moi). Il reste à remplir avec les informations demandées (à adapter à votre cas) :

- Type de base de données : PostgreSQL
- Username : ttrss
- Password : SUPER_MDP
- Database name : ttrss
- Port : 5432

.. image:: /images/installer-framanews/ttrss-1.png
	   :alt: Remplir les infos de base de données
	   :width: 600px

On teste la configuration :

.. image:: /images/installer-framanews/ttrss-2.png
	   :alt: Tester la configuration
	   :width: 600px

Et on sauvegarde le fichier de conf :

.. image:: /images/installer-framanews/ttrss-3.png
	   :alt: Fichier de conf
	   :width: 600px

Et voilà !

.. image:: /images/installer-framanews/ttrss-4.png
	   :alt: Framanews
	   :width: 600px

Je vous laisse faire un tour dans les options pour adapter Framanews à vos
besoins.


Mise à jour des flux
====================

Par défaut, les flux ne se mettent pas à jour via l'interface web. La solution
recommandée est d'entrer la ligne suivante dans un terminal :
::

   su www-data -c './update.php --daemon& > /dev/null&'

Malheureusement, dès que vous quitterez le terminal, le programme sortira. Je
propose donc plutôt d'utiliser `le crontab
<http://fr.wikipedia.org/wiki/Crontab#Syntaxe_de_la_table>`_. Pour éditer le
crontab, tapez (en tant que root) :
::

   crontab -e

et ajouter la ligne suivante pour une mise à jour toutes les 30 minutes (le
chemin est à adapter) :
::

   */30 * * * * su www-data -c '/usr/bin/php    /var/www/framanews/update.php --feeds --quiet'

Pour information : cette ligne lance en tant que l'utilisateur ``www-data`` le
script update.php.

Si vous n'avez pas de crontab, vous pouvez utiliser la mise à jour en arrière
plan. Framanews va alors tenter de mettre à jour les flux tant qu'il est ouvert
dans votre navigateur. Pour cela, dans ``/var/www/framanews/config.php`` mettre
``SIMPLE_UPDATE_MODE`` à ``TRUE``.


Plugins et thèmes
=================

ttrss (et donc Framanews) peut être étendu grâce à des plugins et son pas très
joli thème de base peut être changé. La liste des plugins est disponible `ici
<http://tt-rss.org/redmine/projects/tt-rss/wiki/Plugins>`_ et celle des thèmes
`là <http://tt-rss.org/redmine/projects/tt-rss/wiki/Themes>`_. Installer un
thème ou un plugin n'est pas compliqué.


Plugins
-------

Par exemple, pour le plugin framarticle_toolbar, il suffit de :

- Se placer dans le dossier *plugin* de votre installation de Framanews
  (/var/www/framanews/plugin pour moi.
- Lancer :
  ::

     git clone https://github.com/framasoft/framarticle_toolbar.git
     chown -R www-data:root framarticle_toolbar/

On l'active dans *Actions > Préférences > Préférences > Plugin*.


Thème
-----

On procède comme pour un plugin, mais en se plaçant dans le dossier theme. On
active le thème dans *Actions > Préférences* directement dans l'onglet préférence.


Sources
=======

- Le site officiel de framanews : https://framanews.org/install.php
- Le site officiel de Tiny Tiny RSS : http://tt-rss.org
- How To Install TTRSS with Nginx for Debian 7 on a VPS :
  https://www.digitalocean.com/community/tutorials/how-to-install-ttrss-with-nginx-for-debian-7-on-a-vps
- Tiny Tiny RSS on CentOS with nginx and MYSQL :
  https://www.zufallsheld.de/2013/11/03/tiny-tiny-rss-centos-nginx-mysql/
- Changing the tt-rss database from MySQL to Postgresql :
  https://www.zufallsheld.de/2014/02/25/changing-the-tt-rss-database-from-mysql-to-postgresql/
