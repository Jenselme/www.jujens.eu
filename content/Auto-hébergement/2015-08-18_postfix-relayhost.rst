Utiliser postfix pour relayer ses mails vers un autre serveur
#############################################################

:tags: mail, postfix

J'ai récemment eu besoin de pouvoir envoyer des mails depuis ma machine avec un
serveur de mails tournant en local. La façon la plus simple de faire ça est à
mon avis d'avoir un serveur de mail qui relaie les mails vers un autre serveur
mail. Pour des cas simples (juste l'utilisation de la commande ``mail``,
``ssmtp`` est suffisant, voir configuration à la fin). Pour avoir un vrai
serveur smtp, j'ai décidé d'utiliser ``postfix`` qui fait ça très bien et que
j'utilise sur mon serveur.

En prérequis :

- installer ``postfix``
- disposer d'un autre serveur smtp sur lequel on peut se connecter (le vôtre,
  Gmail, Outlook, …)

Éditer ``/etc/postfix/main.cf`` et ajouter :

.. code:: ini

   relayhost = smtp.exemple.com:587
   smtpd_sasl_path = smtpd
   smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
   smtp_sasl_type = cyrus
   smtp_sasl_auth_enable = yes
   smtp_use_tls = yes

   # Pour que le seveur ne relaie que les mails venant de localhost
   # Attention, c'est le réseau 127.0.0.0/8 qui doit être utilisé et
   # pas l'ip (127.0.0.1 ou 127.0.0.1/8)
   mynetworks = 127.0.0.0/8, [::1]/128
   smtpd_recipient_restrictions = permit_mynetworks, reject_unauth_destination

   # Nécessaire uniquement si le serveur smtp a besoin que le mot de passe
   # soit envoyé en texte clair.
   smtp_sasl_security_options = noanonymous

   # Nécessaire uniquement votre fournisseur utilise du load balancing et
   # transfère les mails vers un autre serveur SMTP.
   smtp_cname_overrides_servername = no


Il reste ensuite à créer le fichier de mots de passes. Dans
``/etc/postfix/sasl_passwd`` avec :

.. code:: ini

   smtp.exemple.com:587 nomutilisateur:mdp

Ensuite, utiliser la commande ``postmap hash:/etc/postfix/sasl_passwd`` pour
générer fichier empreintes des mots passes utilisé par postfix. Vous pouvez
tester la configuration avec ``postmap -q smtp.provider.com:25
/etc/postfix/sasl_passwd`` qui doit renvoyer ``nomutilisateur:mdp``. Vous pouvez
désormais supprimer ``/etc/postfix/sasl_passwd`` et redémarrer postfix.

Note : il est possible que vous ayez besoin de lancer ``newaliases`` pour
terminer la configuration initiale du serveur.

Comme j'ai écrit la conf pour ssmtp, la voilà (dans ``/etc/ssmtp/ssmtp.conf``) :

.. code:: ini

          mailhub=smtp.example.com:587
          AuthUser=nomutilisateur
          AuthPass=mdp
          # Si le serveur utilise StartSSL, ne pas utiliser avec UseTLS=yes
          UseSTARTTLS=YES
