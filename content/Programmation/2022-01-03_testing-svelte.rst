Testing the Svelte JS framework
###############################
:lang: en
:tags: JavaScript

After hearing good things about the `Svelte <https://svelte.dev/>`__ JS framework, I decided to give it a try.
To test it, I did something very original: the classic TODO app.
You can see a picture of the end result on the screenshot below.

.. image:: /images/2022-01-03_testing-svelte.png
    :alt: The styled app

Here are my impressions after this tests:

* Despite using a custom compiler and putting everything in one file with a separation between JavaScript, HTML and CSS (very much like Vue.js), it felt like I was writing standard JS and HTML. That's a good thing and something I don't like with React. You do need to pay attention to update arrays for the app to be rerendered, but that wasn't much of a trouble (and still seems easier than React).
* Simple forms are simple: you can just bind a variable in two ways. If you change the input, the variable is updated and vice versa. It feels way smoother than React. I think it could be an issue when rendering complex forms, but I guess you have ways to mitigate this (by limiting the two way binding to the form scope).
* I'm impressed by how simple it is to add classes dynamically to an element: ``class:CLASS_NAME={condition}``. For instance ``class:done={todo.done}`` or simply ``class:done`` if you need to use a ``done`` variable.
* I added style with `tailwaind CSS <https://tailwindcss.com/>`__ based on `this tutorial <https://css-tricks.com/how-to-use-tailwind-on-a-svelte-site/>`__. It was easy enough but I found that recompile time increased from time to time. It's weird, but I didn't dug up. And as usual with tailwind, you need many custom components to reuse CSS classes (which I didn't do for this test).
* The result feels fast and svelte (pun intended). By default in development mode, the bundle is less than 200kB with a ``node_modules`` folder only 22MB. In contrast, React above 1.5MB with almost 300MB of dependencies in ``node_modules``. Once tailwind was installed, the bundled went up to 3MB but shrunk to around 20kB in production. Again, React alone (without a CSS framework!) is about 150kB in production.

So, I think Svelte is a very good looking framework, I liked using it.
I mostly liked that my code looks "normal" and putting all in one file: at least for small components, it's easier to do it this way.
This test hyped me and I hope to see it again in the future!

Here is my code: https://gitlab.com/Jenselme/test-todo/-/tree/main/svelte
