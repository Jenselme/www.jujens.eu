Détecter une architecture avec CMake
####################################

:tags: CMake, C/C++

Il est tout à fait possible de détecter l'architecture de l'hôte avec
CMake et d'adapter votre Makefile avec cette info. Il suffit d'utiliser
la variable *CMAKE\_SYSTEM\_PROCESSOR*. Ci dessous un petit exemple :

.. code:: cmake

    if(${CMAKE_SYSTEM_PROCESSOR} MATCHES armv6l)
        include_directories(${OpenCV_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS} ~/git/robidouille/raspicam_cv)
        link_directories(${OpenCV_LIB_DIR} ~/git/raspberrypi/userland/build/lib ~/git/robidouille/raspicam_cv)
        target_link_libraries(${PROJECT_NAME} ${OpenCV_LIBS} ${Boost_LIBRARIES} raspicamcv mmal_core mmal mmal_util vcos bcm_host X11 Xext rt stdc++)
    else()
        include_directories(${OpenCV_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})
        target_link_libraries(${PROJECT_NAME} ${OpenCV_LIBS} ${Boost_LIBRARIES})
    endif()
