Periodically import an ical calendar into owncloud
##################################################

:tags: Python, Calbdav, Owncloud
:lang: en
:slug: periodic-import-ical-owncloud
:modified: 2015-09-21

I always thought that owncloud should be able to periodically import calendars
found on the web. In my case, I have a schedule as an ical feed I would like
synchronize with my owncloud. I finally wrote a script to do that.

Initially, I intended to do it in Bash and to use ``curl`` to do requests to
owncloud. However, an ical feed contains line breaks. I had a lot of problems to
manage them correctly. So I wrote my script in python the `excellent requests
library <http://docs.python-requests.org/en/latest/>`_.

This script :

- reads in ``~/.owncloud`` the address and the ids of the owncloud server,
- fetches the events described in the ical feed I ask it to synchronize. Theses
  events are stored in an instance of ``Vevent``. I had the possibility to
  filter out some events,
- deletes the events present in my owncloud but not in the ical feeds,
- put (create or update) the events in owncloud.

I think this script is simple and clear enough to be easily read. In case of
troubles, you can post a comment.

You can also `download this script
<|static|/static/periodic-import-ical-owncloud/edt.py>`_ and see it `on my
mercurial on bitbucket
<https://bitbucket.org/Jenselme/server-bin/src/469c28a3ae87228f1ac3f3d0ad57349dda77e5b8/edt.py?at=default>`_.

.. include:: ../static/periodic-import-ical-owncloud/edt.py
   :code: python
   :number-lines:
