Make JS code communicate with a ServiceWorker
#############################################
:tags: JavaScript, Web, ServiceWorker
:lang: en

If you need to send data from your JS code to your ServiceWorker, you can do it like this:

.. code:: JavaScript

    navigator.serviceWorker.register("./sw.js").then(registration => {
        registration.active.postMessage("Some message");
    });

You can then rely on this code in the ServiceWorker to read the message:

.. code:: JavaScript

    self.addEventListener("message", event => {
        console.log(event.data); // This will log: Some message
    });
