Compilation conditionnelle avec CMake
#####################################

:tags: Make, CMake, C/C++

Lorsqu'on fait de gros programme C ou C++, il peut être intéressant de
ne compiler une partie du programme que si une option est passée en
argument. Nous allons voir comment le faire en utilisant
`CMake <http://fr.wikipedia.org/wiki/CMake>`__, un générateur de
MakeFile très connu et très puissant. `Voir notre article sur
CMake </clubrobot/content/les-bases-de-cmake>`__ pour les bases de
CMake.

Considérons pour cet article, que le programme doit afficher *coucou* si
l'option est présente et rien sinon. Entourons remplaçons simplement la
ligne `` std::cout << "coucou" << std::endl; `` par :

.. code:: c++

    #if defined(COUCOU)
        cout << "coucou" << endl;
    #endif

Il est tout à fait possible d'utiliser ``#ifdef`` à la place de
``#if defined`` mais on ne peut alors imbriquer les conditions :
``#if ( defined(COUCOU) && defined(SALUT) )``

Il n'y a plus qu'à ajouter dans le CMakeLists.txt :

.. code:: cmake

    option(COUCOU "Active coucou" OFF)
    if(COUCOU)
        MESSAGE( STATUS "COUCOU SUPPORT")
        add_definitions(-DCOUCOU)
    endif()

Pour compiler, utilisez simplement :

-  Pour activer COUCOU : ``cmake .. -DCOUCOU=on && make``
-  Pour ne pas activer COUCOU : ``cmake .. -DCOUCOU=off && make``
