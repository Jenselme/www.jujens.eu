My take on UV and Ruff
######################
:tags: Python
:lang: en

I recently tried the new and shiny tools made by `astral <https://astral.sh/>`__.
I only used them on my personal projects yet, but I’m still very impressed!
You may already have heard of them.
I’ll try to keep the article concise and won’t dig too deep into the tools to prevent this article to be outdated fast: these tools are under very active development and thus change quickly.
The drawbacks listed here may not be a think any more when you’ll read it.


Ruff
====

``ruff`` the linting tool to unite them all.
It replaces ``flake8`` and most of its plugins, ``pylint`` and your formatter (most likely ``black`` nowadays).
It’s very feature complete with over 800 rules.
And growing!
It’s main selling point is: it’s blazing fast.

I was personally impressed by its speed both to validate a file and to reformat it.
The difference of speed with ``flake8`` is noticeable and huge from ``pylint`` (which was always slow).

Integration with `pre-commit <https://pre-commit.com/>`__ comes out of the box, so it should be easy to run it both locally and in CI.
`The documentation <https://docs.astral.sh/ruff/integrations/>`__ also notably mentions GitHub actions, Gitlab CI and Docker.

Formatting works great in Pycharm on save by configuring a save action.
Linting doesn’t work out of the box (it might change, see `this GitHub issue <https://github.com/astral-sh/ruff/issues/10102>`__).
But since PyCharm has good linting built in, I only encountered a few linting errors on pre-commit.
So not a big deal for me.

It also has `an official VSCode extension <https://marketplace.visualstudio.com/items?itemName=charliermarsh.ruff>`__.
I never tested it, so no opinions on that.
Given its rating and usage, I guess it’s very good.

With any luck, it will also `replace type checkers <https://github.com/astral-sh/ruff/issues/3893>`__ one day with a much faster alternative.

The only issue I see is if you have custom ``flake8`` plugins: ``ruff`` `doesn’t yet support plugins <https://github.com/astral-sh/ruff/issues/283>`__.
But you could only rely on ``flake8`` for that and use ``ruff`` for everything else.


uv
==

``uv`` is a new package manager. It can replace ``pip`` and tools like ``poetry`` to manage your dependencies.
It supports dependencies groups and lock files.
Everything will be into your ``pyproject.toml`` file following the latest PEP.
Sadly, `dependabot is still not compatible with it <https://github.com/dependabot/dependabot-core/issues/10478>`__, so no update notifications yet.
I hope this comes soon.

It can also replace ``pipx`` to install and run Python commands.
All you need to do is to install the tool with ``uv tool install flake8``.
You can then use the ``flake8`` command as usual (provided ``~/.local/bin`` is in your ``PATH``). You can also download and run a tool once with ``uvx flake8``.

Even better, you can add dependencies to your script and let ``uv`` create a venv and run the script in it with all its dependencies automatically!
To do so:

#. Create your script.
#. Add the dependencies with ``uv add --script example.py DEPS``
#. Run with ``uv run example.py``

It can also download a Python interpretor if you don’t have it already installed on your machine.
So if a script is only compatible with a version of Python you don’t have, ``uv`` will still be able to run it for you automatically.

Once again, with ``uv`` you don’t need anything else to run Python scripts and projects!


Conclusion
==========

I’m very impressed by what they built and how fast they made it.
I strongly suggest you test these tools.
Given the vide of the community, you’ll probably use them too!
And you’ll probably be able to replace all your flake8 and ``pylint`` workflows with them and make them run faster!
And to run your script without a sweat and built in dependency management!

I also think the future of these tools will be great: they are open source after all, so the community should be able to take them back if needed.
Given how used and liked they are, I don’t see them disappear.
The only thing that could be an issue: astral is a VC backed company, so they will have to make money somehow at some point.
No idea what form it will take though.

And you, do you have any thoughts on these tools? Did you use and like them?
