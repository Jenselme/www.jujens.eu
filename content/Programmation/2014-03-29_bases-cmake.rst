Les bases de CMake
##################

:tags: CMake, C/C++

CMake est un utilitaire qui génère des MakeFile. Son principal intérêt
est d'avoir une syntaxe plus légère que le MakeFile de base. Les
bibliothèques les plus connues (Boost, OpenCV, …) sont compatibles avec
ce système. Ci dessous un petit exemple pour vous aider à démarrer. Il faut
enregistrer le contenu du fichier dans ``CMakeLists.txt`` puis dans le dossier
où vous voulez produire vos binaires, entrez : ``cmake VERS_CMakeLists.txt``

.. code:: cmake

    project(image)
    cmake_minimum_required(VERSION 2.8)
    FIND_PACKAGE(OpenCV REQUIRED)
    FIND_PACKAGE(Boost COMPONENTS system filesystem REQUIRED)
    include_directories(${OpenCV_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS})
    link_directories(${OpenCV_LIB_DIR} ${Boost_LIBRARY_DIR})
    aux_source_directory(. SRC_LIST)
    add_executable(${PROJECT_NAME} ${SRC_LIST})
    target_link_libraries(${PROJECT_NAME} ${OpenCV_LIBS} ${Boost_LIBRARIES})

Et voilà un CMakeLists.txt basique. À vous de creuser pour en faire des plus complexes.
