A basic shell for pelican
#########################

:tags: Bash/Shell, Linux, Pelican
:lang: en
:slug: shell-pelican

My blog uses `Pelican <http://docs.getpelican.com>`_ a static website
generator. All my articles are under version control. To generate after each
modification the new html version of the pages, you must use the ``make
regenerate``. To launch the test web server, you use ``make serve``. Finally to
synchronize the content on the server, you use ``make rsync_upload``. Each time
I do this, I also push my modifications to my server and to `Bitbucket
<https://bitbucket.org/Jenselme/www.jujens.eu/overview>`_ in order to have a
backup.

Until now, I managed all that in a standard terminal. I already had a bash
function to switch to the proper directory and enable python's venv. However, I
did not find that very practical. ``make serve`` and ``make regenerate`` must
always stay in background, I have to push to two different repositories and
synchronize with the server. So I decided to create a script to improve that.

Since I need to launch the same commands in the same folder without leaving the
venv and since I want to easily interact with the background commands, I decided
to create a small shell. I am very happy with it.

Some explanations on the script (available below):

- Everything is in an infinite loop to simulate a true shell. ``echo -ne "> "``
  print a prompt and let the cursor on the same line.
- ``exec 3< <(make serve)``

  - ``<(make serve)`` is replaced by a path to a virtual file connected to the
    standard output of the command.
  - Since we want the process to be in background, we launch it with ``exec``.
  - Finally, since we want to fetch the output of the command, we connect the
    file to a new output named 3. We then read its content with ``cat
    <&3``. **While the file is not closed, this command won't stop.**
  - Got to `this thread
    <http://stackoverflow.com/questions/20017805/bash-capture-output-of-command-run-in-background?answertab=active#tab-top>`_
    for more details.

- Additional output: I tried to store the name of the ouputs in variables. That
  would have allowed me to refactor and clarify the code. However, that makes
  ``exec "${var}"< <(make serve)`` and ``cat <&"${var}"`` crash.
- ``[ -n "${serve_pid}" ] && has_died "${serve_pid}"``:
  ``has_died`` must not be surrounded by brackets (that means it must not be
  pass to the ``test`` command) in order for the script to work properly.
- ``echo ${pid}``: you can only send error code with ``return``. For everything
  else, you must use ``echo``.

You will find the whole script below. You can also `download it
<|static|/static/shell-pelican/blog.sh>`_ ou le `see it on github
<https://github.com/Jenselme/dot-files-shell/blob/master/bin/blog.sh>`_ if you
want the latest version

.. include:: ../static/shell-pelican/blog.sh
	     :code: bash
	     :number-lines:
