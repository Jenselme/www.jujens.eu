Install npm packages with non root user
#######################################

:lang: en
:slug: install-npm-packages-as-user
:tags: JavaScript, npm

``npm`` can install packages either in the current folder or globally with the
``-g`` option. By default, in this mode, ``npm`` installs packages in
``/usr/lib/node_mudole``. You must be root to do this and it can be very
dangerous if you are installing an untrusted package.

However, ``npm`` can also install packages in your home. To do this, you must
first create the folder which will contain all your packages :

.. code:: bash

	  mkdir -p ~/.npm-packages

You must then configure the prefix - where ``npm`` installs its packages - with
the ``prefix`` variable in ``~/.npmrc``. So add ``prefix =
/home/username/.npm-packages`` ``~/.npmrc``.

Your shell must then have the proper environment variables. So add to your.zshrc
or .bashrc :

.. code:: bash

	  # NPM
	  NPM_PACKAGES="$HOME/.npm-packages" # So that npm finds its packages.
	  PATH="$NPM_PACKAGES/bin:$PATH" # To launch script directly from the shell.
	  unset MANPATH
	  MANPATH="$NPM_PACKAGES/share/man:$(manpath)" # So that man finds the manpages.
	  NODE_PATH="$NPM_PACKAGES/lib/node_modules:$NODE_PATH" # So that node finds these packages.
	  export NPM_PACKAGES PATH MANPATH NODE_PATH

That's all. Now ``npm install -g bower`` will install ``bower`` in your home
without the need to be root!
