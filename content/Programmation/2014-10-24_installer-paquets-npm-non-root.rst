Installer des paquets npm sans les droits root
##############################################

:slug: install-npm-packages-as-user
:lang: fr
:tags: JavaScript, npm

``npm`` peut soit installer un paquet dans le dossier du projet courant soit de
façon globale pour tous les projets avec l'option ``-g``. Par défaut dans ce
mode, ``npm`` installe les paquets dans ``/usr/lib/node_module`` ce qui
nécessite les droits root. En dehors du désagrément que cela procure, cela peut
être dangereux si on installe un paquet depuis une source inconnue.

Heureusement, ``npm`` peut aussi installer les paquets dans le home de
l'utilisateur courant. Pour cela, il faut tout d'abord créer le dossier qui
contiendra les paquets :

.. code:: bash

	  mkdir -p ~/.npm-packages

Reste à configurer le préfixe de ``npm`` (c'est là qu'il installe ces paquets)
avec la variable ``prefix`` dans le ``.npmrc``. Ajoutez donc ``prefix =
/home/username/.npm-packages`` à votre npmrc (``~/.npmrc``).

Il faut ensuite que votre shell ait les bonnes variables d'environnement. Pour
cela, ajoutez à votre .zshrc ou .bashrc :

.. code:: bash

	  # NPM
	  NPM_PACKAGES="$HOME/.npm-packages" # Pour que npm trouve ces paquets.
	  PATH="$NPM_PACKAGES/bin:$PATH" # Pour que les scripts puissent être lancés directement depuis le terminal.
	  unset MANPATH
	  MANPATH="$NPM_PACKAGES/share/man:$(manpath)" # Pour que man trouve les manuels des commandes.
	  NODE_PATH="$NPM_PACKAGES/lib/node_modules:$NODE_PATH" # Pour que node trouve les paquets.
	  export NPM_PACKAGES PATH MANPATH NODE_PATH

C'est fini. Maintenant, ``npm install -g bower`` installera ``bower`` dans votre
home sans les droits root.
