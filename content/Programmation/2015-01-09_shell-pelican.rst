Un shell pour pelican
#####################

:tags: Bash/Shell, Linux, Pelican

Comme je l'ai déjà dit `ici
<{filename}/Blog/2014-04-22_passage-pelican.creole>`_, mon blog est géré par
`Pelican <http://docs.getpelican.com>`_. Il est également placé sous gestion de
version. Pour régénérer automatiquement après chaque modification la version
html des pages, il faut utiliser la commande ``make regenerate``. Pour lancer le
serveur de test pour voir les pages, on utilise ``make serve``. Enfin, pour
synchroniser le contenu sur le serveur, j'utilise ``make rsync_upload``. J'en
profite pour pousser les modifications de mon dépôt local vers mon serveur et
vers `Bitbucket <https://bitbucket.org/Jenselme/www.jujens.eu/overview>`_ pour
en avoir une copie de sauvegarde.

Jusqu'à présent, je gérais tout ça à la main, dans un terminal. J'avais déjà
écrit une petite fonction bash pour me déplacer dans le bon dossier et activer
le venv python. Mais ce n'était pas très pratique : les commandes ``make serve``
et ``make regenerate`` doivent tourner en permanence en tâche de fond, il faut
pusher sur deux dépôts différents et synchroniser les pages sur le serveur. J'ai
donc décider de me créer un script pour améliorer tout ça.

Comme j'ai besoin de lancer plusieurs commandes bien définies dans un même
dossier, sans sortir de l'environnement virtuel et que je veux pouvoir
interagir facilement avec les processus en tâche de fond, j'ai décidé de créer
un mini-shell. Je dois admettre que j'en suis très satisfait.

Quelques explications sur le script :

- Tout est dans une boucle infinie pour imiter un vrai shell. ``echo -ne "> "``
  permet d'afficher un prompt et de laisser le curseur sur cette ligne.
- ``exec 3< <(make serve)``

  - La syntaxe ``<(make serve)`` est remplacée par chemin vers un fichier
    virtuel qui est connecté à la sortie standard de la commande située entre
    les parenthèses. Ici ``make serve`` qui démarre le serveur.
  - Comme on veut que le processus soit en background, on lance la commande avec
    ``exec``.
  - Enfin, comme on veut pouvoir récupérer facilement la sortie de la commande,
    on envoie le contenu du fichier vers une nouvelle sortie 3. On lit son
    contenu avec ``cat <&3``. **Tant que le fichier n'est pas fermé, cette
    commande ne se retourne pas.**
  - Voir `cette réponse sur stackoverflow
    <http://stackoverflow.com/questions/20017805/bash-capture-output-of-command-run-in-background?answertab=active#tab-top>`_
    pour plus de détails.

- Sorties additionnelles : j'ai tenté de stocker le numéro des sorties dans des
  variables. Le code est plus clair et cela permet de le
  factoriser. Malheureusement, ``exec "${var}"< <(make serve)`` provoque une
  erreur. Idem pour ``cat <&"${var}"``.
- ``[ -n "${serve_pid}" ] && has_died "${serve_pid}"`` : la fonction
  ``has_died`` ne doit pas être entre crochets (donc passée en argument à la
  fonction ``test``) pour que le script fonctionne correctement.
- ``echo ${pid}`` : on ne peut pas renvoyer autre chose que des codes d'erreur avec
  ``return``. Pour tout le reste, il faut utiliser ``echo``.

Ci-dessous le script complet. Vous pouvez aussi le `télécharger
<|static|/static/shell-pelican/blog.sh>`_ ou le `voir sur github
<https://github.com/Jenselme/dot-files-shell/blob/master/bin/blog.sh>`_ pour
avoir la version la plus à jour.

.. include:: ../static/shell-pelican/blog.sh
	     :code: bash
	     :number-lines:
