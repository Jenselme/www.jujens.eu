AngularJS tips
##############

:lang: en
:tags: JavaScript, AngularJS
:modified: 2015-06-20

This page lists all the angularjs tips I have collected.

.. contents::


Disable HTTP request caching
============================

This is mostly useful on Internet Explorer: every HTTP requests made with the ``$http`` service will be cached by the browser. This means that if a resource changed on the server, refetching it, by default, will not show the modification to the client. The application will be presented the request from the cache of the browser. To prevent that, we disable the cache for HTTP requests.

.. code:: javascript

	module.config($httpProvider => {
		// Disable caching for Internet Explorer.
		// See: http://stackoverflow.com/a/19771501
		$httpProvider.defaults.cache = false;
		if (!$httpProvider.defaults.headers.get) {
			$httpProvider.defaults.headers.get = {};
		}
		// Disable IE ajax request caching
		$httpProvider.defaults.headers.get['If-Modified-Since'] = '0';
	});


Launch an action after a $digest
================================

Use ``$timeout`` with only one parameter:

.. code:: javascript

          $timeout(function do_after_digest() {});


Download binary data
====================

You must set ``responseType`` to ``'arraybuffer'`` in the options of the requests, like this:

.. code:: javascript


	$http({
		url: 'your/webservice',
		method: "POST",
		data: json, //this is your json data string
		headers: {
		'Content-type': 'application/json'
		},
		responseType: 'arraybuffer'
	}).success(function (data, status, headers, config) {
		var blob = new Blob([data], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"});
		var objectUrl = URL.createObjectURL(blob);
		window.open(objectUrl);
	}).error(function (data, status, headers, config) {
		//upload failed
	});

Cf : http://stackoverflow.com/a/22448640


Debugging
=========

Note: ``$0`` refers to the last selected element by the debug tool. Works at
least on Firefox and Chrome. In all these examples, this element must be under
the controller.

Get the scope
-------------

.. code:: javascript

	  var scope = angular.element($0).scope()

Get a service
-------------

.. code:: javascript

	  var myService = angular.element($0).injector().get('myService')
