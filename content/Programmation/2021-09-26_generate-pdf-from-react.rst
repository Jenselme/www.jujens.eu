Generate PDF from React components
##################################
:lang: en
:tags: JavaScript, React, Chrome, Puppeteer

How do you generate a PDF from any React components?
I am not talking about just allowing your users to print a page into a PDF with their browser, I am talking about transforming a page as PDF programmatically and saving it server side.
The idea is for our user to see what the page looked like before moving forward in the application and this page becoming unaccessible.

.. note:: The final solution I describe, since it relies on a browser can be applied to any other framework like Angular or Aurelia or Vue.

There are several options to generate PDFs from React:

- `@react-pdf/renderer <https://www.npmjs.com/package/@react-pdf/renderer>`__ to generate beautiful PDF with dedicated components and only from its dedicated components. It's the best solution if you want a beautiful PDF and have the time/need for a dedicated template.
- `jsPDF <https://rawgit.com/MrRio/jsPDF/master/docs/>`__ with its ``html`` method which can transform any HTML element into a PDF. However, from experience, you need to adjust the scale of the page so it can if into the PDF and you can have render issues (with unbreakable spaces for instance).
- A combo based on `html2canvas <https://html2canvas.hertzen.com/>`__ and `jsPDF <https://rawgit.com/MrRio/jsPDF/master/docs/>`__ to transform any HTML element into an image which you can then integrate into a PDF document. The main problem being: it's an image (so text cannot be selected), you cannot easily split it across multiple pages and if you try to put it into a very long page, it won't print properly. It's also quite slow.

All these libraries work client side.
``@react-pdf/renderer`` is also compatible with NodeJS so it can run server side.
So, where to render the PDF, client side or server side?

Rendering it client side is appealing: no need for a dedicated service and you can leverage the browser to render the components. But, you are dependent on the browser of the user (the screen size will impact how the document is rendered and how it will be printed) and you must trust your user not to change anything.

Rendering it server side is more complex: you need a service to do it, but you render the document in an environment our control. You inject the data you want and render it the way you want. The service itself is complex: you need to launch a browser to render the components.

In our use case, we wanted to be sure the PDF was generated with our values without influences from media queries.
So this meant rendering it server side.
Since we couldn't use ``@react-pdf/renderer`` (we wanted to render arbitrary components), what could we do?
Well, we decided to create a NodeJS service that runs a headless Chrome managed by `Puppeteer <https://github.com/puppeteer/puppeteer>`__ to render the components.
We then use the printing functions of Puppeteer and some CSS rules to print the page into a PDF with margins and a custom footer.
The result isn't perfect: just like when you print a page with the browser, you can have page break in middle of words, but it's a fairly good compromise between time and quality.

Here is how we did it:

#. We created a custom ``index-pdf-server.ts`` file to contain a dedicated entry point for the app. It configures our MaterialUI theme, our translations and selects which components to render on the page. It is also responsible for getting the data. To avoid roundtrips, we inject the data into the page with Puppeteer. It works like that:

   #. We launch Puppeteer.
   #. It loads the React app.
   #. We inject data into a dedicated element in the page with Puppeteer.
   #. The React app loads it and renders.
   #. We wait for the page to render in Puppeteer.
   #. We transform the page into a PDF.

   The gist of this index is this (imports omitted):

   .. code:: JavaScript
    :number-lines:

    const rootEl = document.getElementById('root');

    const Component: React.FC = () => {
        const intl = useIntl();

        const [reactData, setReactData] = useState<ReactData>();

        useEffect(() => {
            if (reactData) {
                return;
            }

            const interval = setInterval(() => {
                const reactDataElement = document.getElementById('react-data');
                if (reactDataElement && reactDataElement.textContent) {
                    setReactData(JSON.parse(reactDataElement.textContent));
                }
            }, 100);

            return () => clearInterval(interval);
        }, [reactData]);

        const page = useMemo(() => {
            if (!reactData) {
                return;
            }

            // Switch to get the component to render.
        }, [intl, reactData]);

        const canDisplayPage = useMemo(() => !reactData || !page, [reactData, page]);

        if (canDisplayPage) {
            return null;
        }

        return (
            <div>
            <div>{page}</div>
            {/* We will wait for this element to detect the end of render. */}
            <div id="print" />
            </div>
        );
    };

    if (rootEl) {
        ReactDOM.render(
            <ThemeProvider theme={theme}>
            <RawIntlProvider value={intlData}>
                <Component />
            </RawIntlProvider>
            </ThemeProvider>,
            rootEl,
        );
    }

#. Our app uses `react-app-rewired <https://github.com/timarney/react-app-rewired>`__ for app compilation. In order to use this ``index-pdf-server.ts`` file, we had to modify a bit our ``config-overrides.js`` file. In a nutshell, when we compile the app for the server, we set a ``SERVER`` environment variable to change the entry point and disable CSP and SRI. The most important part is this:

   .. code:: JavaScript

    if (process.env.SERVER) {
        config.entry = config.entry.replace('index', 'index-pdf-server');
    }

   This allows us to reuse almost all our code and configuration while being able to compile an app dedicated to our server. This is done with ``SERVER=true env-cmd -f .env.${REACT_APP_ENV} react-app-rewired build && cp -R build pdf-server/``.

#. We added some CSS code inside ``@media print`` media queries to improve the display and hide some buttons for print. As a nice side effect, if your users try to print a page, it will be more beautiful.

   .. tip:: You can use the ``break-after`` or ``break-before`` rules to force page breaks. For instance, ``break-after: page;`` will force a page break after this element.

#. We can then create a ``server/server.js`` file in our project to handle the backend side of things. It has two routes: one to server the built application and one to receive the PDF request with its data (and respond with the PDF). It looks like this:

   .. code:: JavaScript
    :number-lines:

    const puppeteer = require('puppeteer');
    const express = require('express');
    const bodyParser = require('body-parser');
    const path = require('path');
    const winston = require('winston');
    const pdf = require('pdf-parse');

    // Allowed pages.
    const allowedPages = [];

    const port = process.env.PORT || 4000;
    const logLevel = process.env.LOG_LEVEL || 'info';
    const waitForSelectorTimeout = process.env.WAIT_FOR_SELECTOR_TIMEOUT
        ? parseInt(process.env.WAIT_FOR_SELECTOR_TIMEOUT)
        : 15_000;
    const pdfGenerationRetryCount = process.env.PDF_GENERATION_RETRY_COUNT
        ? parseInt(process.env.PDF_GENERATION_RETRY_COUN)
        : 3;

    const logger = winston.createLogger({
        level: logLevel,
        format: winston.format.json(),
        defaultMeta: { service: 'frontend-pdf-server' },
    });

    if (process.env.NODE_ENV === 'production') {
        logger.add(
            new winston.transports.Console({
                format: winston.format.json(),
            }),
        );
    } else {
        logger.add(
            new winston.transports.Console({
                format: winston.format.simple(),
            }),
        );
    }

    const app = express();
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());

    const injectReactDataIntoPage = async (page, requestBody) => {
        logger.debug('Injecting data into the page.');
        await page.evaluate(reactData => {
            const node = document.createElement('script');
            node.setAttribute('type', 'application/json');
            node.setAttribute('id', 'react-data');
            node.innerText = JSON.stringify(reactData);
            document.body.appendChild(node);
        }, requestBody);
    };

    const print = async (page, timestamp) => {
        const margin = 30;
        return await page.pdf({
            format: 'A4',
            printBackground: true,
            omitBackground: true,
            margin: { top: margin, bottom: margin, left: margin, right: margin },
            displayHeaderFooter: true,
            footerTemplate: `<p style="font-size: 2mm; position: absolute; right: 50%; transform: translateX(-50%)">${timestamp}</p>`,
            headerTemplate: '',
        });
    };

    const checkPDF = async (pdfBuffer, timestamp) => {
        const data = await pdf(pdfBuffer);
        // Do we have text beside the timestamp in the footer? If yes, it's good.
        // Otherwise, the PDF is invalid (we generated it before render completed).
        return data.text.replace(timestamp, '').trim().length > 100;
    };

    const printAndCheckPDF = async (page, timestamp) => {
        let isValid = false;
        let retryCount = 0;
        let pdfBuffer = null;

        // Most of the time, waiting for this element is enough for the page to render correctly and for
        // us to get a proper PDF. Once in a while, no text is rendered and the PDF is empty. So we always
        // check the generated PDF.
        await page.waitForSelector('#print', { timeout: waitForSelectorTimeout });
        do {
            retryCount += 1;
            // Wait a bit for the render before trying again (invalid PDF) or to be sure to
            // get a full render (first try).
            await page.waitForTimeout(retryCount * retryCount * 100);
            pdfBuffer = await print(page, timestamp);
            isValid = await checkPDF(pdfBuffer, timestamp);
        } while (!isValid && retryCount < pdfGenerationRetryCount);

        if (!isValid) {
            throw new Error(`Failed to generate PDF, even after ${retryCount} tries.`);
        }

        return pdfBuffer;
    };

    /**
    * Generate a PDF from React frontend components with a Chrome headless
    * managed by puppeteer.
    */
    app.post('/react-to-pdf', async (req, res) => {
        if (!req.body.page || !allowedPages.includes(req.body.page) || !req.body.pdfData) {
            res.writeHead(400);
            res.end();
            return;
        }

        const browserConsoleMessages = [];
        try {
            const browser = await puppeteer.launch({
            headless: true,
            dumpio: true,
            args: [
                '--disable-gpu',
                '--disable-dev-shm-usage',
                '--disable-setuid-sandbox',
                '--no-sandbox',
                '--disable-software-rasterizer',
            ],
            });
            const page = await browser.newPage();
            await page.setViewport({ width: 1980, height: 768 });
            // Force language for translation and number formatting.
            await page.evaluateOnNewDocument(() => {
                Object.defineProperty(navigator, 'language', {
                    get: function() {
                    return 'fr-FR';
                    },
                });
                Object.defineProperty(navigator, 'languages', {
                    get: function() {
                    return ['fr-FR', 'fr'];
                    },
                });
            });
            await page.goto(`http://localhost:${port}`);
            page.on('console', message => {
                browserConsoleMessages.push(message.text());
            });

            logger.debug('Handling print request', req.body.page);

            await injectReactDataIntoPage(page, req.body);
            const pdfBuffer = await printAndCheckPDF(page, req.body.timestamp);

            logger.debug('Sending response.');
            res.writeHead(200, {
                'Content-Type': 'application/pdf',
                'Content-Length': pdfBuffer.length,
            });
            res.end(pdfBuffer);

            await page.close();
            await browser.close();
        } catch (e) {
            logger.error(e);
            logger.error(browserConsoleMessages.join('\n'));
            res.writeHead(500);
            res.end();
            throw e;
        }
        logger.debug('Print succeeded.');
    });

    app.get('/', async (req, res) => {
        res.sendFile(path.join(__dirname, 'build/index.html'));
    });

    app.get('/health', (req, res) => {
        res.writeHead(200);
        res.end();
    });

    app.use(express.static('public'));
    app.use(express.static('build'));

    logger.info(`Listening on port ${port}`);
    app.listen(port);

#. To deploy this service with Docker, you need to install several libraries as well as run the service as a user other than root. This Dockerfile can help you get started:

   .. code:: Dockerfile
    :number-lines:

    FROM node:14-slim AS builder
    WORKDIR /app

    RUN apt-get update && \
    apt-get install -y python3 make gcc g++ openssl ca-certificates

    ARG REACT_APP_ENV=undefined
    ARG REACT_APP_WEBSITE_BASE_URL=undefined
    ARG REACT_APP_COMMIT_SHA=undefined
    ARG COMMIT_SHA=undefined

    ENV REACT_APP_ENV=$REACT_APP_ENV
    ENV REACT_APP_WEBSITE_BASE_URL=$REACT_APP_WEBSITE_BASE_URL
    ENV REACT_APP_COMMIT_SHA=$REACT_APP_COMMIT_SHA
    ENV COMMIT_SHA=$COMMIT_SHA

    COPY . ./

    RUN yarn install --frozen-lockfile
    RUN yarn build-pdf-server


    # Run the pdf-server in node.
    FROM node:14-slim AS runner
    RUN mkdir -p /var/www/frontend-pdf-server/build/
    WORKDIR /var/www/frontend-pdf-server/

    RUN apt-get update && \
        apt-get upgrade -y && \
        apt-get install -y dumb-init \
            fonts-liberation \
            gconf-service \
            libappindicator1 \
            libasound2 \
            libatk1.0-0 \
            libcairo2 \
            libcups2 \
            libfontconfig1 \
            libgbm-dev \
            libgdk-pixbuf2.0-0 \
            libgtk-3-0 \
            libicu-dev \
            libjpeg-dev \
            libnspr4 \
            libnss3 \
            libpango-1.0-0 \
            libpangocairo-1.0-0 \
            libpng-dev \
            libx11-6 \
            libx11-xcb1 \
            libxcb1 \
            libxcomposite1 \
            libxcursor1 \
            libxdamage1 \
            libxext6 \
            libxfixes3 \
            libxi6 \
            libxrandr2 \
            libxrender1 \
            libxss1 \
            libxtst6 \
            xdg-utils && \
        apt-get clean

    RUN groupadd --gid 1001 noderunner && \
        useradd noderunner --create-home --uid 1001 --gid 1001

    COPY --from=builder /app/build /var/www/frontend-pdf-server/build/
    COPY --from=builder /app/pdf-server/ /var/www/frontend-pdf-server/

    RUN yarn install --frozen-lockfile

    USER noderunner
    ENTRYPOINT ["/usr/bin/dumb-init", "--"]
    CMD ["node", "server.js"]

#. As a bonus, if you also need to generate documents from a ``@react-pdf/renderer`` template, I suggest that you create the template in a JSX file and then import it with the `import-jsx <https://www.npmjs.com/package/import-jsx>`__ library like this:

   .. code:: JavaScript

    const importJsx = require('import-jsx');
    const { renderToStream } = require('@react-pdf/renderer');

    const Document = importJsx('./documents/my-document');

    const documentsToRenderFunctions = {
        MyDocument: Document,
    };
    const allowedDocuments = Array.from(Object.keys(documentsToRenderFunctions));

    /**
    * Generate documents based on dedicated React components from react-pdf/renderer.
    *
    * All the rendering is done in the NodeJS process.
    */
    app.post('/document', async (req, res) => {
        if (!req.body.document || !allowedDocuments.includes(req.body.document) || !req.body.pdfData) {
            res.writeHead(400);
            res.end();
            return;
        }

        logger.debug('Handling document generation request', req.body.document);
        try {
            const pdfStream = await renderToStream(
            documentsToRenderFunctions[req.body.document]({
                ...req.body.pdfData,
                timestamp: req.body.timestamp,
            }),
            );
            res.setHeader('Content-Type', 'application/pdf');
            pdfStream.pipe(res);
            pdfStream.on('end', () => logger.debug('Done streaming, PDF generation succeeded.'));
        } catch (e) {
            logger.error(e);
            res.writeHead(500);
            res.end();
            throw e;
        }
    });

To conclude, it's not as obvious as it seems.
I even wander after all the time I spent to create and stabilize this service if it wouldn't have been shorter to just create a proper template to render the PDFs.
Maybe yes, maybe no.
Our product team really wanted to have almost the same display in the PDF as in the page, so for them it was best this way.
I also think the way I inject data is not optimum: I could try to use a template and render a page that already has the data in it instead of injecting it and relying on a ``setInterval`` to read it.
But this would involve more work to make it work without interfering with the build process.

It's also hard to detect when React is done redering the page.
My method is probably not the best (wait for an element, then wait a bit for a timeout, then render the page and check we have text in the PDF), but it seems to work.

I'd say, despite all the issue this solution can have, right now it serves its purpose and allow us to move forward.
I hope you enjoyed this post and if you have any comments or remarks, please leave a comment!
