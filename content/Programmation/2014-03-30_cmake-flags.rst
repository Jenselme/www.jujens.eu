CMake et drapeaux de compilation
################################

:tags: CMake, C/C++

Pour ajouter des options à gcc, il suffit d'utiliser l'option set comme
ci dessous :

.. code:: cmake

    # Compiler flags
    if(CMAKE_COMPILER_IS_GNUCXX)
        set(CMAKE_CXX_FLAGS "-O2")        ## Optimize
        set(CMAKE_EXE_LINKER_FLAGS "-s")  ## Strip binary
    endif()
