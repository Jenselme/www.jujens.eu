Offline support almost without Javascript
#########################################
:lang: en
:tags: PWA, HTML, Javascript

Recently I wandered wether I could build a website with offline support without building a full SPA.
The answer is yes it’s doable: you only need Javascript for the service worker.
Just for the fun, I also tried it with navigation done with `HTMX <https://htmx.org/>`__ and without much surprise it also works.

.. warning::
    
    To do these tests, you must use a small webserver to serve your content.
    I used ``python3 -m http.server``.

    If you use Firefox, you must shutdown the server to be offline.
    If you go offline in the dev tools (for some reason I don’t know), your navigation won’t be handled by the service worker and you will appear truly offline.

Let’s start by building a simple ``index.html`` page:

.. include:: ../static/basic-offline-pwa/index.html
    :code: html
    :number-lines:

The HTML part should be straightforward.
In the ``script`` tag, I register my service worker (lines 7 to 15) and prefetch a page once it is ready so it’s in the cache (line 17 to 19).
The rest is just code to navigate between the different pages and a title to identify the page.

I build two other page by copy/pasting this into ``page1.html`` and ``page2.html`` and changing the ``h1``.
I then created my ``service-worker.js``:

.. include:: ../static/basic-offline-pwa/service-worker.js
    :code: js
    :number-lines:

In it, I add some content to the cache by default (lines 7 to 14).

I then register a listener to the ``fetch`` event so I can choose how to respond to HTTP requests (lines 16 to 36).
I choose to serve the response from the cache if I have it.
If not, I fetch it then put it in the cache for later usage.

It’s very basic, but it’s enough to make it work.
You can also test it by navigating `here <https://www.jujens.eu/static/basic-offline-pwa/>`__.

.. note::
    
    ``page2.html`` (fetched from the main page) won’t be put into the cache until you refresh the page.
    From what I understood, it’s for consistency: never serve from the cache until you are sure the service worker is active and working.

You can now test the app and see how it behaves.

To test with HTMX, I replaced the ``ul`` (lines 30 to 34) with either:

.. code:: html

    <!-- Test with HTMX boost to use standard links -->
    <ul hx-boost="true">
        <li><a href="/">Index</a></li>
        <li><a href="/page1.html">Page 1</a></li>
        <li><a href="/page2.html">Page 2</a></li>
    </ul>

or:

.. code:: html

    <!-- Test with HTMX links to use standard links -->
    <ul>
        <li><a hx-get="/" hx-push-url="true">Index</a></li>
        <li><a hx-get="/page1.html" hx-push-url="true">Page 1</a></li>
        <li><a hx-get="/page2.html" hx-push-url="true">Page 2</a></li>
    </ul>

That’s it.
The prefetching is very basic, but I think you get the idea: generate a list of URL and then call them.

If you want to learn more about a related topic, I wrote an article about `PWA and Django <|filename|./2020-02-29_django-pwa.rst>`__ a while back.
