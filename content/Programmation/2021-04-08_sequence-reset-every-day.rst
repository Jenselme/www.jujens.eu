Build a sequence that is reset everyday
=======================================
:tags: Django, Python, PostgreSQL
:lang: en
:modified: 2022-06-17

How to create a database sequence that will be reset everyday?
That is a sequence that will get one the first time of each day?
It sounds easy but it's not that much.

.. note:: This article will focus on PostgreSQL since it's the database I use. A similar solution may be possible with other database systems, it'll be up to you to adapt if needed.

.. note:: This article will focus on Django since it's the framework I use. A similar solution is possible with other frameworks. I think all mature ones will give you the tools you need to do this. Again, it'll be up to you to adapt if needed.


Use a Sequence?
---------------

It sounds like the obvious solution.
If you don't know what a sequence is, it's a feature for PostgreSQL which is used under the hood to create sequential primary keys.
We can create custom ones for our needs, and we can also reset them, so they can start over if needed.
So PostgreSQL would do all the work for us giving a very simple a robust solution.
And it works perfectly with transactions out of the box.

We can see it in action with:

.. code:: SQL

    -- Use the NO CYCLE option to be sure never to have the same sequence twice for the same day.
    CREATE SEQUENCE mysequence
        INCREMENT BY 1
        MINVALUE 1
        MAXVALUE 9999999
        START WITH 1 NO CYCLE;

We can then use it like this:

.. code:: SQL

    -- Launch this a few times to view the sequence increment.
    SELECT nextval('mysequence');

Or with transactions:

#. In a first ``psql`` console:

   .. code:: SQL

        BEGIN;
        SELECT nextval('mysequence');

#. In a second ``psql`` console while keeping the first transaction opened:

   .. code:: SQL

        -- This will display the next numbers of the sequence as intended.
        BEGIN;
        SELECT nextval('mysequence');

So we always immediately get the next number, no matter if the transaction was committed or not.
This means we can get holes our sequences if a transaction fails, but generally this shouldn't be a big deal.

We can then reset the sequence with a cronjob launched at midnight with:

.. code:: SQL

    ALTER SEQUENCE mysequence RESTART WITH 1;

So far so good.
However, they are issues with this solution:

- As stated earlier, we can have holes in the sequence. This shouldn't be a problem in most cases though.
- We must own the sequence to alter it, so it can be an issue to reset it in a cronjob if you restricted what the database user used to run the site can do. That's what I describe `here <{filename}../Blog/2021-03-10_db-user-migrations.rst>`__. If you don't apply this pattern or manage permissions differently for the sequence, it shouldn't be an issue.
- If we launch the reset of the sequence in a transaction, calls to get the next values will block until the transaction ends. This shouldn't happen since we reset the sequence in a dedicated command from a cronjob at a time where the site doesn't have much traffic. And altering the sequence is immediate if PG can get a lock on it.
- We need a cronjob to reset the sequence. It looks like nothing, and something that can be easily achieved on all platform. So it looks like a small issue like the ones listed above. In fact, it can be a show stopper: the cronjob will never pass exactly at midnight. In all crontab implementations I've seen, you can plan at most at the minute level. And all platforms I've seen, try to launch it as close as possible of the scheduled time, but it can vary from a few seconds to a few minutes depending on the implementation. And the command itself will need to launch and actually reset the sequence, which will take a big of time. All this means we can be in the following situation:

  #. Ask for the next value in the sequence for the first time of a day at 00:00:01. We expect to get 1, but, since the sequence hasn't been reset yet, we get something else. Let's say 10.
  #. The sequence is reset at 00:00:05.
  #. The day goes one, and we get 10 again near 17:00:00. Now, we got the same sequence twice in the same day.

  Is this an issue? It can be if you require the sequence to generate a unique value for a given day. In my case, that would mean a duplication in a column protected by a unique index (because I need this uniqueness). When can this not be a problem?

  - You don't care about the duplication.
  - If you know your system will never be used near the time the sequence is reset. However, *never* is a very strong words in systems used by humans. You may also forget about this constraint later and launch automated commands that will need the sequence.

  Can we mitigate the problem?

  - If we have a unique index where we use the sequence, we can detect the duplication and ask for another number from the sequence until we can insert the row. Sadly, I think this always makes the sequence logic leak to other part of the code.
  - We could store the date at which the sequence was reset and combine the date of this reset with the sequence number to get a number unique for each day. This requires a table. Since you have a table, it also means you don't need the sequence. Which leads us to the solution below.


Using a table
-------------

We can use a table which stores for each day the sequence number: we store the sequence associated with the date and then retrieve the value for the date, update it and use it.

This has several advantages:

- It's fairly easy to create the row and update it with Django. We can maintain uniqueness with a unique constraint on the date. Whereas with the sequence, some commands required raw SQL.
- We shouldn't have holes in the sequence if we handle the increment correctly with locks. These locks are required to avoid duplicated values anyway since Django doesn't support (at least not that I know of) a way to do ``UPDATE my_sequence_table SET inc = inc + 1 WHERE sequence_date = CURRENT_DATE RETURNING inc;`` to update and retrieve the value in one atomic instruction. The best we can achieve is ``UPDATE my_sequence_table SET inc = inc + 1 WHERE sequence_date = CURRENT_DATE;`` with ``MySequence.objects.filter(sequence_date=date.today()).update(inc=F('inc') + 1)``.
- We can detect potential duplication in the code that handles the sequence. It doesn't leak to other part of our code. This can occur at the very start of a day, when we still haven't created the sequence for a given day and two threads try to create it at the same time. One of the thread will wait for the other one to complete the insertion and then get an integrity error. Meaning we can detect the problem and retry by getting the next value out of the sequence.

On the downsides:

- The implementation is not obvious and requires locks and nested transactions to work properly. It can be a good idea to add ``lock_timeout=20s`` when you connect to the database to be sure locks won't block everything. This can be done like this:

  .. code:: python

    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.postgresql",
            "NAME": env.str("DB_NAME"),
            "USER": env.str("DB_USER"),
            "PASSWORD": env.str("DB_PASSWORD"),
            "HOST": env.str("DB_HOST"),
            "PORT": env.str("DB_PORT"),
            "OPTIONS": {
                # Configure a statement timeout to avoid running long statement even if the HTTP request
                # was killed or timed out.
                # See https://blog.heroku.com/postgres-essentials#set-a-code-statement_timeout-code-for-web-dynos # noqa: E501
                # Configure lock timeout to avoid blocking queries because of invalid locks.
                "options": "-c statement_timeout=30s -c lock_timeout=20s",
            },
        }
    }

- The table will keep growing while we only need one value per day.

  - Since there are only 365 days in a year, we shouldn't have too many rows in the table. We also have an index (required to maintain uniqueness per day) on the date column we use to retrieve values. So this should be a problem within a reasonable time frame.
  - We can mitigate this by cleaning the table from time to time if needed, so definitely not a big deal.

Here is a possible implementation.
Normally with the explanations above and the comments in the code you should be able to understand how it works.
If not, please leave a comment!

.. code:: python
    :number-lines:

    import logging
    from datetime import date

    from django.core.exceptions import ObjectDoesNotExist
    from django.core.validators import MinValueValidator
    from django.db import models, transaction
    from django.db.utils import IntegrityError

    logger = logging.getLogger(__name__)


    class SequenceManager(models.Manager):
        @transaction.atomic()
        def get_next_sequence_for_day(self, day: date):
            """Get the next sequence for the current day.

            If the sequence doesn't exist, we will create it. If it already does, we will
            update it.
            All this must be done in a transaction so we can correctly lock the sequence
            row for update and then update it it in one go and be sure no other threads
            will tangle with it.
            This way, we are sure not to get duplicated sequences.

            We need to do a select_for_update and then update/save the model since Django
            doesn't support the RETURNING feature of UPDATE statements, preventing us
            to do the update in one and retrieval in one query without using raw SQL.
            Since we need the model to create the sequence more easily, it makes sense
            to use the model all the way.

            Note: since the day column is unique, the database will wait for the insert
            of row with a given day (say 2021-03-01) before trying to insert another
            at the same day.
            This means, we correctly wait for the row to be created by the other thread
            before moving on.
            So, if we fail to create the row within this thread, we know another one
            created it.
            If we try to insert a row for a different day, nothing blocks as expected.
            """
            try:
                # This block may fail if another thread create the object after we checked
                # but before we created it. This result in an integrity error, meaning
                # we cannot use the current transaction for anything.
                # To prevent this, we nest the transactions. This inner transaction may
                # become invalid, but the surrounding transaction (at function level)
                # will stay valid allowing us to update the row while maintaining
                # data integrity.
                with transaction.atomic():
                    logger.debug(f"Trying to update the sequence for {day}.")
                    sequence = self._get_or_create_sequence_for_day(day)
                    self._update_sequence(sequence)
                    logger.debug(f"Succeeded to update the sequence as expected for {day}.")
            except IntegrityError:
                logger.debug(
                    "It looks like the row didn't exist yet so we tried to create it, "
                    "but another thread created it while we were checking. So we must "
                    "catch the potential integrity error and try to get the model again."
                )
                sequence = self._get_sequence_for_day(day)
                self._update_sequence(sequence)
                logger.debug(
                    f"Succeeded to update the sequence after integrity error for {day}."
                )

            return sequence.sequence

        def _get_or_create_sequence_for_day(self, day: date):
            try:
                return self._get_sequence_for_day(day)
            except ObjectDoesNotExist:
                logger.debug(f"Creating the sequence for {day}")
                return self.create(day=day)

        def _get_sequence_for_day(self, day: date):
            return self.get_queryset().filter(day=day).select_for_update(of=("self",)).get()

        def _update_sequence(self, sequence):
            sequence.sequence += 1
            sequence.save(update_fields=["sequence"])


    class Sequence(models.Model):
        """The goal of this model is to track the daily sequence.

        **This must not be manipulated directly, always pass by the manager!**
        """

        objects = SequenceManager()

        day = models.DateField(unique=True)
        sequence = models.PositiveIntegerField(
            validators=[MinValueValidator(0)], default=0
        )

You can then use it by calling the ``get_next_sequence_for_day`` method on the manager, like that:

.. code:: python

    Sequence.objects.get_next_sequence_for_day(day)

We can also run a more complete test:

#. In a first ``python manage.py shell_plus``:

   .. code:: python

        from time import sleep
        from datetime import date

        with transaction.atomic():
            print(Sequence.objects.get_next_sequence_for_day(date.today()))
            print(Sequence.objects.get_next_sequence_for_day(date.today()))
            sleep(10)

#. In a second one:

   .. code:: python

        from datetime import date
        Sequence.objects.get_next_sequence_for_day(date.today())

We will see 1 and 2 printed while in the other terminal.
We correctly see the statement to print 3 after waiting for the first transaction to complete.
Here we tested for the insertion of the first value of the day.
You can of course re-run this code to check the behavior for standard, concurrent usage.

We can also check the case when the transaction fails:

#. In a first ``python manage.py shell_plus``:

   .. code:: python

        from time import sleep
        from datetime import date

        with transaction.atomic():
            print(LoanApplicationBusinessIdSequence.objects.get_next_sequence_for_day(date.today()))
            print(LoanApplicationBusinessIdSequence.objects.get_next_sequence_for_day(date.today()))
            sleep(10)
            raise RuntimeError()

#. In the second:

   .. code:: python

        from datetime import date
        LoanApplicationBusinessIdSequence.objects.get_next_sequence_for_day(date.today())

We will see 4 and 5 displayed in the first terminal and then the error while the second terminal will wait and then display 4 as expected.


Wrapping up
-----------

This was not as easy as I though.
The sequence immediately popped into my mind when tasked to tackle this problem.
However, after some thoughts, it turned out to bring issues that I couldn't solve with it.
Hence my solution with the table, which I decided to use.
It's more complex but works as expected.
If you have a remark or comment, please speak below.


Updates
-------

* 2022-06-17: Add missing ``RETURNING`` statement in part about retrieve *and* update a row in one request. See `this discussion <https://www.jujens.eu/posts/en/2021/Apr/08/sequence-reset-every-day/#isso-294>`__.
