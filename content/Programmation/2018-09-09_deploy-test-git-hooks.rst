Deploy to your test server with git hooks
#########################################

:tags: git, devops
:lang: en

You probably already wanted to have your own test environment to allow others in the company to tests what you did. Perhaps you have a common one, but as your team is growing, it is probable that the common environment is a bottleneck and you wish each developer could have its own to test things in isolation. That's the subject of this article where we will create such an environment and deploy to it with a simple ``git push`` (a lot like `Heroku <https://heroku.com>`__ but a lot cheaper). You will also have full control over it and you will be able to modify the files directly on the server if needed (just don't do that last part in production ;-)).

In a nutshell, we need to support:

#. Code updates:

    #. Push to a current branch.
    #. Force push to a current branch
    #. Erasing unclean work tree on the server
    #. Pushing a new branch

#. Restart of the server (meaning the test environments must be up and running if the server restarts).
#. Environment variables to configure the application.

To achieve all this, all you need is a linux server. Your machine or a VM will do if you want to test along. If you want to use a VM, you may want to check `my tips for libvirt to use a static ip for the VM <{filename}/Trucs\ et\ astuces/2015-11-12_libvirt.rst>`__ and `Connect to a VirtualBox VM <{filename}/Trucs\ et\ astuces/2015-11-09_VirtualBox-connect-guest-en.rst>`__ to ease SSH connection in you VM.

To do it for real, a server in the office or in the cloud will do. If you need help, leave a comment, I'll do my best to provide feedback.

For the purpose of this tutorial, we will deploy a `small hello work app <https://gitlab.com/Jenselme/hello-flask>`__. You can fork it and use the fork as a remote repository if you want to follow along. The app is made in Python and there are a few Python specific things here and there but globally, you should be able to adapt it to any other languages. Leave a comment if you need help.

Before we start, some precision:

- If your server is running SELinux, you'll have to add proper context (the web one) to some file and allow the web server to connect to the UNIX or TCP socket of the application server. I won't go into this here, the tutorial is already long enough.
- I'll use jujens.bl.test as my test domain. Replace it with your own.
- Commands that must be run as root with start with ``#`` and as a normal user with ``$``. I'll precise the host too.
- You need new commits to push the code with git. If you change nothing, no hooks (more on that later) will run on the server because git is smart enough to see nothing changed.
- I assume your OS runs systemd. If not, adapt the commands.
- I'll present things in a tutorial manner so you can understand and follow what I do. Go to the `Summary`_ section to view a recap and what you need to do each time you want to add a developer.


.. contents::


Prepare the server
==================

#. Get a linux server (cloud, VM, your machine, Raspberry Pi, …)
#. Install the prerequisites

    - SSH
    - git
    - nginx (apache or any other web server can work too, you'll have to adapt the conf though)
    - Python (or anything else you need to run your application)

#. Configure SSH: you probably want to edit ``/etc/ssh/sshd_config`` to disallow direct root login (with ``PermitRootLogin no``), force SSH key usage (with ``PasswordAuthentication no``) and perhaps change the default port (eg with ``Port 422``). Search the internet to learn more.
#. Enable SSH: on the server, run ``systemctl start sshd`` and ``systemctl enable sshd``
#. Configure the firewall to let SSH and HTTP traffic through (adapt these commands if you changed any ports):

   - With firewalld: ``# firewall-cmd --add-service=http --permanent && firewall-cmd --add-service=http`` and ``# firewall-cmd --add-service=ssh --permanent && firewall-cmd --add-service=ssh``
   - With iptable: ``# iptables -t filter -A INPUT -p TCP --dport 22 -j ACCEPT`` and ``# iptables -t filter -A INPUT -p TCP --dport 80 -j ACCEPT``

#. Create at least one user (each dev should have their own user, more on that later). I'll use ``jujens``.
#. Create the ``deploy`` group: ``# groupadd -r deploy`` and add your user to it: ``# usermod -aG deploy jujens``
#. Start and enable nginx: ``# systemctl start nginx`` and ``# systemctl enable nginx``

To use the ``bl.test`` and ``jujens.bl.test`` domains, you must either modify your ``/etc/hosts`` by adding something like this (change the ip of the server): ::

    192.168.122.45 bl.test jujens.bl.test

Or use dnsmasq to redirect all requests made to ``bl.test`` and its subdomains to an ip. See `my article on that <{filename}/Trucs\ et\ astuces/2018-09-08_NetworkManager-dnsmasq.rst>`__

If your server is publicly accessible, you may add a wildcard entry to your DNS server so all subdomains of test.mypublicdomain.com are served by the test server. This way you will avoid the need to change your DNS configuration when you add a new developer.


Prepare the repo
================

Configure git
-------------

First, we will configure git. You can use a global configuration by editing ``/etc/gitconfig`` (users can still change it for all their repos in ``~/.gitconfig`` or per each repo in ``.git/config``).

.. raw:: html

   <p class="tip">
   Note that if you intend to do other things than git deploys with your server, the global configuration may not be the best choice.
   </p>

Here is the git configuration we'll use

.. code:: ini

    [core]
        hooksPath = /etc/git/hooks

    [receive]
        denyCurrentBranch = updateInstead

You can check it by running:  ``git config receive.denyCurrentBranch`` and ``git config core.hooksPath``.

.. raw:: html

   <p class="tip">
   You must adapt the path to where the git hooks will be. If you choose a per repo configuration, you don't need to specify it and it will default to <code>.git/hooks</code>
   </p>

.. raw:: html

   <p class="note">
   By default git won't allow you to push to a non bare repo (ie a repo in which you can access files easily) because it is dangerous. But in our case, it is required, otherwise, we won't be able to push anything while making the site work. That's the point of the ``denyCurrentBranch = updateInstead`` option. See <a href="https://git-scm.com/docs/git-config#git-config-receivedenyCurrentBranch">the documentation</a> for more details.
   </p>

Now we can create the directory where the global hooks will lie: ``mkdir -p /etc/git/hooks``. We will add scripts here later.

Prepare the server repo
-----------------------

#. Clone the remote repo on your dev machine (where you want) so you can push from it.
#. Clone the remote repo on the server. You could also push directly from your dev machine to it but I find it easier this way (and you still have to create a repo on the server). Do it:

   #. Create the folder ``/var/www/bl.test/jujens.bl.test`` (the location may seem odd right now, but it will have its importance later): ``# mkdir -p /var/www/bl.test/jujens.bl.test``
   #. Set user and group: ``# chown jujens:nginx /var/www/bl.test/jujens.bl.test``
   #. Set the proper permissions: ``# chmod u=rwX,g=rXs,o=- /var/www/bl.test/jujens.bl.test`` (this will give all permission the current user, read and "execute" permissions to the group and nothing to others. The ``s`` is there to set the "sticky bit". Thanks to it, all file created inside the directory will belong to the group you specify.
   #. Clone: ``# git clone https://gitlab.com/Jenselme/hello-flask.git /var/www/bl.test/jujens.bl.test``

#. Add the test remote on your dev repo (so you can push to the test server): ``$ git remote add test jujens@bl.test:/var/www/bl.test/jujens.bl.test``
#. Check everything is configure properly with a ``$ git push test`` It should output: ::

    The authenticity of host 'bl.test (192.168.122.45)' can't be established.
    ECDSA key fingerprint is SHA256:nb/a1ypBTTgahruVf92GzLNGTAdrrDi2afIWfXaFAa0.
    Are you sure you want to continue connecting (yes/no)? yes
    Warning: Permanently added 'bl.test' (ECDSA) to the list of known hosts.
    Everything up-to-date


How to send the code to the server
==================================

First, let me explain briefly what git hooks are since we will need them. Simply put, git hooks are scripts that are run before or after certain actions (commit, push, …). They are named ``pre-ACTION`` or ``post-ACTION`` and are handy to automatically run linting before committing or tests before pushing. By default they lie in ``PROJECT/.git/hooks`` (by the way, you can find samples in this folder). Here, we used the ``hooksPath`` to have global hooks.

**Don't forget to make these script files executable or they won't work** (I forget almost all the time!).

See `the doc <https://git-scm.com/docs/githooks>`__ for more details and the full list of available hooks.

Now that's you know what git hooks are, we will do each step at a time (git commands are run on you machine):

#. Step 1 *Push to a current branch*: Nothing to do since we configured git properly. Just modify ``hello.py``, commit and ``git push test``. You should see the commit and the modified file on the server.
#. Step 2 *Force push to a current branch*: Let's say we made a mistake in our previous commit, amended it and want to force push that. Modify the ``hello.py`` again and do ``git commit --amend -a`` to amend the last commit. ``git push test`` will fail but ``git push test --force`` will do just what we need. This will also work if you have new commits on the test server, just be aware they will be deleted.
#. Step 3 *Erasing unclean work tree on the server*: Modify ``hello.py`` on the server and don't commit. This is meant to simulate a debug modification you made after deploying the app. Now modify ``hello.py`` on your machine, commit and push. Neither ``git push test`` nor ``git push test --force`` will work because the work tree is unclean: you have a modified file whose content would be lost if the push succeeded. You could connect to the machine and run ``git checkout .`` yourself. But we can automate this with git hooks. Here we want to use the pre-receive hook that will be run before git gets the pushed content. In ``/etc/git/hooks/pre-receive`` (or ``.git/hooks/pre-receive`` if you want local hooks, I won't precise again), add:

    .. code:: bash

        #!/usr/bin/env bash

        # We launch the command in a subshell to avoid impacting the true environment with our unset.
        # We unset GIT_DIR because it points to the wrong directory.
        # We are in the .git directory when this hook is run, so we need to move one step up to run git commands.
        # We then force a checkout of the repo to discard any changes.
        ( unset GIT_DIR && cd .. && git checkout -f )

   And voilà! Our ``git push`` works!

#. Step 4 *Pushing a new branch*: this must be done once git received all the commits with the post-receive hook to update to the new branch. Git hooks are generally passed some parameters so we can do "smart" actions with them. Here, we will read the ``ref`` (name of the pushed branch). Create ``/etc/git/hooks/post-receive`` and add:

    .. code:: bash

        #!/usr/bin/env bash

        # Parameters are passed to stdin, we need to use read to get them. Only ref matters for what we do.
        read oldrev newrev ref
        # By default, ref contains refs/heads/branch-name. We use bash substitution mechanism to remove "refs/heads/".
        branch_name=${ref/refs\/heads\//}

        # Once again, force a checkout to the branch.
        ( unset GIT_DIR && cd .. && git checkout -f $branch_name )

Now we can correctly push our code the repo on the test server. On big leap forward. Now let's see how we can configure the server.


Configuring the web server
==========================

Basic setup of the application server
-------------------------------------

First, we will prepare our venv (or whatever else you need in your language) with the proper dependencies. This section must be adapted if you are not using Python. Run on the server:

#. Create the venv: ``$ python3 -m venv .venv``
#. Activate it: ``$ source .venv/bin/activate``
#. Install the dependencies: ``$ pip install -r requirements.txt``
#. Start the application server: ``$ gunicorn hello:app``
#. Check it is running with ``$ curl localhost:8000`` You should see the following output ::

    Hello World!

.. raw:: html

   <p class="tip">
   In a true server, you may want to use <code>--forwarded-allow-ips</code> to limit who can connect to guincorn. See: <a href="http://docs.gunicorn.org/en/latest/deploy.html">gunicorn's documentation</a>
   </p>

Now that it's done, we can add to the post-receive hook (``/etc/git/hooks/post-receive``) the following line to update our venv each time code is pushed:

.. code:: bash

    # We are again in .git by default.
    ( cd .. && source .venv/bin/activate && pip install -r requirements.txt )

Basic nginx configuration
-------------------------

Create nginx configuration file in ``/etc/nginx/conf.d/bl.test.conf`` (or ``/etc/nginx/site-enabled/bl.test.conf`` depending on your system, I'll use ``/etc/nginx/conf.d`` for the rest of this tutorial) with this content:

.. code:: nginx

    server {
        listen 80;
        server_name jujens.bl.test;
        # Put it to the folder that will contains all the repos.
        root /var/www/bl.test/;

        # Prevent access to source files.
        location ~ .*\.pyc? {
            return 404;
        }

        # Prevent access to git
        location ~ .git {
            return 404;
        }

        # Relay everything else to the application server.
        location / {
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Host $http_host;
            proxy_redirect off;
            proxy_pass http://127.0.0.1:8000;
        }
    }

Test the configuration with ``nginx -t`` and if everything is fine, reload nginx: ``systemctl reload nginx``. On your development machine, run ``curl jujens.bl.test`` to check everything is fine. You should see something like: ::

    Hello World!


Configuration for multiple users
================================

In the basic configuration section, we used a TCP socket (with a port) to access our application server. It works but if you want to add an environment for a new developer, you will need to:

#. Find an used port and configure gunicorn to use it.
#. Add a new nginx configuration so nginx accept request from the proper domain and routes them to the proper application server.

Thankfully, we can also use another kind of sockets: UNIX sockets. They exist as files on the server and if named with a proper convention, we can route our traffic easily. That's why I suggested to used the domain as the name of the folder that contains the code. It should become obvious once we completed it.

Configuring the application server
----------------------------------

We will rely on systemd to start and restart the application servers. First, create a file named ``/etc/systemd/system/gunicorn@.service``. It is a service template: it is meant to run multiple instances of a service based on a name (``jujens.bl.test`` for instance). The name will be accessible as a variable (``%i``) in the template so we can alter the behavior of the service based on it.

Put this content in the file (adapted from http://docs.gunicorn.org/en/stable/deploy.html#systemd):

.. code:: ini

    [Unit]
    Description=gunicorn daemon for %i
    After=network.target

    [Service]
    PIDFile=/var/run/gunicorn/%i.pid
    User=nginx
    Group=nginx
    Environment="PATH=/var/www/bl.test/%i/.venv/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin"
    RuntimeDirectory=gunicorn
    WorkingDirectory=/var/www/bl.test/%i
    ExecStart=/var/www/bl.test/%i/.venv/bin/gunicorn \
            --pid /var/run/gunicorn/%i.pid   \
            --bind unix:/var/run/gunicorn/%i.sock \
            hello:app
    ExecReload=/bin/kill -s HUP $MAINPID
    ExecStop=/bin/kill -s TERM $MAINPID
    PrivateTmp=true
    Restart=on-failure
    RestartSec=5s

    [Install]
    WantedBy=multi-user.target

I won't go into the details here (`archlinux has a great doc for that <https://wiki.archlinux.org/index.php/Systemd#Writing_unit_files>`__). It is just how we create new unit in systemd. A unit being used to run a service. We use a description and dependencies in the ``Unit`` section. We then define the service:

- ``PIDFile`` is used by systemd to know whether the service is working.
- We define as who the service will run.
- We update the path so it can find our venv.
- We defined the working directory to where the app of any given user will live thanks to ``%i``
- We defined how we want to start the service and how it can be reloaded and restarted.

.. raw:: html

   <p class="note">
   I didn't use the <code>--reload</code> option of guincorn. That is because we may need to update the dependencies of our project before restarting it.
   </p>

.. raw:: html

   <p class="tip">
   You may want to use the <code>--worker X</code> option to run multiple worker and thus allow your app to handle more traffic. In a test server it may not be needed though.
   </p>

Now we need to "tell systemd" the file has changed with ``systemctl daemon-reload``. We must also prepare the directories required by gunicorn to run:

#. Where the PID and socket files will be: ``mkdir -p /var/run/gunicorn``
#. Correct permissions: ``chmod u=rwX,g=rX,o=- /var/run/gunicorn/``

We can now start and enable the server by using the template and specifying the name of the instance like this: ``guincorn@DOMAIN``. Or in my case: ``systemctl start gunicorn@jujens.bl.test`` and ``systemctl enable gunicorn@jujens.bl.test``

.. raw:: html

   <p class="tip">
   The trailing <code>.service</code> in the service name is optional.
   </p>

.. raw:: html

   <p class="tip">
   Use <code>journalctl -u gunicorn@INTANCE</code> to view the logs. For instance: <code>journalctl -u gunicorn@jujens.bl.test</code>
   </p>

.. raw:: html

    <p class="tip">
    You don't actually need to create the <code>/var/run/gunicorn</code> directory. <code>systemd</code> will create it based on the value of <code>RuntimeDirectory</code> and set the permissions based on the values of <code>User</code> and <code>Group</code>.
    </p>

.. raw:: html

    <p class="tip">
    On Ubuntu, <code>/var/run/</code> is mounted on a tmpfs so the directory will disappear. It also seems that when you will launch multiple instances, the folder will be deleted and recreated making its usage as described in this tutorial impossible (<code>guincorn</code> cannot start by putting its PID and Socket directly in <code>/var/run</code> because of a permission issue). I suggest you use a runtime directory per user with <code>RuntimeDirectory=gunicorn-%i</code> or create a <code>/var/myrun/</code> folder and adapt the configuration or dig into how Ubuntu does things.
    </p>


Configuring nginx
-----------------

We need to update the ``location /`` section of our nginx configuration to use the proper UNIX socket:

.. code:: nginx

    location / {
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_redirect off;
        proxy_pass http://unix:/var/run/gunicorn/$host.sock;
    }

We also need to update the server name into ``server_name *.bl.test;``.

.. raw:: html

   <p class="note">
   nginx will respond with a <em>502 Bad Gateway</em> if there is no socket for that domain (either because it doesn't exist or because gunicorn is down).
   </p>


Static files
++++++++++++

We could use something like `whitenoise <https://github.com/evansd/whitenoise>`__ to serve our static files. But since we have a nginx server, we can also rely on it to do the job:

.. code:: nginx

    server {
        listen 80;
        server_name *.bl.test;
        root /var/www/bl.test;

        # Prevent access to source files.
        location ~ .*\.pyc? {
            return 404;
        }

        # Prevent access to git.
        location ~ .git {
            return 404;
        }

        # If you request /page, we will check if /$host/page exists. If so we serve it.
        # Otherwise we pass the request to gunicorn for processing.
        # For instance, if I request jujens.bl.test/requirements.txt, nginx will check whether
        # /var/www/bl.test/jujens.bl.test/requirements.txt exists and serve it to me.
        # /var/www/bl.test comes from the server root and jujens.bl.test from the $host variable.
        location / {
            try_files /$host/$uri @gunicorn;
        }

        # Relay everything else to the django web server.
        location @gunicorn {
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Host $http_host;
            proxy_redirect off;
            proxy_pass http://unix:/var/run/gunicorn/$host.sock;
        }
    }

.. raw:: html

   <p class="warning">
   Serving directly static files this way is insecure. You may want to choose an empty folder as root and relay everything to your app to serve only relevant files. Another good idea would be to put all static assets in a static directory and serve only its content. For Django users, you can adapt the configuration I mention <a href="/posts/en/2018/May/21/django/#use-a-nginx-reverse-proxy-in-dev">here</a>.
   </p>

Testing deployment
------------------

We are almost ready to test the deployment of our app. We still need two things. First, we must configure sudo to allow the user to restart gunicorn (because git hooks are run as the currently logged in user, jujens in my case, they may not be allowed to run ``systemctl restart`` by default). To do that:

#. Run ``visudo`` to edit the sudoers file (``visudo`` will check the syntax of the file before exiting. You can use another editor by setting the ``EDITOR`` environment variable).
#. Add at the bottom of the file: ``%deploy ALL=(ALL) NOPASSWD: /usr/bin/systemctl restart gunicorn@*`` This will allow all users in the ``deploy`` group to restart any instance of gunicorn. I think it is the easiest way to achieve our goal. Be more restrictive if you need to.

We can now add to our post-receive hook (``/etc/git/hooks/post-receive``):

.. code:: bash

    # We move to the folder that contains our code.
    # We extract the domain (and guincorn instance name) from the folder
    # by keeping only the name of the current directory of the full path.
    # Finally, we restart the proper instance of gunicorn.
    ( cd ..; unit_name=$(pwd); unit_name=${unit_name##*/}; sudo systemctl restart "gunicorn@${unit_name}" )

Now if you deploy a new commit, gunicorn will restart and if you do ``curl http://jujens.bl.test`` you will be able to see you modification.


Environment variables
=====================

So far so good. But you may want to configure your application on a per user basis (to use a specific database for instance). To do this, you can rely on environment variables as recommended by `the twelve factor app <https://12factor.net/config>`__. To do this, we will configure systemd to load an environment file which will contain all our variables. gunicorn will them be able to access them and so will our app.

To do that:

#. Add to the ``gunicorn@.service`` file (``/etc/systemd/system/gunicorn@.service``), after ``Environment``:

    .. code:: ini
        
        EnvironmentFile=/var/www/bl.test/%i/.env

#. Notify systemd about the change with: ``systemctl daemon-reload``.

You may also want to prevent access to this file with nginx:

.. code:: nginx

    location ~ ^/.env$ {
        return 404;
    }

Now create /var/www/bl.test/jujens.test.bl/.env and put in it:

.. code:: bash

    MY_ENV_VAR="Loaded from env"

Redeploy your app, run ``curl http://jujens.bl.test`` and you should see ``Loaded from env`` in the response.


Secure access
=============

Since this is a test server, you may want to restrict access to the application. You can do this by putting the server in a local network not accessible to the outside world, basic auth or X509 auth. I'll only detail the basic auth method. For X509, see: https://arcweb.co/securing-websites-nginx-and-client-side-certificate-authentication-linux/

To protect your server with a username and a password at the nginx level:

#. Install ``httpd-tools`` (or ``apache2-utils`` or whatever provides ``/usr/bin/htpasswd``)
#. Create the password file and add username to it: ``htpasswd -c /etc/nginx/bl.test.passwd username``
#. Type password
#. Configure nginx to use this file and prevent access without authentication by adding in the ``server`` block:

.. code:: nginx

    auth_basic "Test server";
    auth_basic_user_file /etc/nginx/bl.test.passwd;

Test the configuration with ``nginx -t`` and reload if the configuration is good with: ``systemctl reload nginx``.


Going further
=============

At this point, we are done and everything should work as intended. I just want to point out some other ways to do things:

- You could use the post-receive hooks to move static files into a dedicated folder (eg ``/var/www/bl.test/static/jujens.bl.test``) and use ``/var/www/bl.test/static`` as the root of nginx to be sure not to serve source files. Since it will communicate with gunicorn with a socket, its root need not be where the source lives. You don't need to bother about a dedicated path for each user if your static files contains their hash in their name. You'll have to setup a cleanup mechanism though. I didn't do it in this tutorial to keep things more simple.
- If you repo is big, you can use something like: ``git --work-tree=/var/www/bl.test/jujens.bl.test --git-dir=/home/jujens/bl.test/.git checkout -f`` to make everyone push to one git repository and update their source tree. You can use the ``$USER`` environment variable to know who pushed and copy to the proper directory. You'll have to adapt the file structure.
- `A very good article form digital ocean <https://www.digitalocean.com/community/tutorials/how-to-use-git-hooks-to-automate-development-and-deployment-tasks>`__ has almost the same subject as this tutorial (they give more precision about git hooks and handle only static applications).
- You can also extract the subdomain name (jujens in my case) to use just it in your paths and instance names. Something like this should work (see https://stackoverflow.com/a/33681072/3900519):

    .. code:: nginx

        map $host $env_name {
            default ...;
            ~* ^(?<x_env_name>[a-z0-9]+)\..* $x_env_name;
        }

- You can also add HTTPS with something like `let's encrypt <https://letsencrypt.org/>`__ or you usual provider.
- Depending on the level of customization you want, you could also use a project like `dokku <http://dokku.viewdocs.io/dokku/>`__.


Summary
=======

Thank you for going this far. The article is quite dense and I think it deserves a quick summary. You can also find all the configurations below with a download link.

So in a nutshell, to setup the server, you need to (refer to `Prepare the server`_ for the details):

#. Install the prerequisites.
#. Configure SSH.
#. Add and configure the base user and group.
#. Setup the global domain if possible.

Each time you want to add a user, you need to:

#. Add a new DNS entry if you can't use "glob domains".
#. Add a new user in the ``deploy`` group.
#. Prepare the code repository (clone and setup hooks if you didn't choose to use global ones).
#. Prepare the environment (``.env`` file, venv, …)
#. Start the application server.

You could use an adaptation of `this script <|static|/static/deploy-test-git-hooks/add-user.sh>`__ to make your life easier (run as root):

.. include:: ../static/deploy-test-git-hooks/add-user.sh
    :code: bash
    :number-lines:

Full configurations
-------------------

- `nginx configuration <|static|/static/deploy-test-git-hooks/bl.test.conf>`__:

  .. include:: ../static/deploy-test-git-hooks/bl.test.conf
      :code: nginx
      :number-lines:

- .. raw:: html

    <a href="/static/deploy-test-git-hooks/gunicorn@.service">gunicorn configuration</a>

  .. include:: ../static/deploy-test-git-hooks/gunicorn@.service
      :code: ini
      :number-lines:

- git hooks:

  - `pre-receive <|static|/static/deploy-test-git-hooks/pre-receive>`__ 
  
    .. include:: ../static/deploy-test-git-hooks/pre-receive
        :code: bash
        :number-lines:

  - `post-receive <|static|/static/deploy-test-git-hooks/post-receive>`__ 
  
    .. include:: ../static/deploy-test-git-hooks/post-receive
        :code: bash
        :number-lines:
