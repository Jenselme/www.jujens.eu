Importer périodiquement un flux ical dans owncloud
##################################################

:tags: Python, Calbdav, Owncloud
:uptated: 2015-09-21

J'ai toujours trouvé dommage que owncloud ne soit pas capable d'importer
périodiquement des calendriers que l'on trouve sur le web. Dans mon cas, mon
emploi du temps est un flux ical régulièrement mis à jour et j'aimerais bien que
owncloud soit capable de l'importer. Je me suis finalement résigné à faire mon
propre script pour régler ce problème.

Je pensais initialement le faire en Bash et utiliser ``curl`` pour faire les
requêtes vers owncloud. Cependant, un flux ical peut contenir des sauts de
lignes et les tenter de gérer correctement m'a fait souffrir. Du coup, je me
suis rabattu sur python et son excellente bibliothèque `requests
<http://docs.python-requests.org/en/latest/>`_.

Concrètement, ce script :

- lit dans ``~/.owncloud`` l'adresse et les identifiants du serveur owncloud,
- récupère les événements définis dans les flux icals que je lui demande de
  synchroniser. Ces événements sont stockés dans une instance de la classe
  ``Vevent`` crée à cet effet. J'ai ajouté la possibilité de filtrer certains
  événements (pour exclure des cours que je n'ai pas),
- supprime les événements présents dans owncloud mais absent des flux icals,
- poste (crée ou met à jour) les événements.

Il ne reste plus qu'à mettre le script dans un cron pour faire l'import
périodiquement.

Normalement le script est assez simple et clair pour être facilement
compréhensible. En cas de problème, postez un commentaire.

Vous pouvez aussi `télécharger ce script
<|static|/static/periodic-import-ical-owncloud/edt.py>`_ et le voir sur `le
dépôt mercurial
<https://bitbucket.org/Jenselme/server-bin/src/469c28a3ae87228f1ac3f3d0ad57349dda77e5b8/edt.py?at=default>`_.

.. include:: ../static/periodic-import-ical-owncloud/edt.py
   :code: python
   :number-lines:
