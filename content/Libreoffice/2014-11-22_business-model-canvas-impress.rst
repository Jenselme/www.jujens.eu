Des modèles de business model canvas en français pour LibreOffice Impress
#########################################################################

:tags: business model canvas, entreprenariat, création d'entreprise

Je viens de traduire en français un modèle de business modèle canvas pour
LibreOffice Impress. Il est au format A0. Pour l'inclure dans un autre document,
je vous conseille de l'exporter au format SVG puis le l'inclure en tant
qu'image. J'ai tenté d'utiliser l'import OLE mais le document est toujours trop
gros.

- `Le template en anglais
  <|static|/static/business_model_canvas_templates/BusinessModelCanvas.otp>`_
- `Le template en anglais à éditer
  <|static|/static/business_model_canvas_templates/BusinessModelCanvas_edit.otp>`_
- `Le template en français
  <|static|/static/business_model_canvas_templates/BusinessModelCanvas_fr.otp>`_
- `Le template en français à éditer </static/business_model_canvas_templates/BusinessModelCanvas_fr_edit.otp>`_

Les modèles dont le nom de fichier contient *edit* ne contiennent pas les
descriptifs des sections et sont donc prêts à être remplis par vos soins.

Le modèle original en anglais vient de
http://thoughtsof.matthiasjuette.de/business-model-canvas-template/. Ils sont
tous publiés sous la licence `Creative Commons Share Alike
<http://creativecommons.org/licenses/by-sa/3.0/>`_.
