How I switched an Aurelia application to webpack
================================================

:tags: aurelia, webpack
:lang: en
:modified: 2016-08-01

**Note:** I wrote this post when ``aurelia-webpack`` wast still in pre 1.0.0 release. As `niieani pointed out in the comments <http://www.jujens.eu/posts/en/2016/Apr/24/switch-to-webpack/#isso-74>`__ part of it may be outdated and you way want to use ``aurelia-webpack`` with `easy-webpack <https://github.com/easy-webpack/core>`__ instead. The general ideas are still valid though.

First thing first: why would I want to use `webpack
<https://webpack.github.io/>`__ with `Aurelia <aurelia.io>`__ instead of the
default JSPM?

- I was tired of JSPM which I find buggy
- No need for gulp anymore
- All deps are in the ``package.json`` file and ``node_modules`` folder
- webpack can build all kind of files (JS, TS, CSS, JSON, …) and perform
  optimization on them
- Awesome dev server with hot reload.

If you are interested in the subject, you can read `this article from Dwayne
Charrington
<http://ilikekillnerds.com/2016/03/ditching-jspmsystem-js-webpack/>`__ which
gives more details on why you may want to use webpack.

Here are the steps I followed. I did them nearly a month back but haven't found
the time to write this article before today, so it is based on my small notes
and memories. I apologize if this article is not as precise as it could be. If
you have a specific question or run into trouble, please leave a
comment. Anyway, the thing to remember is that the migration went well and
webpack works very well with Aurelia.

#. Update ``index.html``. Copy/Pasting the `index.html
   <https://github.com/aurelia/skeleton-navigation/blob/master/skeleton-es2016-webpack/index.html>`__
   from the skeleton with little adaptation should do the trick. You will also
   need a ``index.prod.html`` file for use in production. Again, `the skeleton
   has what you need
   <https://github.com/aurelia/skeleton-navigation/blob/master/skeleton-es2016-webpack/index.prod.html>`__.
#. Update dependencies in ``package.json``. Adapt `the one from the skeleton
   <https://github.com/aurelia/skeleton-navigation/blob/master/skeleton-es2016-webpack/package.json>`__. You
   may need to add dependencies beside webpack specific ones (specified in the
   skeleton) to get your app working. For instance, ``aurelia-logging-console``
   was not explicitly in my ``package.json`` (not even under the ``jspm``
   key). You can also get rid of the ``jspm`` key. In my case, Aurelia will give
   you a nice debug message if you are lacking a required dependency.
#. Update configuration of Karma. See `karma.conf in the skeleton
   <https://github.com/aurelia/skeleton-navigation/blob/master/skeleton-es2016-webpack/karma.conf.js>`__. For
   your tests to work, you are very likely to need to bootstrap your tests by
   adding to each file (you can also put that in a ``setup.js`` file and import
   it in your test files):

.. code:: js

   import { initialize } from 'aurelia-pal-browser';

   initialize();

4. Update ``main.js`` (create it if you don't have one). See `the skeleton for
   inspiration
   <https://github.com/aurelia/skeleton-navigation/blob/master/skeleton-es2016-webpack/src/main.js>`__. **Important
   note**: if you want you style files to be loaded with webpack, they must be
   imported in a JS file. ``main.js`` can be a good place to place these import.
#. Copy `webpack.config.js
   <https://github.com/aurelia/skeleton-navigation/blob/master/skeleton-es2016-webpack/webpack.config.js>`__
   and `webpack.prod.config.js
   <https://github.com/aurelia/skeleton-navigation/blob/master/skeleton-es2016-webpack/webpack.prod.config.js>`__
   and adapt them if need be.
#. Remove JSPM files: ``rm -rf jspm_packages config.js``
#. Remove unneeded node modules (eg jspm) ``rm -rf node_modules``
#. Remove gulp related files: ``rm -rf gulpfile.js build``
#. Install new dependencies: ``npm install``
#. Take care of submodules **Not needed with aurelia-webpack 1.0.0**: if you see an error like this:

::

   .*$:106 Uncaught (in promise) Error: Cannot find module './aurelia-i18n/t'.
   .*$:106 Uncaught (in promise) Error: Cannot find module './aurelia-i18n/nf'.
   .*$:106 Uncaught (in promise) Error: Cannot find module './aurelia-i18n/df'.
   Uncaught (in promise) Error: Cannot find module './aurelia-i18n/rt'.

You need to enable sub-modules for this modules, with something like this:

.. code:: js

   new AureliaWebpackPlugin({
       includeSubModules: [
           { moduleId: "aurelia-i18n" }
       ]
   })

See the `documentation of the webpack plugin for more information
<https://github.com/aurelia/webpack-plugin/#configuration-options>`__ on that.

Normally you are ready to launch
``./node_modules/.bin/webpack-dev-server --config
webpack.config.js --hot --inline --progress --devtool eval`` and start
developing with Aurelia and webpack.
