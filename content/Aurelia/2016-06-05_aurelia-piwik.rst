Plugin for statistics with Piwik on an Aurelia SPA
##################################################

:tags: aurelia, Piwik
:lang: en

On `Arena of Titans <http://www.arenaoftitans.com/>`__ a game based on the
`Aurelia <http://aurelia.io>`__ web framework I am developing with a friend, we
use `Piwik <https://piwik.org>`__ to have statistics about our visitors. When we
were about to launch the game, I realized that Piwik was not logging all the
pages but only the pages on which you entered. It makes perfect sense:
navigation on the site is handled by Aurelia which means there is no page load
and if there is no page load, Piwik doesn't load the navigation.

To go around this problem, I created a small plugin for Aurelia
``aurelia-piwik``. What it does is straightforward: it register a callback for
the ``router:navigation:success`` event which is fired by Aurelia's router on
successful navigation. In this callback, I use Piwik's API to log the
navigation.

You can easily use this plugin once installed by registering it in your
``main.js``:

.. code:: javascript

   aurelia.use
    .standardConfiguration()
    .developmentLogging()
    .plugin('aurelia-piwik')

I must say I am impressed by how easy it was to implement and use. After nearly
seven months of Aurelia usage, I am really glad to use this framework. If you
haven't checked it out yet, please do!

The source are available under the MIT license `on GitHub
<https://github.com/Jenselme/aurelia-piwik>`__. The package is also available on
`npm <https://www.npmjs.com/package/aurelia-piwik>`__.
