Extract translations from your Aurelia app
##########################################

:tags: JavaScript, TypeScript, Aurelia, i18N
:lang: en

If you use Aurelia with the `i18n plugin <https://github.com/aurelia/i18n>`__, you will have to maintain JSON files that will map a translation key to an actual translation. Manually editing the file to add/remove keys can be tedious.

Luckily, keys can be extracted automatically thanks to `i18next-scanner <https://github.com/i18next/i18next-scanner>`__.  It can extract translation keys  from JavaScript, TypeScript and HTML files.

To do this:

#. Install ``i18next-scanner`` with ``npm install --save-dev i18next-scanner`` or ``yarn add -D i18next-scanner``.
#. Add the code below in ``i18next-scanner.config.js`` file at the root of your project.
#.  Run ``./node_modules/.bin/i18next-scanner src/**/*.{js,ts,html}`` to extract the translations. They will be under ``src/locales/{{lng}}/translation.json``. Keys to be translated will have the value ``__STRING_NOT_TRANSLATED__``.

The extraction supports the following patterns:

- In code files: it will extract keys directly from ``i18next`` (usage like ``i18next.t('my-key')``) and from the injected ``I18N`` service (ie if you use it like this: ``this.i18n.tr('my-key')``).
- In HMTL files:

  - With the following attributes: ``data-i18n``, ``data-t``, ``t``, ``i18n`` (usage like this ``<p t='my-key'></p>``).
  - With the following behaviors: ``t`` (usage ``${ 'myKey' | t }`` and ``${ 'myKey' & t }`` – the whitespace is not important).

If this doesn't exactly work for your use cases, please adapt the code below and ask a question in the comments if needed.

.. code:: javascript
    :number-lines:

    const fs = require('fs');
    const path = require('path');
    const typescript = require("typescript");

    module.exports = {
        input: [
            'src/**/*.{ts}',
        ],
        output: './',
        options: {
            debug: true,
            func: {
                list: ['i18next.t', 'i18n.t', 'this.i18n.tr'],
                extensions: ['.js'],
            },
            lngs: ['en', 'fr'],
            ns: [
                'translation',
            ],
            defaultLng: 'en',
            defaultNs: 'translation',
            defaultValue: '__STRING_NOT_TRANSLATED__',
            resource: {
                loadPath: 'src/locales/{{lng}}/{{ns}}.json',
                savePath: 'src/locales/{{lng}}/{{ns}}.json',
                jsonIndent: 4,
                lineEnding: '\n'
            },
            nsSeparator: false, // namespace separator
            keySeparator: false, // key separator
            interpolation: {
                prefix: '{{',
                suffix: '}}'
            },
            removeUnusedKeys: true,
        },

        transform: function customTransform(file, enc, done) {
            const {ext} = path.parse(file.path);
            const content = fs.readFileSync(file.path, enc);

            if (ext === '.ts') {
                const {outputText} = typescript.transpileModule(content.toString(), {
                    compilerOptions: {
                        target: 'es2018',
                    },
                    fileName: path.basename(file.path),
                });

                this.parser.parseFuncFromString(outputText, {list: ['i18next.t', 'i18next.tr', 'this.i18n.tr']});
            } else if (ext === '.html') {
                this.parser
                    .parseAttrFromString(content, {list: ['data-i18n', 'data-t', 't', 'i18n'] });

                // We extra behaviours `${ 'myKey' | t }` and `${ 'myKey' & t }` from the file.
                const extractBehaviours = /\${ *'([a-zA-Z0-9]+)' *[&|] *t *}/g;
                const strContent = content.toString();
                let group;
                while (true) {
                    group = extractBehaviours.exec(strContent);
                    if (group === null) {
                        break;
                    }
                    this.parser.set(group[1]);
                }
            }

            done();
        },
    };

You can also view the code in gitlab: https://gitlab.com/snippets/1881948

.. raw:: html

    <p class="warning">
    Because of the <code>removeUnusedKeys: true</code> options, keys that are not found will be removed from the JSON files. If you don't want this behavior, switch this option to <code>false</code>.
    </p>

.. raw:: html

    <p class="note">
    It is compatible with Javascript and Typescript. If you don't use Typescript, remove the line <code>const typescript = require("typescript");</code> to avoid issues at import.
    </p>
