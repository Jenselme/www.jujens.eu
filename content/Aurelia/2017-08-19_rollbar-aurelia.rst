Integrate Rollbar with Aurelia
##############################

:tags: aurelia, rollbar
:lang: en

I recently integrated `Rollbar <https://rollbar.com/>`__ (a service to `Detect, diagnose and debug errors with ease`) into an Aurelia application. I explain here how I did it. I start by explaining how logging works with Aurelia and then how I integrated Rollbar into this.

.. contents::


Logging with Aurelia
====================

The main module to log things with Aurelia is simply named `aurelia-logging <https://github.com/aurelia/logging>`__. It is responsible for:

- Setting the log level you want with ``setLevel``.
- Create/get loggers with ``getLogger``. When you get a logger with this function, logs messages will be dispatched to all registered log appenders (more on that in a minute) while respecting the level of log you set.
- Defining the ``Appender`` interface.

So the ``aurelia-logging`` is a generic module to deal with logging but doesn't log anything by itself. It is defined this way so it can work in various environment (eg the browser and Node) and send logs where you want (let it be the browser console or Rollbar).

To actually do some logging, we need to add at least one Appender with ``addAppender``. For instance, you can use the `aurelia-logging-console <https://github.com/aurelia/logging-console>`__ module to append log to the browser console. Or you can use `au-rollbar <https://github.com/Jenselme/au-rollbar>`__ to log the messages into Rollbar.

Let's illustrate this with an example. Let's say I configure the log level to log only warnings and above with ``Logger.setLevel(Logger.logLevel.warn);`` and that you added the ``ConsoleAppender`` and ``RollbarAppender`` to the list of appenders with ``Logger.addAppender(new ConsoleAppender())`` and ``Logger.addAppender(new RollbarAppender());``.

You then create your logger with ``let myLogger = Logger.getLogger('my-module')``. You then use the logger, eg:

.. code:: javascript

    myLogger.warn('A warning')
    myLogger.debug('A debug message')

The ``A warning`` message will be dispatched to the console and rollbar while ``A debug message`` won't be logged anywhere.


Integration with Rollbar
========================

Using Rollbar within Aurelia
----------------------------

In order to integrate Rollbar into Aurelia, I created a ``RollbarAppender`` that implements the ``Appender`` interface (see the interface definition `here <https://github.com/aurelia/logging/blob/master/src/index.js#L85>`__). What it does is pretty straightforward: it dispatches incoming log messages to the proper methods of the ``Rollbar`` object. Just like in ``ConsoleAppender`` I prepend the level and id of the logger to the message.

Since according to `the doc <https://rollbar.com/docs/notifier/rollbar.js/#usage>`__ I can only give one message, I put the eventual extra parameters of the function in a JSON object to log them as extra arguments.

Here is the full code. You can download it `here <|static|/static/aurelia-rollbar/rollbar.js>`__. You can also install it with ``npm install au-rollbar``.

.. include:: ../static/aurelia-rollbar/rollbar.js
    :code: javascript
    :number-lines:

You then need to import it with something like ``import RollbarAppender from './logging';`` (or ``import RollbarAppender from 'au-rollbar'`` if you installed it with npm). You then need to configure your logging. Since I doubt you will want to enable it in development, I suggest you configure Aurelia like this (in your ``main.js`` file):

.. code:: javascript
    :number-lines:

    import RollbarAppender from 'au-rollbar'
    import * as Logger from 'aurelia-logging';

    export function configure(aurelia) {
        aurelia.use.standardConfiguration();

        if (environment.debug) {
            // Only log to the console when developing (default configuration of developmentLogging)
            aurelia.use.developmentLogging();
        } else {
            // Production logging is inspired by developmentLogging
            // This loads the ``aurelia-logging-console`` module and then configures logging.
            aurelia.use.preTask(() => {
                return aurelia.loader.normalize(
                        'aurelia-logging-console',
                        aurelia.bootstrapperName
                ).then(name => {
                    return aurelia.loader.loadModule(name).then(m => {
                        // Once the module is loaded, we configure logging.
                        // We add the ConsoleAppender so all messages are logged in the console of the browser.
                        Logger.addAppender(new m.ConsoleAppender());
                        // If the Rollbar exists in the window object (ie if Rollbar is loaded),
                        // we add the RollbarAppender so messages are sent to rollbar.
                        // If it is not, we print a warning in the browser console.
                        if (window.Rollbar) {
                            Logger.addAppender(new RollbarAppender());
                        } else {
                            console.warn('Rollbar is not defined');  // eslint-disable-line no-console
                        }
                        // Configure logger to log only messages of level warning or above.
                        Logger.setLevel(Logger.logLevel.warn);
                    });
                });
            });
        }

        if (environment.testing) {
            aurelia.use.plugin('aurelia-testing');
        }

        aurelia.start().then(() => aurelia.setRoot('app', document.body));
    }

Loading Rollbar
---------------

Now we need to load the Rollbar script for Rollbar to actually work. Since in addition to logging messages with the ``Rollbar`` object, Rollbar can log uncaught exception, it is better to load it as early as possible (to detect errors when Aurelia is bootstraping for instance). To achieve this, I decided to put the configuration and quickstart script (from `here <https://rollbar.com/docs/notifier/rollbar.js/>`__) into a ``rollbar.js`` script located in the ``scripts`` folder. Then, since I use the CLI to build my project, I edited ``aurelia_project/aurelia.json`` to add ``"scripts/rollbar.js",`` in the prepend section of the vendor bundle. So the definition of the vendor bundle looks like:

.. code:: json
    :number-lines:

    {
        "name": "vendor-bundle.js",
        "prepend": [
            "node_modules/bluebird/js/browser/bluebird.js",
            "scripts/polyfills.js",
            "scripts/rollbar.js",
            "node_modules/requirejs/require.js"
        ],
        "dependencies": [
            "aurelia-binding",
            "aurelia-bootstrapper",
            "aurelia-dependency-injection",
            "aurelia-event-aggregator",
            "aurelia-framework",
            "aurelia-history",
            "aurelia-history-browser",
            "aurelia-loader",
            "aurelia-loader-default",
            "aurelia-logging",
            "aurelia-logging-console",
            "aurelia-metadata",
            "aurelia-pal",
            "aurelia-pal-browser",
            "aurelia-path",
            "aurelia-polyfills",
            "aurelia-route-recognizer",
            "aurelia-router",
            "aurelia-task-queue",
            "aurelia-templating",
            "aurelia-templating-binding",
            "text",
            {
                "name": "aurelia-templating-resources",
                "path": "../node_modules/aurelia-templating-resources/dist/amd",
                "main": "aurelia-templating-resources"
            },
            {
                "name": "aurelia-templating-router",
                "path": "../node_modules/aurelia-templating-router/dist/amd",
                "main": "aurelia-templating-router"
            },
            {
                "name": "aurelia-testing",
                "path": "../node_modules/aurelia-testing/dist/amd",
                "main": "aurelia-testing",
                "env": "dev"
            }
        ]
    }

If you want to load Rollbar from another location, you can use the ``rollbarJsUrl`` property in the configuration object and give it the URL from which you want to load the script.

That's it. If you have any question or remark, please leave a comment.
