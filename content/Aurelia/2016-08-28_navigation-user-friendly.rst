Make navigation user friendly on your Aurelia SPA
#################################################

:tags: aurelia, font awesome
:lang: en

.. contents::

Add a spinner
=============

#. Add the `font awesome files <http://fontawesome.io/>`__ to your project (CSS
   and fonts files).
#. Import the font awesome style sheet in your ``index.html``

   .. code:: html

      <link rel="stylesheet" href="/assets/fonts/fontawesome/font-awesome.min.css">

#. Add the CSS code below to create the spinner either directly in the
   ``index.html`` with ``style`` tag or in a CSS file that you load in it:

   .. code:: css

            .splash {
                text-align: center;
                margin: 10% 0 0 0;
                box-sizing: border-box;
            }

            .splash .message {
                font-size: 72px;
                line-height: 72px;
                text-shadow: rgba(0, 0, 0, 0.5) 0 0 15px;
                text-transform: uppercase;
                font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            }

            .splash .fa-spinner {
                text-align: center;
                display: inline-block;
                font-size: 72px;
                margin-top: 50px;
            }

#. Update the ``div`` in which your Aurelia application will be instantiated. Eg, replace:

   .. code:: html

      <body aurelia-app="main">
        <div>
        </div>
      </body>

   By:

   .. code:: html

      <body aurelia-app="main">
        <div class="splash">
            <div class="message">Arena of Titans</div>
            <i class="fa fa-spinner fa-spin"></i>
         </div>
      </body>


Use the wait cursor while navigating
====================================

#. Change your ``router-view`` from:

   .. code:: html

      <router-view></router-view>

   to:

   .. code:: html

      <router-view class="${router.isNavigating ? 'navigating' : ''}"></router-view>

   this will add a class ``navigating`` to the ``router-view`` tag when the
   router is navigating and nothing otherwise.

#. Add this global CSS code to your application:

   .. code:: css

      .navigating {
          cursor: wait !important;
          opacity: 0.7;
      }

      .navigating button {
          cursor: wait !important;
          opacity: 0.5;
      }

   This will display the wait cursor on the page and reduce the opacity on the
   page to signal the user a navigation is going on. You can of course change
   this code to match your application.

#. Don't forget to add the router to your object. In the
   ``configureRouter(config, router)`` function, do:

   .. code:: javascript

      this.router = router;


Display a message to no-script user
===================================

Some of your users may use an extension like `noscript
<https://addons.mozilla.org/en-US/firefox/addon/noscript>`__ which disable
JavaScript on page you visit. By default, you will greet them with either a
blank page or a never-ending spinner. I think it is more user friendly to show
them a message asking to enable JavaScript. To do, add in your ``body`` tag:

.. code:: html

    <noscript>
        <p>Arena of Titans cannot work without JavaScript. Please enable it.</p>
        <p>Arena of Titans ne peut pas fonctionner sans JavaScript. Merci de l'activer.</p>
   </noscript>


More
====

To view the full details, look at these two commits:

#. `feat(index): improve feeling on page load
   <https://bitbucket.org/arenaoftitans/arena-of-titans/commits/a0630aadd969e87da85c9b87c972cb94d4443ae7>`__
#. `feat(app): notify visualy that the router is navigating
   <https://bitbucket.org/arenaoftitans/arena-of-titans/commits/e0ad0e16b804920442cd39749b4b964c64a98580>`__
